-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE DATABASE `rbac_egesetzgebung` /*!40100 COLLATE 'utf8mb4_0900_ai_ci' */;
CREATE USER 'rbac_user'@'%' IDENTIFIED BY 'password';

GRANT USAGE ON `rbac_egesetzgebung`.* TO 'rbac_user'@'%';

GRANT SELECT, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, DELETE, DROP, INDEX, INSERT, REFERENCES, UPDATE  ON `rbac\_egesetzgebung`.* TO 'rbac_user'@'%';
FLUSH PRIVILEGES;

USE `rbac_egesetzgebung`;