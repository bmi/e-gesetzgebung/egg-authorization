-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- Hinweis: In H2 werden Spalten- und Tabellennamen standardmäßig in Großbuchstaben umgewandelt,
-- es sei denn, sie werden in Anführungszeichen gesetzt.

CREATE TABLE `aktionen` (
  `id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `bezeichnung` varchar(255) DEFAULT NULL,
  CONSTRAINT `UK_aktionen_bezeichnung` UNIQUE (`bezeichnung`)
);

CREATE TABLE `ressource_typen` (
  `id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `bezeichnung` varchar(255) DEFAULT NULL,
  CONSTRAINT `UK_ressource_typen_bezeichnung` UNIQUE (`bezeichnung`)
);

CREATE TABLE `rollen` (
  `id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `bezeichnung` varchar(255) DEFAULT NULL,
  CONSTRAINT `UK_rollen_bezeichnung` UNIQUE (`bezeichnung`)
);

CREATE TABLE `status` (
  `id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `bezeichnung` varchar(255) DEFAULT NULL,
  CONSTRAINT `UK_status_bezeichnung` UNIQUE (`bezeichnung`)
);

CREATE TABLE `regeln` (
  `id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `bezeichnung` varchar(255) DEFAULT NULL,
  `aktion_id` bigint DEFAULT NULL,
  `ressource_typ_id` bigint DEFAULT NULL,
  `rolle_id` bigint DEFAULT NULL,
  `status_id` bigint DEFAULT NULL,
  CONSTRAINT `UK_regeln_bezeichnung` UNIQUE (`bezeichnung`),
  CONSTRAINT `UK_regeln_eintraege` UNIQUE (`ressource_typ_id`,`rolle_id`,`status_id`,`aktion_id`),
  CONSTRAINT `IK_regeln_eintraege` UNIQUE (`ressource_typ_id`,`rolle_id`,`status_id`,`aktion_id`),
  CONSTRAINT `FK_aktion_regel__regel_id` FOREIGN KEY (`aktion_id`) REFERENCES `aktionen` (`id`),
  CONSTRAINT `FK_ressourcetype_regel__regeln_id` FOREIGN KEY (`ressource_typ_id`) REFERENCES `ressource_typen` (`id`),
  CONSTRAINT `FK_rolle_regel__regel_id` FOREIGN KEY (`rolle_id`) REFERENCES `rollen` (`id`),
  CONSTRAINT `FK_status_regel__regel_id` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`)
);

CREATE TABLE `zuordnungen` (
  `id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `benutzer_id` varchar(255) DEFAULT NULL,
  `bezeichnung` varchar(255) DEFAULT NULL,
  `ressource_id` varchar(255) DEFAULT NULL,
  `ressource_typ_id` bigint DEFAULT NULL,
  `rolle_id` bigint DEFAULT NULL,
  `anmerkungen` varchar(255) DEFAULT NULL,
  CONSTRAINT `UK_zuordnungen_eintraege` UNIQUE (`ressource_typ_id`,`rolle_id`,`ressource_id`,`benutzer_id`),
  CONSTRAINT `FK_ressource_typ_zuordnung` FOREIGN KEY (`ressource_typ_id`) REFERENCES `ressource_typen` (`id`),
  CONSTRAINT `FK_rolle_zuordnung` FOREIGN KEY (`rolle_id`) REFERENCES `rollen` (`id`)
);

CREATE TABLE `nutzer` (
  `id` UUID NOT NULL PRIMARY KEY,
  `erstellt_am` TIMESTAMP NOT NULL,
  `bearbeitet_am` TIMESTAMP NOT NULL,
  `name` VARCHAR(255),
  `email` VARCHAR(255) NOT NULL,
  `gid` VARCHAR(255),
  `telefon` VARCHAR(255),
  `abteilung` VARCHAR(255),
  `referat` VARCHAR(255),
  `titel` VARCHAR(255),
  `ressort_id` BIGINT,
  `ressort_kurzbezeichnung` VARCHAR(255),
  `anrede` VARCHAR(255),
  `aktiv` BOOLEAN NOT NULL,
  `deleted` BOOLEAN NOT NULL,
  `last_modified` TIMESTAMP,
  CONSTRAINT `UK_nutzer_gid` UNIQUE (`gid`)
);

CREATE TABLE `nutzer_stellvertreter` (
  `nutzer_id` UUID NOT NULL,
  `stellvertreter_id` UUID NOT NULL,
  PRIMARY KEY (`nutzer_id`, `stellvertreter_id`),
  CONSTRAINT `FK_nutzer_stellvertreter_nutzer_id` FOREIGN KEY (`nutzer_id`) REFERENCES `nutzer`(`id`),
  CONSTRAINT `FK_nutzer_stellvertreter_stellvertreter_id` FOREIGN KEY (`stellvertreter_id`) REFERENCES `nutzer`(`id`)
);
