-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `aktionen` (`bezeichnung`) VALUES
	('LESEN'),
	('SCHREIBEN'),
	('UMBENNENEN'),
	('KOMMENTIEREN'),
	('DOKUMENTE_ERSTELLEN'),
    ('KOMMENTAR_ANTWORTEN'),
    ('SUCHENundERSETZEN');

INSERT INTO `ressource_typen` (`bezeichnung`) VALUES
	('DOKUMENTE'),
	('DOKUMENTENMAPPE'),
	('REGELUNGSVORHABEN'),
	('HRA');

INSERT INTO `rollen` (`bezeichnung`) VALUES
	('ERSTELLER'),
	('MITARBEITER');

INSERT INTO `status` (`bezeichnung`) VALUES
	('DRAFT'),
	('FREEZE'),
	('FINAL'),
	('DONT CARE');
