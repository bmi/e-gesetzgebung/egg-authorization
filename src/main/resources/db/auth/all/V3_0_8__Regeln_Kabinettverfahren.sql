-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT IGNORE INTO `status` (`bezeichnung`) VALUES ('BEREIT_FUER_KABINETTVERFAHREN');
INSERT IGNORE INTO `status` (`bezeichnung`) VALUES ('ZUGELEITET_PKP');
INSERT IGNORE INTO `status` (`bezeichnung`) VALUES ('ZUGESTELLT_BUNDESRAT');


INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ERSTELLER darf BEREIT_FUER_KABINETTVERFAHREN Dokumenten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
      (SELECT id FROM status WHERE bezeichnung = 'BEREIT_FUER_KABINETTVERFAHREN')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ERSTELLER darf ZUGELEITET_PKP Dokumenten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
      (SELECT id FROM status WHERE bezeichnung = 'ZUGELEITET_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ERSTELLER darf ZUGESTELLT_BUNDESRAT Dokumenten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
      (SELECT id FROM status WHERE bezeichnung = 'ZUGESTELLT_BUNDESRAT')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'LESER darf BEREIT_FUER_KABINETTVERFAHREN Dokumenten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
      (SELECT id FROM status WHERE bezeichnung = 'BEREIT_FUER_KABINETTVERFAHREN')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'LESER darf ZUGELEITET_PKP Dokumenten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
      (SELECT id FROM status WHERE bezeichnung = 'ZUGELEITET_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'LESER darf ZUGESTELLT_BUNDESRAT Dokumenten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
      (SELECT id FROM status WHERE bezeichnung = 'ZUGESTELLT_BUNDESRAT')
    );