-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- rule set FEDERFUEHRER for eGFA

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf GFA Module: Sonstige Kosten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf GFA Module: Sonstige Kosten schreiben',
      (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf GFA Module: Sonstige Kosten exportieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EXPORTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Sonstige Kosten die Ergebnisdokumentation lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'ERGEBNISDOKUMENTATION_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Sonstige Kosten die Berechtigungen lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Sonstige Kosten die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

-- rule set MITARBEITER for eGFA

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf GFA Module: Sonstige Kosten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf GFA Module: Sonstige Kosten schreiben',
      (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf GFA Module: Sonstige Kosten exportieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EXPORTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Sonstige Kosten die Ergebnisdokumentation lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'ERGEBNISDOKUMENTATION_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Sonstige Kosten die Berechtigungen lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Sonstige Kosten die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

-- rule set GAST (Beobachtung) for eGFA

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Beobachtung darf GFA Module: Sonstige Kosten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GAST'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Beobachtung darf GFA Module: Sonstige Kosten exportieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EXPORTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GAST'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Beobachtung darf im GFA Module: Sonstige Kosten die Ergebnisdokumentation lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'ERGEBNISDOKUMENTATION_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GAST'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Beobachtung darf im GFA Module: Sonstige Kosten die Berechtigungen lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GAST'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

-- rule set GFA_MODUL_MITARBEIT for eGFA

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf GFA Module: Sonstige Kosten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf GFA Module: Sonstige Kosten schreiben',
      (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf GFA Module: Sonstige Kosten exportieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EXPORTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Sonstige Kosten die Ergebnisdokumentation lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'ERGEBNISDOKUMENTATION_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Sonstige Kosten die Berechtigungen lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Sonstige Kosten die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Sonstige Kosten seine eigene Berechtigungen löschen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EIGENE_BERECHTIGUNGEN_LOESCHEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

-- rule set GFA_MODUL_BEOBACHTUNG for eGFA

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Beobachter darf GFA Module: Sonstige Kosten lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_BEOBACHTUNG'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Beobachter darf GFA Module: Sonstige Kosten exportieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EXPORTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_BEOBACHTUNG'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Beobachter darf im GFA Module: Sonstige Kosten die Ergebnisdokumentation lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'ERGEBNISDOKUMENTATION_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_BEOBACHTUNG'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Beobachter darf im GFA Module: Sonstige Kosten die Berechtigungen lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_BEOBACHTUNG'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Beobachter darf im GFA Module: Sonstige Kosten seine eigene Berechtigungen löschen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EIGENE_BERECHTIGUNGEN_LOESCHEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GFA_MODUL_BEOBACHTUNG'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );