-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- EGBR-15 rename BR_AUSSCHUSSMITGLIED to BR_AUSSCHUSSSEKRETARIAT
-- Migrates all tables with foreign key to rolle.id.

-- Migrates entries in table nutzer.
UPDATE nutzer SET rolle_id = (
  SELECT id FROM rollen WHERE bezeichnung = 'BR_AUSSCHUSSSEKRETARIAT'
) WHERE rolle_id = (
  SELECT id FROM rollen WHERE bezeichnung = 'BR_AUSSCHUSSMITGLIED'
);

-- Migrates entries in table regeln.
UPDATE regeln SET rolle_id = (
  SELECT id FROM rollen WHERE bezeichnung = 'BR_AUSSCHUSSSEKRETARIAT'
) WHERE rolle_id = (
  SELECT id FROM rollen WHERE bezeichnung = 'BR_AUSSCHUSSMITGLIED'
);

-- Migrates entries in table zuordnungen.
UPDATE zuordnungen SET rolle_id = (
  SELECT id FROM rollen WHERE bezeichnung = 'BR_AUSSCHUSSSEKRETARIAT'
) WHERE rolle_id = (
  SELECT id FROM rollen WHERE bezeichnung = 'BR_AUSSCHUSSMITGLIED'
);

-- Migrates entries in table globalerollen.
UPDATE zuordnungen_globalerollen SET rolle_id = (
  SELECT id FROM rollen WHERE bezeichnung = 'BR_AUSSCHUSSSEKRETARIAT'
) WHERE rolle_id = (
  SELECT id FROM rollen WHERE bezeichnung = 'BR_AUSSCHUSSMITGLIED'
);