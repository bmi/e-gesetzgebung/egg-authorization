-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

UPDATE ressource_typen SET bezeichnung = 'GFA' where bezeichnung = 'EGFA';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_DEMOGRAFIE' where bezeichnung = 'EGFA_MODUL_DEMOGRAFIE';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_DISABILITY' where bezeichnung = 'EGFA_MODUL_DISABILITY';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_EAOEHH' where bezeichnung = 'EGFA_MODUL_EAOEHH';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_ENAP' where bezeichnung = 'EGFA_MODUL_ENAP';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_ERFUELLUNGSAUFWAND' where bezeichnung = 'EGFA_MODUL_ERFUELLUNGSAUFWAND';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_EVALUIERUNG' where bezeichnung = 'EGFA_MODUL_EVALUIERUNG';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_GLEICHSTELLUNG' where bezeichnung = 'EGFA_MODUL_GLEICHSTELLUNG';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_GLEICHWERTIGKEIT' where bezeichnung = 'EGFA_MODUL_GLEICHWERTIGKEIT';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_KMU' where bezeichnung = 'EGFA_MODUL_KMU';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_PREISE' where bezeichnung = 'EGFA_MODUL_PREISE';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN' where bezeichnung = 'EGFA_MODUL_SONSTIGE_KOSTEN';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_VERBRAUCHER' where bezeichnung = 'EGFA_MODUL_VERBRAUCHER';
UPDATE ressource_typen SET bezeichnung = 'GFA_MODUL_WEITERE' where bezeichnung = 'EGFA_MODUL_WEITERE';