-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- EGBR-15: Makes 'Ausschussbüro BR' a clone of BR_AUSSCHLUSSMITGLIED.
INSERT INTO rollen (bezeichnung)
VALUES ('BR_AUSSCHUSSBUERO'), ('BR_AUSSCHUSSSEKRETARIAT');


-- There are no rules to clone.
-- Neither AUSSCHUSSMITGLIED, AUSSCHUSSSEKRETARIAT, BR_AUSSCHUSSMITGLIED have any entries defined in table 'regeln'.