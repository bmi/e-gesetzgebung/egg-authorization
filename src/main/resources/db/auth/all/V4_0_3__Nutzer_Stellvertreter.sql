-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `nutzer_stellvertreter` (
  nutzer_id binary(16) NOT NULL,
  stellvertreter_id binary(16) NOT NULL,
  PRIMARY KEY (nutzer_id, stellvertreter_id),
  FOREIGN KEY (nutzer_id) REFERENCES nutzer(id),
  FOREIGN KEY (stellvertreter_id) REFERENCES nutzer(id),
  KEY `IK_nutzer_id` (nutzer_id),
  KEY `IK_stellvertreter_id` (stellvertreter_id)
)