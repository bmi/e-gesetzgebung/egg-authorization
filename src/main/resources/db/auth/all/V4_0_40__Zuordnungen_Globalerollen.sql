-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `zuordnungen_globalerollen` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `rolle_id` bigint NOT NULL,
  `bezeichnung` varchar(255) DEFAULT NULL,
  `ressource_id` varchar(255) NOT NULL,
  `ressource_typ_id` bigint NOT NULL,
  `globale_rolle_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_zuordnungen_globalerollen_eintraege` (`ressource_typ_id`,`rolle_id`,`ressource_id`,`globale_rolle_id`),
  KEY `IK_zuordnungen_globalerollen` (`ressource_typ_id`,`rolle_id`,`ressource_id`,`globale_rolle_id`),
  KEY `IK_zuordnungen_globalerollen_rolle` (`globale_rolle_id`),
  CONSTRAINT `FKressourcetypidressourcetypen` FOREIGN KEY (`ressource_typ_id`) REFERENCES `ressource_typen` (`id`),
  CONSTRAINT `FKglobalerolleidrollen` FOREIGN KEY (`globale_rolle_id`) REFERENCES `rollen` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2137 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;