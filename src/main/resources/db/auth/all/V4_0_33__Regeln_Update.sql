-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES(
    'BR_FACHADMINISTRATION_P1 darf Dokumente LADEN in der Fachadministration',
    (SELECT id FROM aktionen WHERE bezeichnung = 'LADEN'),
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION'),
    (SELECT id FROM rollen WHERE bezeichnung = 'BR_FACHADMINISTRATION_P1'),
    (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES(
    'BR_FACHADMINISTRATION_P1 darf Dokumente ZUWEISEN in der Fachadministration',
    (SELECT id FROM aktionen WHERE bezeichnung = 'ZUWEISEN'),
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION'),
    (SELECT id FROM rollen WHERE bezeichnung = 'BR_FACHADMINISTRATION_P1'),
    (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES(
    'BR_FACHADMINISTRATION_AUSSCHUESSE darf Dokumente LADEN in der Fachadministration',
    (SELECT id FROM aktionen WHERE bezeichnung = 'LADEN'),
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION'),
    (SELECT id FROM rollen WHERE bezeichnung = 'BR_FACHADMINISTRATION_AUSSCHUESSE'),
    (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES(
    'BR_FACHADMINISTRATION_AUSSCHUESSE darf Dokumente ZUWEISEN in der Fachadministration',
    (SELECT id FROM aktionen WHERE bezeichnung = 'ZUWEISEN'),
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION'),
    (SELECT id FROM rollen WHERE bezeichnung = 'BR_FACHADMINISTRATION_AUSSCHUESSE'),
    (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );