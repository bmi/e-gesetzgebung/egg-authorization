-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `rollen` (`bezeichnung`) VALUES ('ZPV_BESITZER'), ('ZPV_BEOBACHTER');

INSERT INTO `ressource_typen` (`bezeichnung`) VALUES ('ZEITPLANUNGSVORLAGE');

INSERT INTO `aktionen` (`bezeichnung`) VALUES ('EXPORTIEREN'), ('EIGENE_BERECHTIGUNGEN_LOESCHEN');

INSERT INTO `status` (`bezeichnung`) VALUES ('AKTIV');

-- Regeln für den ZPV_BESITZER

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf Zeitplanungsvorlage lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf Element einer Zeitplanungsvorlage (Phase/Termin) bearbeiten',
      (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf Element einer Zeitplanungsvorlage (Phase/Termin) löschen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LOESCHEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf Kopie einer Zeitplanungsvorlage als neue interne Version speichern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'NEUE_VERSION_ERSTELLEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf die Berechtigungen der Zeitplanungsvorlage lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf die Berechtigungen der Zeitplanungsvorlage ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf Zeitplanungsvorlage archivieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'ARCHIVIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf Zeitplanungsvorlage exportieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EXPORTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Besitzer darf einen Kommentar für eine Zeitplanungsvorlage erstellen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BESITZER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

-- Regeln für den ZPV_BEOBACHTER

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Beobachter darf Zeitplanungsvorlage lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BEOBACHTER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Beobachter darf die Berechtigungen der Zeitplanungsvorlage lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BEOBACHTER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Beobachter darf seine eigenen ZPV-Berechtigungen von der Zeitplanungsvorlage entfernen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EIGENE_BERECHTIGUNGEN_LOESCHEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BEOBACHTER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Beobachter darf Zeitplanungsvorlage exportieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'EXPORTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BEOBACHTER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'ZPV_Beobachter darf einen Kommentar für eine Zeitplanungsvorlage erstellen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'ZEITPLANUNGSVORLAGE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ZPV_BEOBACHTER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );