-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `rollen` (`bezeichnung`) VALUES
    ('GFA_MODUL_MITARBEIT'),
    ('GFA_MODUL_BEOBACHTUNG');

INSERT INTO `ressource_typen` (`bezeichnung`) VALUES
    ('GFA'),
    ('GFA_MODUL_DEMOGRAFIE'),
    ('GFA_MODUL_DISABILITY'),
    ('GFA_MODUL_EAOEHH'),
    ('GFA_MODUL_ENAP'),
    ('GFA_MODUL_ERFUELLUNGSAUFWAND'),
    ('GFA_MODUL_EVALUIERUNG'),
    ('GFA_MODUL_GLEICHSTELLUNG'),
    ('GFA_MODUL_GLEICHWERTIGKEIT'),
    ('GFA_MODUL_KMU'),
    ('GFA_MODUL_PREISE'),
    ('GFA_MODUL_SONSTIGE_KOSTEN'),
    ('GFA_MODUL_VERBRAUCHER'),
    ('GFA_MODUL_WEITERE');

INSERT INTO `aktionen` (`bezeichnung`) VALUES
    ('ERGEBNISDOKUMENTATION_LESEN');