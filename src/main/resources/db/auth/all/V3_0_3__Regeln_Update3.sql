-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'RV-Ersteller darf in RV ein Dokument erstellen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'DOKUMENTE_ERSTELLEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
	);
