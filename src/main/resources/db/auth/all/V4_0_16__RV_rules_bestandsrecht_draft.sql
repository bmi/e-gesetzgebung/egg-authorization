-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf für RV (in Entwurf) Bestandsrecht aus Neu-RIS importieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'IMPORT_NEURIS'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'IN_ENTWURF')
    );