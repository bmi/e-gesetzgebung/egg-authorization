-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT IGNORE INTO `status` (`bezeichnung`)
VALUES ('BESTANDSRECHT')
;

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Ersteller darf Regelungstext als BESTANDSRECHT lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
      (SELECT id FROM status WHERE bezeichnung = 'BESTANDSRECHT')
    );

