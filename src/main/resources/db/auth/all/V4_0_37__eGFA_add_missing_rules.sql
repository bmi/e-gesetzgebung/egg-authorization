-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf die Berechtigungen der GFA ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf die Berechtigungen der GFA lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf die Berechtigungen der GFA lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Beobachtung darf die Berechtigungen der GFA lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GAST'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );