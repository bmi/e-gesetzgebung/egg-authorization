-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `rollen` (`bezeichnung`) VALUES('FEDERFUEHRER');

INSERT INTO `status` (`bezeichnung`) VALUES ('IN_ENTWURF');
INSERT INTO `status` (`bezeichnung`) VALUES ('IN_BEARBEITUNG');
INSERT INTO `status` (`bezeichnung`) VALUES ('ABGESCHLOSSEN');
INSERT INTO `status` (`bezeichnung`) VALUES ('ABGEBROCHEN');

INSERT INTO `aktionen` (`bezeichnung`) VALUES ('AN_PKP_SENDEN');
INSERT INTO `aktionen` (`bezeichnung`) VALUES ('LOESCHEN');
INSERT INTO `aktionen` (`bezeichnung`) VALUES ('ARCHIVIEREN');
INSERT INTO `aktionen` (`bezeichnung`) VALUES ('FARBE_AENDERN');
INSERT INTO `aktionen` (`bezeichnung`) VALUES ('AUSWAEHLEN');

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV im Entwurf schreiben',
      (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'IN_ENTWURF')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV in Bearbeitung schreiben',
      (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'IN_BEARBEITUNG')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV an PKP senden',
      (SELECT id FROM aktionen WHERE bezeichnung = 'AN_PKP_SENDEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'IN_BEARBEITUNG')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV löschen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LOESCHEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'IN_ENTWURF')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV archivieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'ARCHIVIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'IN_BEARBEITUNG')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV-Farbe ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'FARBE_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV auswählen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'AUSWAEHLEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'IN_BEARBEITUNG')
    );