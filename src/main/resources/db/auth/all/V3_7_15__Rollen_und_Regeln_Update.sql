-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- ---------- Rollen -------------------------
INSERT IGNORE INTO `rollen` (`bezeichnung`) VALUES
	('SCHREIBER');

-- ---------- Dokumentenmappen ---------------
INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf DRAFT Mappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf DRAFT Mappe schreiben',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf FINAL Mappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf FINAL Mappe schreiben',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf FREEZE Mappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf FREEZE Mappe schreiben',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    );

 -- ---------- Dokumente ----------------------
INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf DRAFT Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf DRAFT Dokument schreiben',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf FINAL Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf FINAL Dokument schreiben',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf FREEZE Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'SCHREIBER darf FREEZE Dokument schreiben',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    );