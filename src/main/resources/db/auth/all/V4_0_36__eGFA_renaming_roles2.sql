-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

UPDATE rollen SET bezeichnung = 'EGFA_MODUL_MITARBEIT' where bezeichnung = 'GFA_MODUL_MITARBEIT';
UPDATE rollen SET bezeichnung = 'EGFA_MODUL_BEOBACHTUNG' where bezeichnung = 'GFA_MODUL_BEOBACHTUNG';