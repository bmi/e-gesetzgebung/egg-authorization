-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

DELETE FROM regeln r WHERE
	r.aktion_id = (SELECT a.id FROM aktionen a WHERE a.bezeichnung = 'BERECHTIGUNGEN_AENDERN')
	AND
	r.rolle_id = (SELECT ro.id FROM rollen ro WHERE ro.bezeichnung = 'EGFA_MODUL_MITARBEIT');