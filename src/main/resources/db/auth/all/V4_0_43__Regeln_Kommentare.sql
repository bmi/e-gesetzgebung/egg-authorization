-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

DELETE FROM regeln WHERE
 aktion_id=(SELECT id FROM aktionen WHERE bezeichnung='KOMMENTAR_ANTWORTEN');

DELETE FROM aktionen WHERE bezeichnung='KOMMENTAR_ANTWORTEN';

DELETE FROM regeln WHERE
 aktion_id=(SELECT id FROM aktionen WHERE bezeichnung='KOMMENTIEREN') AND
 ressource_typ_id=(SELECT id FROM ressource_typen WHERE bezeichnung='DOKUMENTE');

DELETE FROM regeln WHERE
 aktion_id=(SELECT id FROM aktionen WHERE bezeichnung='KOMMENTIEREN') AND
 ressource_typ_id=(SELECT id FROM ressource_typen WHERE bezeichnung='DOKUMENTENMAPPE');

INSERT IGNORE INTO `aktionen` (`bezeichnung`) VALUES ('KOMMENTARE_LESEN');

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ('ERSTELLER darf DRAFT Dokumentenmappe kommentieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
      (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ('ERSTELLER darf FREEZE Dokumentenmappe kommentieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
      (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ('ERSTELLER darf FINAL Dokumentenmappe Kommentare lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTARE_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
      (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ('LESER darf DRAFT Dokumentenmappe kommentieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
      (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ('LESER darf FINAL Dokumentenmappe Kommentare lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTARE_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
      (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ('MITARBEITER darf FREEZE Dokumentenmappe kommentieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ('MITARBEITER darf FINAL Dokumentenmappe Kommentare lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTARE_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );