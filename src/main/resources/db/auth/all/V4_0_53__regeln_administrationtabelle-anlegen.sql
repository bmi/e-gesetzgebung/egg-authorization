-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `regeln_administration` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `aktion_id` bigint NOT NULL,
  `rolle_id` bigint NOT NULL,
  `angefragte_rolle_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_regeln_administration` (`aktion_id`,`rolle_id`,`angefragte_rolle_id`),
  KEY `IK_regeln_administration` (`aktion_id`,`rolle_id`,`angefragte_rolle_id`),
  CONSTRAINT `FK_regeln_administration_aktion_id_aktionen` FOREIGN KEY (`aktion_id`) REFERENCES `aktionen` (`id`),
  CONSTRAINT `FK_regeln_administration_rolle_id_rollen` FOREIGN KEY (`rolle_id`) REFERENCES `rollen` (`id`)
) DEFAULT CHARSET=utf8mb4;