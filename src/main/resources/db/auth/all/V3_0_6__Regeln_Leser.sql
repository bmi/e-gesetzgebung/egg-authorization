-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `rollen` (`bezeichnung`) VALUES ('LESER');

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'LESER darf DRAFT Mappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'LESER darf FINAL Mappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'LESER darf FREEZE Mappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'LESER darf DRAFT Dokumente lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'LESER darf FINAL Dokumente lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'FINAL')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'LESER darf FREEZE Dokumente lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    );