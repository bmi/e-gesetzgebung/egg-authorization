-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT IGNORE INTO `status` (`bezeichnung`) VALUES ('AENDERUNG_GEWUENSCHT_PKP');
INSERT IGNORE INTO `status` (`bezeichnung`) VALUES ('AENDERUNG_GEWUENSCHT_FEDERFUEHRER');

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Teilnehmer einer HRA darf AENDERUNG_GEWUENSCHT_PKP Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Ersteller darf in AENDERUNG_GEWUENSCHT_PKP Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('LESER darf AENDERUNG_GEWUENSCHT_PKP Dokumente lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('SCHREIBER darf AENDERUNG_GEWUENSCHT_PKP Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Teilnehmer einer HRA darf AENDERUNG_GEWUENSCHT_PKP Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Ersteller darf in AENDERUNG_GEWUENSCHT_PKP Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('LESER darf AENDERUNG_GEWUENSCHT_PKP Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('SCHREIBER darf AENDERUNG_GEWUENSCHT_PKP Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('ERSTELLER darf AENDERUNG_GEWUENSCHT_PKP Dokumentenmappe kommentieren',
     (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('MITARBEITER darf AENDERUNG_GEWUENSCHT_PKP Dokumentenmappe kommentieren',
     (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Ersteller darf AENDERUNG_GEWUENSCHT_PKP Mappe Neue Version erstellen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'NEUE_VERSION_ERSTELLEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Teilnehmer einer HRA darf AENDERUNG_GEWUENSCHT_PKP Mappe Neue Version erstellen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'NEUE_VERSION_ERSTELLEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Ersteller darf in AENDERUNG_GEWUENSCHT_PKP Dokumentenstatus ändern',
     (SELECT id FROM aktionen WHERE bezeichnung = 'DOKUMENTENMAPPEN_STATUS_AENDERN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_PKP')
    );


INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Teilnehmer einer HRA darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Ersteller darf in AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('LESER darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokumente lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('SCHREIBER darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Teilnehmer einer HRA darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Ersteller darf in AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('LESER darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'LESER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('SCHREIBER darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'SCHREIBER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('ERSTELLER darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokumentenmappe kommentieren',
     (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('MITARBEITER darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokumentenmappe kommentieren',
     (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Ersteller darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Mappe Neue Version erstellen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'NEUE_VERSION_ERSTELLEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Teilnehmer einer HRA darf AENDERUNG_GEWUENSCHT_FEDERFUEHRER Mappe Neue Version erstellen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'NEUE_VERSION_ERSTELLEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	('Ersteller darf in AENDERUNG_GEWUENSCHT_FEDERFUEHRER Dokumentenstatus ändern',
     (SELECT id FROM aktionen WHERE bezeichnung = 'DOKUMENTENMAPPEN_STATUS_AENDERN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'AENDERUNG_GEWUENSCHT_FEDERFUEHRER')
    );

