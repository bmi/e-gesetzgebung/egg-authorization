-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

ALTER TABLE `nutzer` ADD COLUMN `rolle_id` bigint;
ALTER TABLE `nutzer` ADD CONSTRAINT `FK_nutzer__rolle_id` FOREIGN KEY (rolle_id) REFERENCES rollen (id);

INSERT INTO `rollen` (`bezeichnung`) VALUES ('ADMINISTRATION');
INSERT INTO `rollen` (`bezeichnung`) VALUES ('SACHBEARBEITUNG');
INSERT INTO `rollen` (`bezeichnung`) VALUES ('BUNDESTAGSFRAKTION');
INSERT INTO `rollen` (`bezeichnung`) VALUES ('ADMINISTRATION_UND_SACHBEARBEITUNG');