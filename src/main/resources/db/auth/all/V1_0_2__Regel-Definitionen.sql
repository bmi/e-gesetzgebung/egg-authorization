-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'Ersteller darf DRAFT Mappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
	),
	( 'Ersteller darf FREEZE Mappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    ),
	( 'Ersteller darf DRAFT Mappe schreiben',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
	),
	( 'Ersteller darf in DRAFT Dokument schreiben',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
	),
	( 'Ersteller darf in FREEZE Dokument kommentieren',
     (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
	),
	( 'Ersteller darf in FREEZE Dokument Antworten kommentieren',
     (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTAR_ANTWORTEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
	),
	( 'Ersteller darf DRAFT Mappe suchen und ersetzen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SUCHENundERSETZEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
  	),
	( 'RV-Teilnehmer darf in RV ein Dokument erstellen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'DOKUMENTE_ERSTELLEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
	),
    ( 'Teilnehmer einer HRA darf FREEZE Dokument lesen',
       (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
       (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
       (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
       (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
    ),
	( 'Teilnehmer einer HRA darf FREEZE Dokumentenmappe kommentieren',
     (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTIEREN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
	),
	( 'Teilnehmer einer HRA darf FREEZE Dokumentenmappe Antworten kommentieren',
     (SELECT id FROM aktionen WHERE bezeichnung = 'KOMMENTAR_ANTWORTEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
	),
	( 'Teilnehmer einer HRA darf FREEZE Dokumentenmappe lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
	);
