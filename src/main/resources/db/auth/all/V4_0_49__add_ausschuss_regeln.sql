-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- add read/write rules for ausschusssekretariat and ausschussmitglied

INSERT INTO `ressource_typen` (`bezeichnung`) VALUES ('UEBERWEISUNG_AUSSCHUSS');

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ('AUSSCHUSSSEKRETARIAT darf Eigenschaften LESEN in UEBERWEISUNG_AUSSCHUSS',
    (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'UEBERWEISUNG_AUSSCHUSS'),
    (SELECT id FROM rollen WHERE bezeichnung = 'AUSSCHUSSSEKRETARIAT'),
    (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
);

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
     ('AUSSCHUSSMITGLIED darf Eigenschaften LESEN in UEBERWEISUNG_AUSSCHUSS',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'UEBERWEISUNG_AUSSCHUSS'),
     (SELECT id FROM rollen WHERE bezeichnung = 'AUSSCHUSSMITGLIED'),
     (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
);

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
     ('AUSSCHUSSSEKRETARIAT darf Eigenschaften SCHREIBEN in UEBERWEISUNG_AUSSCHUSS',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'UEBERWEISUNG_AUSSCHUSS'),
     (SELECT id FROM rollen WHERE bezeichnung = 'AUSSCHUSSSEKRETARIAT'),
     (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
);

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
     ('AUSSCHUSSMITGLIED darf Eigenschaften SCHREIBEN in UEBERWEISUNG_AUSSCHUSS',
     (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'UEBERWEISUNG_AUSSCHUSS'),
     (SELECT id FROM rollen WHERE bezeichnung = 'AUSSCHUSSMITGLIED'),
     (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
);
