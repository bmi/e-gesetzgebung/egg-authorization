-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'Ersteller darf DRAFT Mappe Neue Version erstellen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'NEUE_VERSION_ERSTELLEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
	),
    ( 'Teilnehmer einer HRA darf DRAFT Mappe Neue Version erstellen',
       (SELECT id FROM aktionen WHERE bezeichnung = 'NEUE_VERSION_ERSTELLEN'),
       (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
       (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
       (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
    );
