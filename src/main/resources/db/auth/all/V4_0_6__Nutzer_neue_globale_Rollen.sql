-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `rollen` (`bezeichnung`) VALUES ('AUSSCHUSSSEKRETARIAT');
INSERT INTO `rollen` (`bezeichnung`) VALUES ('AUSSCHUSSMITGLIED');
INSERT INTO `rollen` (`bezeichnung`) VALUES ('PD1_ANNAHMESTELLE');
INSERT INTO `rollen` (`bezeichnung`) VALUES ('PD1_LEKTORAT');