-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Lektorat (PD1) darf Eigenschaften LESEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'), (SELECT id FROM ressource_typen WHERE bezeichnung = 'DRUCKSACHE'), (SELECT id FROM rollen WHERE bezeichnung = 'PD1_LEKTORAT'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Lektorat (PD1) darf Eigenschaften SCHREIBEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'), (SELECT id FROM ressource_typen WHERE bezeichnung = 'DRUCKSACHE'), (SELECT id FROM rollen WHERE bezeichnung = 'PD1_LEKTORAT'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Lektorat (PD1) darf Eigenschaften LOESCHEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'LOESCHEN'), (SELECT id FROM ressource_typen WHERE bezeichnung = 'DRUCKSACHE'), (SELECT id FROM rollen WHERE bezeichnung = 'PD1_LEKTORAT'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));