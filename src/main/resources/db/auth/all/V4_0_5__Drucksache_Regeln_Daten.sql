-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO ressource_typen
(bezeichnung)
VALUES('DRUCKSACHE');

set @drucksache_id = last_insert_id();

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Sachbearbeitung (PD1) darf Eigenschaften LESEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'), @drucksache_id, (SELECT id FROM rollen WHERE bezeichnung = 'SACHBEARBEITUNG'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Sachbearbeitung (PD1) darf Eigenschaften SCHREIBEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'), @drucksache_id, (SELECT id FROM rollen WHERE bezeichnung = 'SACHBEARBEITUNG'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Sachbearbeitung (PD1) darf Eigenschaften LOESCHEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'LOESCHEN'), @drucksache_id, (SELECT id FROM rollen WHERE bezeichnung = 'SACHBEARBEITUNG'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));


INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Administration und Sachbearbeitung (PD1) darf Eigenschaften LESEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'), @drucksache_id, (SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION_UND_SACHBEARBEITUNG'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Administration und Sachbearbeitung (PD1) darf Eigenschaften SCHREIBEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'), @drucksache_id, (SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION_UND_SACHBEARBEITUNG'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Administration und Sachbearbeitung (PD1) darf Eigenschaften LOESCHEN in der Drucksache', (SELECT id FROM aktionen WHERE bezeichnung = 'LOESCHEN'), @drucksache_id, (SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION_UND_SACHBEARBEITUNG'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));