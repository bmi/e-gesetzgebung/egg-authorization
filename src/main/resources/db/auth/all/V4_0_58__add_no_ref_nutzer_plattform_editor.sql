-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

ALTER TABLE nutzer ADD COLUMN plattform_no_ref bit(1) NOT NULL;
ALTER TABLE nutzer ADD COLUMN editor_no_ref bit(1) NOT NULL;