-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- delete rules

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_DEMOGRAFIE') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_DISABILITY') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_EAOEHH') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_ENAP') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_ERFUELLUNGSAUFWAND') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_EVALUIERUNG') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_GLEICHSTELLUNG') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_GLEICHWERTIGKEIT') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_KMU') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_PREISE') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_SONSTIGE_KOSTEN') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_VERBRAUCHER') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

DELETE FROM `regeln` WHERE `ressource_typ_id` =
    (SELECT id FROM ressource_typen WHERE bezeichnung = 'EGFA_MODUL_WEITERE') AND
    `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

-- delete zuordnungen

DELETE FROM `zuordnungen` WHERE `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_BEOBACHTUNG');

-- delete role

DELETE FROM `rollen` WHERE `bezeichnung` = 'EGFA_MODUL_BEOBACHTUNG';