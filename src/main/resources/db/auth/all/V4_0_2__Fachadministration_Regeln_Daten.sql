-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- BTEG-261/693-Regeln für Fachadministration


INSERT INTO aktionen
(bezeichnung)
VALUES('ZUWEISEN');

set @aktion_zuweisen_id = last_insert_id();

INSERT INTO aktionen
(bezeichnung)
VALUES('LADEN');

set @aktion_laden_id = last_insert_id();

INSERT INTO ressource_typen
(bezeichnung)
VALUES('FACHADMINISTRATION');

set @fachadministration_id = last_insert_id();

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Administrator darf Dokumente LADEN in der Fachadministration', @aktion_zuweisen_id, @fachadministration_id, (SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Administrator darf Dokumente ZUWEISEN in der Fachadministration',  @aktion_laden_id, @fachadministration_id, (SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Sachbearbeiter darf Dokumente LADEN in der Fachadministration',  @aktion_zuweisen_id, @fachadministration_id, (SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION_UND_SACHBEARBEITUNG'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT INTO regeln
(bezeichnung, aktion_id, ressource_typ_id, rolle_id, status_id)
VALUES('Sachbearbeiter darf Dokumente ZUWEISEN in der Fachadministration', @aktion_laden_id, @fachadministration_id, (SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION_UND_SACHBEARBEITUNG'), (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));




