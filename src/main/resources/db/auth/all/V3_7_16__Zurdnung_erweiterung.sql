-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

ALTER TABLE `zuordnungen` ADD COLUMN `deaktiviert` BOOLEAN DEFAULT false;
ALTER TABLE `zuordnungen` ADD COLUMN `befristung` DATE;
ALTER TABLE `zuordnungen` ADD COLUMN `fallback_zuordnung` BIGINT DEFAULT NULL;
ALTER TABLE `zuordnungen` ADD CONSTRAINT `FK_fallback_zuordnungen` FOREIGN KEY (`fallback_zuordnung`) REFERENCES `zuordnungen` (`id`);
