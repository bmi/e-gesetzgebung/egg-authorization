-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT IGNORE INTO `rollen` (`bezeichnung`)
VALUES ('BR_FACHADMINISTRATION_P1'),
       ('BR_P1'),
       ('BR_FACHADMINISTRATION_AUSSCHUESSE'),
       ('BR_AUSSCHUSSMITGLIED'),
       ('BR_BUNDESRAT');

DELETE FROM rollen WHERE bezeichnung = 'BUNDESRAT';