-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

DELETE FROM `regeln` WHERE `aktion_id` = (SELECT id FROM aktionen WHERE bezeichnung = 'ARCHIVIEREN')
                       AND `ressource_typ_id` = (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN')
                       AND `rolle_id` = (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER');