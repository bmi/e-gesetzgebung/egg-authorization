-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `rollen` (`bezeichnung`) VALUES('GAST');

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Gast darf RV lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'GAST'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Gast darf RV archivieren',
      (SELECT id FROM aktionen WHERE bezeichnung = 'ARCHIVIEREN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'Gast'),
      (SELECT id FROM status WHERE bezeichnung = 'IN_BEARBEITUNG')
    );