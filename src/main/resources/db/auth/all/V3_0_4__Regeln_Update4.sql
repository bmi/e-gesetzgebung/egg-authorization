-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO `aktionen` (`bezeichnung`) VALUES
	('DOKUMENTENMAPPEN_STATUS_AENDERN');

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	( 'Ersteller darf in DRAFT Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
	),
	( 'Ersteller darf in FREEZE Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
	),
	( 'Ersteller darf in FINAL Dokument lesen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'FINAL')
	),
	( 'Ersteller darf in FREEZE Dokumentenstatus ändern',
     (SELECT id FROM aktionen WHERE bezeichnung = 'DOKUMENTENMAPPEN_STATUS_AENDERN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'FREEZE')
	),
	( 'Ersteller darf FINAL Mappe Neue Version erstellen',
     (SELECT id FROM aktionen WHERE bezeichnung = 'NEUE_VERSION_ERSTELLEN'),
     (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
     (SELECT id FROM rollen WHERE bezeichnung = 'ERSTELLER'),
     (SELECT id FROM status WHERE bezeichnung = 'FINAL')
	);

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
	    ( 'Teilnehmer einer HRA darf DRAFT Dokument lesen',
           (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
           (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
           (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
           (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
        ),
        ( 'Teilnehmer einer HRA darf FINAL Dokument lesen',
           (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
           (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTE'),
           (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
           (SELECT id FROM status WHERE bezeichnung = 'FINAL')
        ),
        ( 'Teilnehmer einer HRA darf FINAL Dokumentenmappe lesen',
           (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
           (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
           (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
           (SELECT id FROM status WHERE bezeichnung = 'FINAL')
        ),
        ( 'Teilnehmer einer HRA darf DRAFT Dokumentenmappe lesen',
           (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
           (SELECT id FROM ressource_typen WHERE bezeichnung = 'DOKUMENTENMAPPE'),
           (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
           (SELECT id FROM status WHERE bezeichnung = 'DRAFT')
        );