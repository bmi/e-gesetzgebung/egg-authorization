-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

UPDATE rollen SET bezeichnung = 'BT_FACHADMINISTRATION_PD1' where bezeichnung = 'BT_Fachadministration_PD1';
UPDATE rollen SET bezeichnung = 'BT_FACHADMINISTRATION_AUSSCHUESSE' where bezeichnung = 'BT_Fachadministration_Ausschüsse';
