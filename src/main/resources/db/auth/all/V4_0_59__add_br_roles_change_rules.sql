-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- BR_ADMINISTRATION has combined rights of all BR_FACHADMINISTRATION roles
INSERT IGNORE INTO `rollen` (`bezeichnung`)
VALUES ('BR_ADMINISTRATION');

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`)
VALUES ('BR_ADMINISTRATION darf LADEN in der Fachadministration',
        (SELECT id FROM aktionen WHERE bezeichnung = 'LADEN'),
        (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION'),
        (SELECT id FROM rollen WHERE bezeichnung = 'BR_ADMINISTRATION'),
        (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`)
VALUES ('BR_ADMINISTRATION darf ZUWEISEN in der Fachadministration',
        (SELECT id FROM aktionen WHERE bezeichnung = 'ZUWEISEN'),
        (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION'),
        (SELECT id FROM rollen WHERE bezeichnung = 'BR_ADMINISTRATION'),
        (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

-- BR_BUNDESRAT shall have equals rights as ADMINISTRATION; BR_BUNDESRAT is a development only role; Bundesrat shall not
    -- use ADMINISTRATION anymore

-- As of 06-SEP-2024 the isAllowedGlobal check allows the role to do the action on every resource,
    -- disregarding the ressource_typen
INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`)
VALUES ('BR_BUNDESRAT darf Regelungsvorhaben lesen',
        (SELECT id FROM aktionen WHERE bezeichnung = 'LESEN'),
        (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
        (SELECT id FROM rollen WHERE bezeichnung = 'BR_BUNDESRAT'),
        (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`)
VALUES ('BR_BUNDESRAT darf Regelungsvorhaben schreiben',
        (SELECT id FROM aktionen WHERE bezeichnung = 'SCHREIBEN'),
        (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
        (SELECT id FROM rollen WHERE bezeichnung = 'BR_BUNDESRAT'),
        (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`)
VALUES ('BR_BUNDESRAT darf LADEN in der Fachadministration',
        (SELECT id FROM aktionen WHERE bezeichnung = 'LADEN'),
        (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION'),
        (SELECT id FROM rollen WHERE bezeichnung = 'BR_BUNDESRAT'),
        (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

INSERT IGNORE INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`)
VALUES ('BR_BUNDESRAT darf ZUWEISEN in der Fachadministration',
        (SELECT id FROM aktionen WHERE bezeichnung = 'ZUWEISEN'),
        (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION'),
        (SELECT id FROM rollen WHERE bezeichnung = 'BR_BUNDESRAT'),
        (SELECT id FROM status WHERE bezeichnung = 'DONT CARE'));

UPDATE nutzer
SET rolle_id = (SELECT id FROM rollen WHERE bezeichnung = 'BR_BUNDESRAT')
WHERE email = 'abc.cde@example.com';

UPDATE nutzer
SET rolle_id = (SELECT id FROM rollen WHERE bezeichnung = 'BR_ADMINISTRATION')
WHERE email = 'abc.cde@example.com';

DELETE
FROM regeln
WHERE bezeichnung = 'LÖSCH MICH, DIES IST EIN TEST FÜR GLOBALE ROLLEN';
