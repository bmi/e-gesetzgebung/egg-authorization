-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

update regeln set bezeichnung = REPLACE(bezeichnung, 'LADEN', 'ZUWEISEN ') where aktion_id=(SELECT id FROM aktionen WHERE bezeichnung = 'ZUWEISEN') and rolle_id=(SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION');
update regeln set bezeichnung = REPLACE(bezeichnung, 'ZUWEISEN', 'LADEN ') where aktion_id=(SELECT id FROM aktionen WHERE bezeichnung = 'LADEN') and rolle_id=(SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION');
update regeln set bezeichnung = REPLACE(bezeichnung, 'LADEN', 'ZUWEISEN  ') where aktion_id=(SELECT id FROM aktionen WHERE bezeichnung = 'ZUWEISEN') and rolle_id=(SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION_UND_SACHBEARBEITUNG');
update regeln set bezeichnung = REPLACE(bezeichnung, 'ZUWEISEN', 'LADEN  ') where aktion_id=(SELECT id FROM aktionen WHERE bezeichnung = 'LADEN') and rolle_id=(SELECT id FROM rollen WHERE bezeichnung = 'ADMINISTRATION_UND_SACHBEARBEITUNG');