-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

DELETE FROM `regeln` WHERE
    bezeichnung = 'Mitarbeiter darf für RV (Entwurf) Bestandsrecht aus Neu-RIS importieren';

DELETE FROM `regeln` WHERE
    bezeichnung = 'Federführer darf für RV (Entwurf) Bestandsrecht aus Neu-RIS importieren';

INSERT INTO `aktionen` (`bezeichnung`) VALUES ('BESTANDSRECHT_LESEN');

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf RV Bestandsrecht lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BESTANDSRECHT_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf RV Bestandsrecht lesen',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BESTANDSRECHT_LESEN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'REGELUNGSVORHABEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );