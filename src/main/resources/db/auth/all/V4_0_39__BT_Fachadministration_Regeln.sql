-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- BTEG-926-Regeln für BT_Fachadministration

set @aktion_zuweisen_id = (SELECT id FROM aktionen WHERE bezeichnung = 'ZUWEISEN');

set @aktion_laden_id = (SELECT id FROM aktionen WHERE bezeichnung = 'LADEN');

set @fachadministration_id = (SELECT id FROM ressource_typen WHERE bezeichnung = 'FACHADMINISTRATION');

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'BT_FACHADMINISTRATION_PD1 darf Dokumente LADEN in der Fachadministration',
      @aktion_zuweisen_id, @fachadministration_id,
      (SELECT id FROM rollen WHERE bezeichnung = 'BT_Fachadministration_PD1'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'BT_FACHADMINISTRATION_PD1 darf Dokumente ZUWEISEN in der Fachadministration',
      @aktion_laden_id, @fachadministration_id,
      (SELECT id FROM rollen WHERE bezeichnung = 'BT_Fachadministration_PD1'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'BT_FACHADMINISTRATION_AUSSCHUESSE darf Dokumente LADEN in der Fachadministration',
      @aktion_zuweisen_id, @fachadministration_id,
      (SELECT id FROM rollen WHERE bezeichnung = 'BT_Fachadministration_Ausschüsse'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'BT_FACHADMINISTRATION_AUSSCHUESSE darf Dokumente ZUWEISEN in der Fachadministration',
      @aktion_laden_id, @fachadministration_id,
      (SELECT id FROM rollen WHERE bezeichnung = 'BT_Fachadministration_Ausschüsse'),
      (SELECT id FROM status WHERE bezeichnung = 'DONT CARE')
    );
