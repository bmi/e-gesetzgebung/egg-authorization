-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- original Query which works in DBeaver but not with Flyway
-- UPDATE regeln r
-- JOIN status s ON r.status_id = s.id
-- JOIN ressource_typen rt ON r.ressource_typ_id = rt.id
-- JOIN aktionen a ON r.aktion_id = a.id
-- SET r.status_id = (SELECT id from status where bezeichnung = 'AKTIV')
-- WHERE
--     a.bezeichnung = 'BERECHTIGUNGEN_AENDERN' AND
--     rt.bezeichnung IN (
-- 	    'GFA_MODUL_DEMOGRAFIE',
-- 	    'GFA_MODUL_DISABILITY',
--	    'GFA_MODUL_EAOEHH',
--	    'GFA_MODUL_ENAP',
--	    'GFA_MODUL_ERFUELLUNGSAUFWAND',
--	    'GFA_MODUL_EVALUIERUNG',
--	    'GFA_MODUL_GLEICHSTELLUNG',
--	    'GFA_MODUL_GLEICHWERTIGKEIT',
--	    'GFA_MODUL_KMU',
--	    'GFA_MODUL_PREISE',
--	    'GFA_MODUL_SONSTIGE_KOSTEN',
--	    'GFA_MODUL_VERBRAUCHER',
--	    'GFA_MODUL_WEITERE',
--	    'GFA_MODUL_EXPERIMENTIERKLAUSELN'
-- );

DELETE FROM regeln r WHERE
	r.aktion_id = (SELECT a.id FROM aktionen a WHERE a.bezeichnung = 'BERECHTIGUNGEN_AENDERN')
	AND
	r.ressource_typ_id in (SELECT rt.id FROM ressource_typen rt WHERE rt.bezeichnung in (
	    'GFA_MODUL_DEMOGRAFIE',
	    'GFA_MODUL_DISABILITY',
	    'GFA_MODUL_EAOEHH',
	    'GFA_MODUL_ENAP',
	    'GFA_MODUL_ERFUELLUNGSAUFWAND',
	    'GFA_MODUL_EVALUIERUNG',
	    'GFA_MODUL_GLEICHSTELLUNG',
	    'GFA_MODUL_GLEICHWERTIGKEIT',
	    'GFA_MODUL_KMU',
	    'GFA_MODUL_PREISE',
	    'GFA_MODUL_SONSTIGE_KOSTEN',
	    'GFA_MODUL_VERBRAUCHER',
	    'GFA_MODUL_WEITERE',
	    'GFA_MODUL_EXPERIMENTIERKLAUSELN'
	));

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Demografie im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_DEMOGRAFIE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Demografie im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_DEMOGRAFIE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Demografie im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_DEMOGRAFIE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Disability im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_DISABILITY'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Disability im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_DISABILITY'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Disability im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_DISABILITY'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: EaOehh im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EAOEHH'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: EaOehh im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EAOEHH'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: EaOehh im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EAOEHH'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Enap im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_ENAP'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Enap im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_ENAP'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Enap im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_ENAP'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Erfüllungsaufwand im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_ERFUELLUNGSAUFWAND'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Erfüllungsaufwand im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_ERFUELLUNGSAUFWAND'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Erfüllungsaufwand im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_ERFUELLUNGSAUFWAND'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Evaluierung im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EVALUIERUNG'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Evaluierung im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EVALUIERUNG'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Evaluierung im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EVALUIERUNG'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Gleichstellung im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_GLEICHSTELLUNG'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Gleichstellung im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_GLEICHSTELLUNG'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Gleichstellung im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_GLEICHSTELLUNG'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Gleichwertigkeit im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_GLEICHWERTIGKEIT'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Gleichwertigkeit im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_GLEICHWERTIGKEIT'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Gleichwertigkeit im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_GLEICHWERTIGKEIT'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: KMU im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_KMU'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: KMU im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_KMU'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: KMU im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_KMU'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Preise im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_PREISE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Preise im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_PREISE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Preise im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_PREISE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Sonstige Kosten im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Sonstige Kosten im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Sonstige Kosten im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_SONSTIGE_KOSTEN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Verbraucher im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_VERBRAUCHER'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Verbraucher im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_VERBRAUCHER'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Verbraucher im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_VERBRAUCHER'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Weitere im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_WEITERE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Weitere im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_WEITERE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Weitere im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_WEITERE'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Federführer darf im GFA Module: Experimentierklauseln im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EXPERIMENTIERKLAUSELN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'FEDERFUEHRER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Mitarbeiter darf im GFA Module: Experimentierklauseln im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EXPERIMENTIERKLAUSELN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'MITARBEITER'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );

INSERT INTO `regeln` (`bezeichnung`, `aktion_id`, `ressource_typ_id`, `rolle_id`, `status_id`) VALUES
    ( 'Modul Mitarbeiter darf im GFA Module: Experimentierklauseln im Status AKTIV die Berechtigungen ändern',
      (SELECT id FROM aktionen WHERE bezeichnung = 'BERECHTIGUNGEN_AENDERN'),
      (SELECT id FROM ressource_typen WHERE bezeichnung = 'GFA_MODUL_EXPERIMENTIERKLAUSELN'),
      (SELECT id FROM rollen WHERE bezeichnung = 'EGFA_MODUL_MITARBEIT'),
      (SELECT id FROM status WHERE bezeichnung = 'AKTIV')
    );