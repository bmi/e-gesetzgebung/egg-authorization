-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

DELETE FROM status WHERE bezeichnung='ABGESCHLOSSEN';
DELETE FROM status WHERE bezeichnung='ABGEBROCHEN';

INSERT INTO `status` (`bezeichnung`) VALUES ('ARCHIVIERT');