-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

DELETE FROM regeln WHERE
 rolle_id=(SELECT id FROM rollen WHERE bezeichnung='SCHREIBER') AND
 aktion_id=(SELECT id FROM aktionen WHERE bezeichnung='SCHREIBEN' ) AND
 status_id=(SELECT id FROM status WHERE bezeichnung='FINAL');

DELETE FROM regeln WHERE
 rolle_id=(SELECT id FROM rollen WHERE bezeichnung='SCHREIBER') AND
 aktion_id=(SELECT id FROM aktionen WHERE bezeichnung='SCHREIBEN' ) AND
 status_id=(SELECT id FROM status WHERE bezeichnung='FREEZE');