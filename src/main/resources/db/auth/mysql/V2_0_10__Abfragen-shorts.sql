-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

DROP PROCEDURE IF EXISTS `PROC_SHOW_BENUTZER_RESSOURCEN`;
DELIMITER //
CREATE PROCEDURE `PROC_SHOW_BENUTZER_RESSOURCEN`()
BEGIN

  SELECT
     zu.benutzer_id AS Benutzer, rt.bezeichnung AS Ressourcetyp, zu.ressource_id AS Ressource_Id, ro.bezeichnung AS Rolle
  FROM zuordnungen zu
  JOIN ressource_typen rt ON zu.ressource_typ_id = rt.id
  JOIN rollen ro ON zu.rolle_id = ro.id
  ORDER BY Benutzer, Ressourcetyp;

END//
DELIMITER ;


DROP PROCEDURE IF EXISTS `PROC_SHOW_REGELN`;
DELIMITER //
CREATE PROCEDURE `PROC_SHOW_REGELN`()
BEGIN

  SELECT
    re.bezeichnung AS TextDarstellung,
    ro.bezeichnung AS Rolle,
    rt.bezeichnung AS RessourceTyp,
    st.bezeichnung AS status,
    ak.bezeichnung AS Aktion
  FROM regeln re
  JOIN aktionen ak ON re.aktion_id = ak.id
  JOIN ressource_typen rt ON re.ressource_typ_id = rt.id
  JOIN rollen ro ON re.rolle_id = ro.id
  JOiN status st ON re.status_id = st.id
  ORDER BY Rolle, RessourceTyp, Status, Aktion;

END//
DELIMITER ;
