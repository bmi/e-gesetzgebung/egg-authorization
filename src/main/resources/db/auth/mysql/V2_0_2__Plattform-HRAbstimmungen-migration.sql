-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- @FlywayIgnore
-- Das Quellschema ist fest codiert !!!
DROP PROCEDURE IF EXISTS `PROC_MIGRATE_PLATTFORM_HRA`;

DELIMITER //

CREATE PROCEDURE PROC_MIGRATE_PLATTFORM_HRA(ressource_typ VARCHAR(50), rolle VARCHAR(50))
BEGIN

    DECLARE var_benutzer_id VARCHAR(50);
    DECLARE var_ressource_id VARCHAR(50);
    DECLARE resource_typ_id INT; -- Konstante
    DECLARE rolle_id INT; -- Konstante
    DECLARE done INT DEFAULT FALSE;

    -- Cursor zum Durchlaufen der Tabelle
    DECLARE cur CURSOR FOR SELECT DISTINCT u.gid AS benutzer_id, ab.dokumentenmappe_id AS ressource_id FROM `eGesetz-dev-editor`.teilnehmer tn
                                                                             JOIN `eGesetz-dev-editor`.user u ON tn.fk_user = u.id
                                                                             JOIN `eGesetz-dev-editor`.abstimmung ab ON ab.id = tn.fk_abstimmung
                                                                             WHERE ab.`status`="IN_ABSTIMMUNG"
                                                                             AND ab.dokumentenmappe_id IS NOT NULL
                                                                             AND u.gid IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    SELECT id INTO resource_typ_id FROM ressource_typen WHERE bezeichnung=ressource_typ;
    SELECT id INTO rolle_id FROM rollen WHERE bezeichnung=rolle;

    -- Erstelle eine temporäre Tabelle, um die Ergebnisse zu speichern
    -- CREATE TEMPORARY TABLE IF NOT EXISTS temp_tabelle (benutzer_id  VARCHAR(1024), bezeichnung VARCHAR(1024), ressource_id VARCHAR(50), ressource_typ_id VARCHAR(50), rolle_id VARCHAR(50));

    -- Öffne den Cursor und lese die Datensätze in die temporäre Tabelle
    OPEN cur;
    read_loop: LOOP
        FETCH cur INTO var_benutzer_id, var_ressource_id;
        IF done THEN
            LEAVE read_loop;
        END IF;

        -- Füge die Daten in die temporäre Tabelle ein
        -- INSERT INTO temp_tabelle (benutzer_id, bezeichnung, ressource_id, ressource_typ_id, rolle_id) VALUES (var_benutzer_id, CONCAT(ressource_typ, var_ressource_id), var_ressource_id, resource_typ_id, rolle_id);
        INSERT INTO zuordnungen (benutzer_id, bezeichnung, ressource_id, ressource_typ_id, rolle_id) VALUES (var_benutzer_id, CONCAT(CONCAT('DOKMAPausHRA', var_ressource_id), var_benutzer_id), var_ressource_id, resource_typ_id, rolle_id);
    END LOOP;

    -- Schließe den Cursor
    CLOSE cur;

    -- Gib die Daten aus der temporären Tabelle aus
    -- SELECT * FROM temp_tabelle;

    -- Lösche die temporäre Tabelle
    -- DROP TEMPORARY TABLE IF EXISTS temp_tabelle;
END //

DELIMITER ;

-- CALL PROC_MIGRATE_PLATTFORM_HRA('DOKUMENTENMAPPE', 'MITARBEITER');