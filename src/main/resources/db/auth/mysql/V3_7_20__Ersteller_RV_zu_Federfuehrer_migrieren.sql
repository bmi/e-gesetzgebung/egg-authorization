-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

INSERT INTO zuordnungen (benutzer_id, bezeichnung, ressource_id, ressource_typ_id, rolle_id, deaktiviert, befristung, fallback_zuordnung)
SELECT benutzer_id, bezeichnung, ressource_id, ressource_typ_id, (SELECT r.id FROM rollen r where r.bezeichnung = 'FEDERFUEHRER'), deaktiviert, befristung, fallback_zuordnung
FROM zuordnungen z
WHERE z.rolle_id = (SELECT r.id FROM rollen r where r.bezeichnung = 'ERSTELLER')
  AND z.ressource_typ_id = (SELECT r.id FROM ressource_typen r where r.bezeichnung = 'REGELUNGSVORHABEN')
  AND NOT EXISTS (SELECT 1 FROM zuordnungen z2 WHERE z2.benutzer_id = z.benutzer_id AND z2.ressource_typ_id = z.ressource_typ_id AND z2.ressource_id = z.ressource_id AND z2.rolle_id = (SELECT r.id FROM rollen r where r.bezeichnung = 'FEDERFUEHRER'));