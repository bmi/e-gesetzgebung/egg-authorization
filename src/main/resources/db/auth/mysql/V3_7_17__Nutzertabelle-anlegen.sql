-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE `nutzer` (
  `id` binary(16) NOT NULL,
  `abteilung` varchar(255) DEFAULT NULL,
  `anrede` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `referat` varchar(255) DEFAULT NULL,
  `gid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `telefon` varchar(255) DEFAULT NULL,
  `titel` varchar(255) DEFAULT NULL,
  `ressort_kurzbezeichnung` varchar(255) DEFAULT NULL,
  `ressort_id` bigint DEFAULT NULL,
  `bearbeitet_am` DATETIME NOT NULL,
  `erstellt_am` DATETIME NOT NULL,
  `last_modified` DATETIME NULL,
  `aktiv` BIT NOT NULL,
  `deleted` BIT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nutzer_gid` (`gid`),
  KEY `IK_nutzer_gid` (`gid`),
  KEY `IK_nutzer_email` (`email`)
) DEFAULT CHARSET=utf8mb4;