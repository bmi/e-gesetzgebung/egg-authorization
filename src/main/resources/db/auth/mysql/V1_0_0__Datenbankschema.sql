-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

CREATE TABLE IF NOT EXISTS `aktionen` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_aktionen_bezeichung` (`bezeichnung`),
  KEY `IK_aktionen_bezeichnung` (`bezeichnung`)
) DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `ressource_typen` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ressource_typen_bezeichung` (`bezeichnung`),
  KEY `IK_ressource_typen_bezeichnung` (`bezeichnung`)
) DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `rollen` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rollen_bezeichung` (`bezeichnung`),
  KEY `IK_rollen_bezeichnung` (`bezeichnung`)
) DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `status` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_status_bezeichung` (`bezeichnung`),
  KEY `IK_status_bezeichnung` (`bezeichnung`)
) DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `regeln` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(255) NOT NULL,
  `aktion_id` bigint NOT NULL,
  `ressource_typ_id` bigint NOT NULL,
  `rolle_id` bigint NOT NULL,
  `status_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_regeln_bezeichnung` (`bezeichnung`),
  UNIQUE KEY `UK_regeln_eintraege` (`ressource_typ_id`,`rolle_id`,`status_id`,`aktion_id`),
  KEY `IK_regeln_eintraege` (`ressource_typ_id`,`rolle_id`,`status_id`,`aktion_id`),
  KEY `FK_aktion_regel__regel_id` (`aktion_id`),
  KEY `FK_rolle_regel__regel_id` (`rolle_id`),
  KEY `FK_status_regel__regel_id` (`status_id`),
  CONSTRAINT `FK_aktion_regel__regel_id` FOREIGN KEY (`aktion_id`) REFERENCES `aktionen` (`id`),
  CONSTRAINT `FK_ressourcetype_regel__regeln_id` FOREIGN KEY (`ressource_typ_id`) REFERENCES `ressource_typen` (`id`),
  CONSTRAINT `FK_rolle_regel__regel_id` FOREIGN KEY (`rolle_id`) REFERENCES `rollen` (`id`),
  CONSTRAINT `FK_status_regel__regel_id` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`)
) DEFAULT CHARSET=utf8mb4;


-- Exportiere Struktur von Tabelle schema_test.zuordnungen
CREATE TABLE IF NOT EXISTS `zuordnungen` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `benutzer_id` varchar(255) NOT NULL,
  `bezeichnung` varchar(255) DEFAULT NULL,
  `ressource_id` varchar(255) NOT NULL,
  `ressource_typ_id` bigint NOT NULL,
  `rolle_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_zuordnungen_eintraege` (`ressource_typ_id`,`rolle_id`,`ressource_id`,`benutzer_id`),
  KEY `IK_zuordnungen` (`ressource_typ_id`,`rolle_id`,`ressource_id`,`benutzer_id`),
  KEY `IK_zuordnungen_rolle` (`rolle_id`),
  CONSTRAINT `FK7jyctnw0digqd6ljxo8qup3o7` FOREIGN KEY (`ressource_typ_id`) REFERENCES `ressource_typen` (`id`),
  CONSTRAINT `FKaasnq4q6wj01qw4us65o117m8` FOREIGN KEY (`rolle_id`) REFERENCES `rollen` (`id`)
) DEFAULT CHARSET=utf8mb4;
