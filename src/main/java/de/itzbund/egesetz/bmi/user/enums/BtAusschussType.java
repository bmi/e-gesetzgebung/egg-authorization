// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.enums;

// different ausschuesse in BT
public enum BtAusschussType {
	PA_3("Auswärtiger Ausschuss"),
	PA_4("Ausschuss für Inneres und Heimat"),
	PA_5("Sportausschuss"),
	PA_6("Rechtsausschuss"),
	PA_7("Finanzausschuss"),
	PA_8("Haushaltsausschuss"),
	PA_9("Wirtschaftsausschuss"),
	PA_10("Ausschuss für Ernährung und Landwirtschaft"),
	PA_11("Ausschuss für Arbeit und Soziales"),
	PA_12("Verteidigungsausschuss"),
	PA_13("Ausschuss für Familie, Senioren, Frauen und Jugend"),
	PA_14("Ausschuss für Gesundheit"),
	PA_15("Verkehrsausschuss"),
	PA_16("Ausschuss für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz"),
	PA_17("Ausschuss für Menschenrechte und humanitäre Hilfe"),
	PA_18("Ausschuss für Bildung, Forschung und Technologiefolgenabschätzung"),
	PA_19("Ausschuss für wirtschaftliche Zusammenarbeit und Entwicklung"),
	PA_20("Ausschuss für Tourismus"),
	PA_22("Ausschuss für Kultur und Medien"),
	PA_23("Ausschuss für Digitales"),
	PA_24("Ausschuss für Wohnen, Stadtentwicklung, Bauwesen und Kommunen"),
	PA_25("Ausschuss für Klimaschutz und Energie");


	private final String label;

	BtAusschussType(String label) {
		this.label = label;
	}

	public String getDescription() {
		return label;
	}
}
