// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.entities;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * represents stellvertreter
 */
@Data
@Entity
@Table(name = "nutzer_stellvertreter")
@IdClass(StellvertreterEntity.StellvertreterPK.class)
@SuperBuilder
@NoArgsConstructor
public class StellvertreterEntity {

	@Data
	public static class StellvertreterPK implements Serializable {

		private UUID nutzerId;
		private UUID stellvertreterId;
	}

	@Id
	@Column(columnDefinition = "UUID")
	private UUID nutzerId;

	@Id
	@Column(columnDefinition = "UUID")
	private UUID stellvertreterId;
}
