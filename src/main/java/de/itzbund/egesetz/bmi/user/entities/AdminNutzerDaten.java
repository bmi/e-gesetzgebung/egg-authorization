// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.entities;

/**
 * provides static data for E-Gesetzgebungs Administrator (system user for provided time planning templates)
 */
public final class AdminNutzerDaten {

	public static final String ADMIN_NAME = "E-Gesetzgebung Admin";
	public static final String ADMIN_GID = "00000000-0000-0000-0000-000000000000";
	public static final String ADMIN_EMAIL = "admin@user.de";
	public static final String ADMIN_TELEFON = "+49-(0)30 18 681-0";
	public static final String ADMIN_ABTEILUNG = "DG";
	public static final String ADMIN_FACHREFERAT = "II 6";

	private AdminNutzerDaten() {
		//intentionally left
	}

	public static NutzerEntity createAdminNutzerEntity() {
		return NutzerEntity.builder()
			.name(ADMIN_NAME)
			.email(ADMIN_EMAIL)
			.telefon(ADMIN_TELEFON)
			.abteilung(ADMIN_ABTEILUNG)
			.referat(ADMIN_FACHREFERAT)
			.gid(ADMIN_GID)
			.deleted(false)
			.aktiv(false)
			.build();
	}

}
