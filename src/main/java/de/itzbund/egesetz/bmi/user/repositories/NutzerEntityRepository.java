// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.repositories;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.user.entities.AdminNutzerDaten;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.enums.BtAusschussType;

import static de.itzbund.egesetz.bmi.user.entities.AdminNutzerDaten.ADMIN_GID;

/**
 * Access to user table
 */
@Qualifier("authDataSource")
@Conditional(EnabledCondition.class)
public interface NutzerEntityRepository extends JpaRepository<NutzerEntity, UUID> {

	default NutzerEntity getOrCreateAdmin() {
		Optional<NutzerEntity> admin = findByGid(ADMIN_GID);
		if (admin.isEmpty()) {
			NutzerEntity newAdmin = AdminNutzerDaten.createAdminNutzerEntity();
			newAdmin.setErstelltAm(Instant.now());
			newAdmin.setBearbeitetAm(Instant.now());
			return save(newAdmin);
		} else {
			return admin.get();
		}
	}

	Optional<NutzerEntity> findByGid(String gid);

	List<NutzerEntity> findAllByGidIn(Collection<String> gids);

	List<NutzerEntity> findAllByEmailIn(Collection<String> emails);

	List<NutzerEntity> findAllByRolleIn(Collection<RolleEntity> rollen);

	@Transactional
	long deleteByIdInAndGidIsNull(Collection<UUID> ids);

	@Transactional
	@Modifying
	@Query("update NutzerEntity n set n.rolle =:rolleEntity where n.id =:nutzerId")
	int setRolle(@Param("nutzerId") UUID nutzerId, @Param("rolleEntity") RolleEntity rolleEntity);

	@Transactional
	@Modifying
	@Query("update NutzerEntity n set n.ausschuss =:ausschuss where n.id =:nutzerId")
	int setAusschuss(@Param("nutzerId") UUID nutzerId, @Param("ausschuss") BtAusschussType ausschuss);

	@Transactional
	@Modifying
	@Query("update NutzerEntity n set n.rolle =:rolleEntity where n.gid =:userGid")
	int setRolle(@Param("userGid") String userGid, @Param("rolleEntity") RolleEntity rolleEntity);

	@Query("select n.rolle from NutzerEntity n where n.id = :nutzerId")
	Optional<RolleEntity> getRolle(@Param("nutzerId") UUID nutzerId);

	List<NutzerEntity> findAllByRessortKurzbezeichnungIgnoreCase(String ressortKurzbezeichnung);

	Integer countByRolle_BezeichnungIn(@Param("bezeichnung") List<String> bezeichnungen);

	// Excludes users marked for cleanup: Stellvertreter, Nutzer mit einer Zuordnung, or Fachadministrator
	@Query("SELECT n.id FROM NutzerEntity n " +
		"LEFT JOIN n.rolle r " +
		"WHERE n.deleted = true " +
		"AND n.id NOT IN (" +
		"SELECT s.nutzerId FROM StellvertreterEntity s) " +
		"AND n.id NOT IN (" +
		"SELECT s.stellvertreterId FROM StellvertreterEntity s) " +
		"AND n.gid NOT IN (" +
		"SELECT z.benutzerId FROM ZuordnungEntity z) " +
		"AND (r IS NULL OR r.bezeichnung <> 'FACHADMINISTRATOR_BUNDESREGIERUNG')")
	List<NutzerIds> findDeletedNutzerWithoutStellvertreterOrZuordnung();

	@Transactional
	@Modifying
	@Query("UPDATE NutzerEntity n SET n.plattformNoRef = true WHERE n.id = :nutzerId")
	void setPlattformNoRef(@Param("nutzerId") UUID nutzerId);

	@Transactional
	@Modifying
	@Query("UPDATE NutzerEntity n SET n.editorNoRef = true WHERE n.gid = :userGid")
	void setEditorNoRef(@Param("userGid") String userGid);

	@Transactional
	@Modifying
	@Query("DELETE FROM NutzerEntity n WHERE n.id = :nutzerId")
	void deleteNutzerById(@Param("nutzerId") UUID nutzerId);

	@Query("SELECT n FROM NutzerEntity n WHERE n.plattformNoRef = true AND n.editorNoRef = true AND n.deleted = true")
	List<NutzerEntity> findAllOrphanedUsers();
}
