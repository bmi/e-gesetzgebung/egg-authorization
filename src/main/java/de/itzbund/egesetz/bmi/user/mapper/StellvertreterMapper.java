// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.mapper;

import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import de.itzbund.egesetz.bmi.user.dto.StellvertreterEntityDTO;
import de.itzbund.egesetz.bmi.user.entities.StellvertreterEntity;

/**
 * mapper between {@link StellvertreterEntity} and DTOs like {@link StellvertreterEntityDTO}}
 */
@Mapper(componentModel = "spring",
	injectionStrategy = InjectionStrategy.CONSTRUCTOR,
	builder = @Builder(disableBuilder = true))
public interface StellvertreterMapper {

	StellvertreterEntity map(StellvertreterEntityDTO stellvertreter);

	StellvertreterEntityDTO mapDto(StellvertreterEntity stellvertreter);

	default List<StellvertreterEntityDTO> mapDtoList(List<StellvertreterEntity> stellvertreterList) {
		return stellvertreterList.stream()
			.map(this::mapDto)
			.collect(Collectors.toList());
	}
}
