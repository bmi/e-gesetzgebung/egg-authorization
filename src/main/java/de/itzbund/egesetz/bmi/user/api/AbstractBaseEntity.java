// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.api;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

/**
 * defines common fields for (almost) all entities
 */
@Data
@SuperBuilder(toBuilder = true)
@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
@NoArgsConstructor
public abstract class AbstractBaseEntity implements Serializable {

	@CreatedDate
	@Column(nullable = false)
	protected Instant erstelltAm;

	@LastModifiedDate
	@Column(nullable = false)
	protected Instant bearbeitetAm;


	public abstract UUID getId();

	public abstract void setId(UUID id);
}
