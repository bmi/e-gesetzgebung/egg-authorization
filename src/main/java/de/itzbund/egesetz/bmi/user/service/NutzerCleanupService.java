// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import de.itzbund.egesetz.bmi.user.repositories.NutzerIds;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Service
@Slf4j
@Conditional(EnabledCondition.class)
public class NutzerCleanupService {

	private final NutzerEntityRepository nutzerEntityRepository;

	public void setOrphanedNutzerPlattformNoRef(UUID nutzerId) {
		nutzerEntityRepository.setPlattformNoRef(nutzerId);
	}

	public void deleteNutzerById(UUID nutzerId) {
		nutzerEntityRepository.deleteNutzerById(nutzerId);
		log.info("Orphaned user with ID: {} deleted", nutzerId);
	}

	public List<NutzerEntity> getAllOrphanedUsers() {
		return nutzerEntityRepository.findAllOrphanedUsers();
	}


	public List<NutzerIds> findNutzerWithoutStellvertreterOrZuordnung() {
		try {
			List<NutzerIds> nutzerIds = nutzerEntityRepository.findDeletedNutzerWithoutStellvertreterOrZuordnung();
			log.info("Found {} users (mark as deleted) without Stellvertreter or Zuordnung: {}", nutzerIds.size(), nutzerIds);
			return nutzerIds;
		} catch (Exception e) {
			log.debug(String.valueOf(e));
		}
		return Collections.emptyList();
	}
}
