// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.mapper;

import de.itzbund.egesetz.bmi.user.dto.NutzerEntityDTO;
import de.itzbund.egesetz.bmi.user.dto.NutzerEntitySaveDTO;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * mapper between {@link NutzerEntity} and DTOs like {@link NutzerEntityDTO} and {@link NutzerEntitySaveDTO}
 */
@Mapper(componentModel = "spring",
	injectionStrategy = InjectionStrategy.CONSTRUCTOR,
	builder = @Builder(disableBuilder = true))
public interface NutzerMapper {

	@Mapping(target = "id", source = "id")
	@Mapping(target = "name", source = "name")
	@Mapping(target = "email", source = "email")
	@Mapping(target = "gid", source = "gid")
	@Mapping(target = "telefon", source = "telefon")
	@Mapping(target = "titel", source = "titel")
	@Mapping(target = "anrede", source = "anrede")
	@Mapping(target = "ressortKurzbezeichnung", source = "ressortKurzbezeichnung")
	@Mapping(target = "abteilung", source = "abteilung")
	@Mapping(target = "referat", source = "referat")
	@Mapping(target = "ressortId", source = "ressortId")
	@Mapping(target = "aktiv", source = "aktiv")
	@Mapping(target = "deleted", source = "deleted")
	@Mapping(target = "ausschuss", source = "ausschuss")
	NutzerEntityDTO mapDto(NutzerEntity userEntity);

	@Mapping(target = "id", source = "id")
	@Mapping(target = "name", source = "name")
	@Mapping(target = "email", source = "email")
	@Mapping(target = "gid", source = "gid")
	@Mapping(target = "telefon", source = "telefon")
	@Mapping(target = "titel", source = "titel")
	@Mapping(target = "anrede", source = "anrede")
	@Mapping(target = "ressortKurzbezeichnung", source = "ressortKurzbezeichnung")
	@Mapping(target = "abteilung", source = "abteilung")
	@Mapping(target = "referat", source = "referat")
	@Mapping(target = "ressortId", source = "ressortId")
	@Mapping(target = "aktiv", source = "aktiv")
	@Mapping(target = "deleted", source = "deleted")
	@Mapping(target = "lastModified", source = "lastModified")
	NutzerEntitySaveDTO mapSaveDto(NutzerEntity userEntity);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "name", source = "name")
	@Mapping(target = "email", source = "email")
	@Mapping(target = "gid", source = "gid")
	@Mapping(target = "telefon", source = "telefon")
	@Mapping(target = "titel", source = "titel")
	@Mapping(target = "anrede", source = "anrede")
	@Mapping(target = "ressortKurzbezeichnung", source = "ressortKurzbezeichnung")
	@Mapping(target = "abteilung", source = "abteilung")
	@Mapping(target = "referat", source = "referat")
	@Mapping(target = "ressortId", source = "ressortId")
	@Mapping(target = "aktiv", source = "aktiv")
	@Mapping(target = "deleted", source = "deleted")
	@Mapping(target = "lastModified", source = "lastModified")
	NutzerEntity merge(NutzerEntitySaveDTO userDto, @MappingTarget NutzerEntity userEntity);
}
