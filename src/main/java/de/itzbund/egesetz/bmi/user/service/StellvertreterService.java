// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;
import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.user.dto.StellvertreterEntityDTO;
import de.itzbund.egesetz.bmi.user.entities.StellvertreterEntity;
import de.itzbund.egesetz.bmi.user.mapper.StellvertreterMapper;
import de.itzbund.egesetz.bmi.user.repositories.StellvertreterEntityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Service
@Slf4j
@Conditional(EnabledCondition.class)
public class StellvertreterService {

	private final StellvertreterEntityRepository stellvertreterEntityRepository;
	private final StellvertreterMapper mapper;

	/**
	 * Loads the list of my representatives (people who represent me)
	 *
	 * @param nutzerId the UUID of the user whose representatives are being requested
	 * @return a list of representatives
	 */
	public List<StellvertreterEntityDTO> getStellvertreter(UUID nutzerId) {
		return mapper.mapDtoList(stellvertreterEntityRepository.getStellvertreter(nutzerId));
	}

	/**
	 * Loads a specific representation (a person I represent).
	 *
	 * @param nutzerId         the UUID of the user who is representing
	 * @param stellvertreterId the UUID of the person being represented
	 * @return stellvetreter dto
	 */
	public StellvertreterEntityDTO getStellvertretung(UUID nutzerId, UUID stellvertreterId) {
		return stellvertreterEntityRepository.getStellvertretung(nutzerId, stellvertreterId)
			.map(mapper::mapDto)
			.orElse(null);
	}

	/**
	 * Loads the list of my representations (people I represent).
	 *
	 * @param nutzerId the UUID of the user whose representatives are being requested
	 * @return a list of all representatives
	 */
	public List<StellvertreterEntityDTO> getStellvertretungen(UUID nutzerId) {
		return stellvertreterEntityRepository.getStellvertretungen(nutzerId)
			.stream()
			.map(mapper::mapDto)
			.collect(Collectors.toList());
	}

	/**
	 * Checks if one user is the representative of another user
	 *
	 * @param nutzerId         the UUID of the user who might be represented
	 * @param stellvertreterId the UUID of the user who might be the representative
	 * @return true if the specified user is a representative for the other user, otherwise false
	 */
	public boolean isStellvertreter(UUID nutzerId, UUID stellvertreterId) {
		return stellvertreterEntityRepository.getStellvertretung(nutzerId, stellvertreterId).isPresent();
	}

	/**
	 * Updates the list of representatives by deleting and adding specified representatives
	 *
	 * @param stellvertreterToDelete list of representatives to be deleted
	 * @param stellvertreterToAdd    list of representatives to be added
	 */
	public void setStellvertreter(List<StellvertreterEntityDTO> stellvertreterToDelete, List<StellvertreterEntityDTO> stellvertreterToAdd) {
		List<StellvertreterEntity> toDelete = stellvertreterToDelete.stream()
			.map(mapper::map)
			.collect(Collectors.toList());
		List<StellvertreterEntity> toAdd = stellvertreterToAdd.stream()
			.map(mapper::map)
			.collect(Collectors.toList());

		stellvertreterEntityRepository.deleteAll(toDelete);
		stellvertreterEntityRepository.saveAll(toAdd);
	}
}