// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.itzbund.egesetz.bmi.user.enums.BtAusschussType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

/**
 * represents data of user (use this dto for readonly actions only)
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class NutzerEntityDTO {

	private UUID id;
	private String name;
	@NotNull
	private String email;
	private String telefon;
	private String abteilung;
	private String referat;
	private String titel;
	private Long ressortId;
	private String ressortKurzbezeichnung;
	private String anrede;
	private String gid;
	private boolean aktiv;
	private boolean deleted;
	@Enumerated(EnumType.STRING)
	@Column(name = "ausschuss", nullable = true)
	private BtAusschussType ausschuss;

	@JsonIgnore
	public boolean isBundestagUser() {
		return Optional.ofNullable(StringUtils.trimToNull(ressortKurzbezeichnung))
			.map(kurzbezeichnung -> StringUtils.equalsIgnoreCase(kurzbezeichnung, "BT"))
			.orElse(false);
	}

	public BtAusschussType getAusschuss() {
		return ausschuss != null ? ausschuss : null;
	}

	public void setAusschuss(BtAusschussType btAusschussType) {
		this.ausschuss = btAusschussType;
	}

}

