// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.dto;

import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * represents data of stellvertreter user (use this dto for readonly actions only)
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class StellvertreterEntityDTO {

	@NotNull
	private UUID nutzerId;
	@NotNull
	private UUID stellvertreterId;
}