// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.entities;

import java.time.Instant;
import java.util.Optional;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang3.StringUtils;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.user.enums.BtAusschussType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import static de.itzbund.egesetz.bmi.user.entities.AdminNutzerDaten.ADMIN_EMAIL;

/**
 * represents user
 */
@Data
@Entity
@Table(name = "nutzer")
@SuperBuilder
@NoArgsConstructor
public class NutzerEntity extends BaseEntity {

	private String name;
	@Column(nullable = false)
	private String email;
	//gid: Globale ID in IAM/Keycloak
	private String gid;
	private String telefon;
	private String abteilung;
	private String referat;
	private String titel;
	//ressortId: ID aus dem IAM/Keycloak
	private Long ressortId;
	private String ressortKurzbezeichnung;
	private String anrede;
	private boolean aktiv;
	private boolean deleted;
	private Instant lastModified;
	@Enumerated(EnumType.STRING)
	@Column(name = "ausschuss", nullable = true)
	private BtAusschussType ausschuss;
	private boolean plattformNoRef;
	private boolean editorNoRef;

	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@ManyToOne
	@JoinColumn(name = "rolle_id", foreignKey = @ForeignKey(name = "FK_nutzer__rolle_id"))
	private RolleEntity rolle;


	/**
	 * Checks whether the user is E-Gesetzgebung Admin
	 *
	 * @return is user admin?
	 */
	public boolean isAdmin() {
		return Optional.ofNullable(email)
			.map(m -> StringUtils.equalsIgnoreCase(m, ADMIN_EMAIL))
			.orElse(false);
	}

	public BtAusschussType getAusschuss() {
		return ausschuss != null ? ausschuss : null;
	}

	public void setAusschuss(BtAusschussType btAusschussType) {
		this.ausschuss = btAusschussType;
	}

}
