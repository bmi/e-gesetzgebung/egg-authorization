// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.repository.JpaRepository;
import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.user.entities.StellvertreterEntity;

/**
 * Access to nutzer_stellvertreter table
 */
@Qualifier("authDataSource")
@Conditional(EnabledCondition.class)
public interface StellvertreterEntityRepository extends JpaRepository<StellvertreterEntity, UUID> {

	default List<StellvertreterEntity> getStellvertreter(UUID nutzerId) {
		return findByNutzerId(nutzerId);
	}

	default List<StellvertreterEntity> getStellvertretungen(UUID stellvertreterId) {
		return findByStellvertreterId(stellvertreterId);
	}

	List<StellvertreterEntity> findByNutzerId(UUID nutzerId);

	List<StellvertreterEntity> findByStellvertreterId(UUID stellvertreterId);

	default Optional<StellvertreterEntity> getStellvertretung(UUID nutzerId, UUID stellvertreterId) {
		return findByNutzerIdAndStellvertreterId(nutzerId, stellvertreterId);
	}

	Optional<StellvertreterEntity> findByNutzerIdAndStellvertreterId(UUID nutzerId, UUID stellvertreterId);
}