// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.entities;

import de.itzbund.egesetz.bmi.user.api.AbstractBaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

/**
 * represents class with common fields from which (almost) all other classes inherit from
 */
@Data
@SuperBuilder(toBuilder = true)
@MappedSuperclass
@NoArgsConstructor
public class BaseEntity extends AbstractBaseEntity {

	@Id
	@GeneratedValue(generator = "UUID", strategy = GenerationType.IDENTITY)
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", updatable = false, nullable = false, columnDefinition = "UUID")
	protected UUID id;

}
