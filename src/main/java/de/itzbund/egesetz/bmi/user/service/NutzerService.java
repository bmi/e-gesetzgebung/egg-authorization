// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.service;

import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.repositories.RollenRepository;
import de.itzbund.egesetz.bmi.user.dto.NutzerEntityDTO;
import de.itzbund.egesetz.bmi.user.dto.NutzerEntitySaveDTO;
import de.itzbund.egesetz.bmi.user.entities.BaseEntity;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.enums.BtAusschussType;
import de.itzbund.egesetz.bmi.user.mapper.NutzerMapper;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
@Conditional(EnabledCondition.class)
public class NutzerService {

	private final NutzerMapper mapper;
	private final NutzerEntityRepository nutzerEntityRepository;
	private final RollenRepository rollenRepository;

	/**
	 * picks single user from a list of users by email address. This method tries to handle ambigious results (E-Mail is not unique!): users with existing GID
	 * and recent editing date are returned first
	 *
	 * @param nutzerEntities list of users
	 * @param email          find user by email address
	 * @return user found
	 */
	static Optional<NutzerEntity> pickNutzer(List<NutzerEntity> nutzerEntities, String email) {
		nutzerEntities = CollectionUtils.emptyIfNull(nutzerEntities)
			.stream()
			.filter(Objects::nonNull)
			.filter(nutzerEntity -> StringUtils.equalsIgnoreCase(nutzerEntity.getEmail(), email))
			.sorted(Comparator.comparing(NutzerEntity::getBearbeitetAm, Comparator.nullsLast(Comparator.reverseOrder())))
			.collect(Collectors.toList());
		if (nutzerEntities.isEmpty()) {
			return Optional.empty();
		}
		NutzerEntity nutzerEntity;
		if (nutzerEntities.size() == 1) {
			nutzerEntity = nutzerEntities.get(0);
		} else {
			//found multiple results, prefer user with GID for return
			List<NutzerEntity> nutzerEntitiesWithGid = nutzerEntities.stream()
				.filter(u -> StringUtils.isNotBlank(u.getGid()))
				.collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(nutzerEntitiesWithGid)) {
				nutzerEntity = nutzerEntitiesWithGid.get(0);
				if (nutzerEntitiesWithGid.size() > 1) {
					log.warn("found multiple ({}) users with gid and the same email address -> will pick first one with id {} to continue with",
						nutzerEntitiesWithGid.size(), nutzerEntity.getId());
				}
			} else {
				nutzerEntity = nutzerEntities.get(0);
				log.warn("found multiple ({}) users without gid and the same email address -> will pick first one with id {} to continue with",
					nutzerEntities.size(), nutzerEntity.getId());
			}
		}
		return Optional.of(nutzerEntity);
	}

	/**
	 * convenience method: find single user by E-Mail.
	 *
	 * @param email E-Mail
	 * @return user found
	 * @apiNote E-Mail is not a unique key, so maybe you get unexpected results!
	 */
	public Optional<NutzerEntityDTO> getNutzerByEmail(String email) {
		List<NutzerEntity> nutzerEntities = nutzerEntityRepository.findAllByEmailIn(List.of(email));
		return pickNutzer(nutzerEntities, email).map(this::map);
	}

	/**
	 * find multiple users by E-Mail
	 *
	 * @param emails collection of E-Mails
	 * @return list of users found
	 * @apiNote E-Mail is not a unique key, so maybe you get unexpected results!
	 */
	public List<NutzerEntityDTO> getNutzerByEmails(Collection<String> emails) {
		List<NutzerEntity> nutzerEntities = nutzerEntityRepository.findAllByEmailIn(emails);
		List<NutzerEntity> result = CollectionUtils.emptyIfNull(emails)
			.stream()
			.map(email -> pickNutzer(nutzerEntities, email))
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toList());
		return mapList(result);
	}

	/**
	 * convenience method: find single user by GID (Globale ID in IAM/Keycloak)
	 *
	 * @param gid GID
	 * @return user found
	 */
	public Optional<NutzerEntityDTO> getNutzerByGid(String gid) {
		return nutzerEntityRepository.findByGid(gid).map(this::map);
	}

	/**
	 * find multiple users by GID (Globale ID in IAM/Keycloak)
	 *
	 * @param gids collection of GIDs
	 * @return list of users found
	 */
	public List<NutzerEntityDTO> getNutzerByGids(Collection<String> gids) {
		return mapList(nutzerEntityRepository.findAllByGidIn(gids));
	}

	/**
	 * convenience method: find single user by ID (EGG ID)
	 *
	 * @param id EGG ID
	 * @return user found
	 */
	public Optional<NutzerEntityDTO> getNutzerById(UUID id) {
		return nutzerEntityRepository.findById(id).map(this::map);
	}

	/**
	 * find multiple users by ID (EGG ID)
	 *
	 * @param ids collection of IDs
	 * @return list of users found
	 */
	public List<NutzerEntityDTO> getNutzerByIds(Collection<UUID> ids) {
		return mapList(nutzerEntityRepository.findAllById(ids));
	}

	/**
	 * returns all users
	 *
	 * @return list of all users
	 */
	public List<NutzerEntityDTO> getAllNutzer() {
		return mapList(nutzerEntityRepository.findAll());
	}

	/**
	 * returns users by specified ressort short name
	 *
	 * @return list of users by ressort
	 */
	public List<NutzerEntityDTO> getNutzerByRessort(String ressortKurzbezeichnung) {
		return mapList(nutzerEntityRepository.findAllByRessortKurzbezeichnungIgnoreCase(ressortKurzbezeichnung));
	}

	/**
	 * sets role of user
	 *
	 * @param nutzerId user id
	 * @param rolle    new role
	 * @return true if role has been saved
	 */
	public boolean setRolle(UUID nutzerId, String rolle) {
		RolleEntity rolleEntity = null;
		if (rolle != null) {
			Optional<RolleEntity> rolleEntityOptional = rollenRepository.findByBezeichnung(rolle);
			if (rolleEntityOptional.isEmpty()) {
				String message = MessageFormat.format("Rolle ''{0}'' wurde nicht gefunden", rolle);
				throw new IllegalArgumentException(message);
			} else {
				rolleEntity = rolleEntityOptional.get();
			}
		}
		return nutzerEntityRepository.setRolle(nutzerId, rolleEntity) > 0;
	}

	/**
	 * gets role of user
	 *
	 * @param nutzerId user id
	 */
	public Optional<String> getRolle(UUID nutzerId) {
		return nutzerEntityRepository.getRolle(nutzerId)
			.map(RolleEntity::getBezeichnung);
	}

	/**
	 * for PLATEG: creates or updates users
	 *
	 * @param dtos users to create or update
	 * @return list of saved users
	 */
	public List<NutzerEntityDTO> saveNutzer(Collection<NutzerEntitySaveDTO> dtos) {
		List<NutzerEntity> nutzerEntities = CollectionUtils.emptyIfNull(dtos)
			.stream()
			.map(this::merge)
			.collect(Collectors.toList());
		return mapList(nutzerEntityRepository.saveAll(nutzerEntities));
	}

	/**
	 * for PLATEG: creates or updates user
	 *
	 * @param dto user to create or update
	 * @return list of saved users
	 */
	public NutzerEntityDTO saveNutzer(NutzerEntitySaveDTO dto) {
		NutzerEntity nutzerEntity = merge(dto);
		return map(nutzerEntityRepository.save(nutzerEntity));
	}

	/**
	 * for PLATEG: finds user to update data on login
	 *
	 * @param gid   GID to find (primary key)
	 * @param email E-Mail to find (secondary non-unique key)
	 * @return user for update found
	 */
	public Optional<NutzerEntitySaveDTO> getUserForSaving(String gid, String email) {
		NutzerEntityDTO dto = NutzerEntityDTO.builder()
			.gid(gid)
			.email(email)
			.build();
		return findUserForUpdate(dto).map(this::mapSave);
	}

	/**
	 * for PLATEG: finds users for nightly update
	 *
	 * @param gids collection of GIDs
	 * @return list of all users for update
	 */
	public List<NutzerEntitySaveDTO> getUsersForSaving(Collection<String> gids) {
		return mapSaveList(nutzerEntityRepository.findAllByGidIn(gids));
	}

	private NutzerEntity merge(NutzerEntitySaveDTO dto) {
		Optional<NutzerEntity> optionalNutzer = findUserForUpdate(dto);
		NutzerEntity nutzerEntity;
		if (optionalNutzer.isPresent()) {
			//will update user
			nutzerEntity = optionalNutzer.get();
		} else {
			//will create new user
			nutzerEntity = NutzerEntity.builder().build();
		}
		return mapper.merge(dto, nutzerEntity);
	}

	/**
	 * removes users without GID
	 *
	 * @param ids ids of users that should be removed
	 * @return number of removed entries
	 */

	public long cleanUpUsersWithoutGid(Collection<UUID> ids) {
		return nutzerEntityRepository.deleteByIdInAndGidIsNull(ids);
	}

	/**
	 * returns the E-Gesetzgebungs Administator (it is actually a system user and not a real Administrator)
	 *
	 * @return dto of admin
	 */
	public NutzerEntityDTO getAdminNutzer() {
		return map(nutzerEntityRepository.getOrCreateAdmin());
	}

	public Integer getUserCountForRoles(List<String> rollen) {
		return nutzerEntityRepository.countByRolle_BezeichnungIn(rollen);
	}
	Optional<NutzerEntity> findUserForUpdate(NutzerEntityDTO dto) {
		Optional<UUID> idOptional = Optional.ofNullable(dto.getId());
		if (idOptional.isPresent()) {
			Optional<NutzerEntity> optionalUser = idOptional.flatMap(nutzerEntityRepository::findById);
			if (optionalUser.isEmpty()) {
				throw new EntityNotFoundException("unable to find user with id " + idOptional.get());
			}
			return optionalUser;
		}
		Optional<String> gidOptional = Optional.ofNullable(StringUtils.trimToNull(dto.getGid()));
		if (gidOptional.isPresent()) {
			Optional<NutzerEntity> optionalUser = gidOptional.flatMap(nutzerEntityRepository::findByGid);
			if (optionalUser.isPresent()) {
				return optionalUser;
			}
		}
		Optional<String> emailOptional = Optional.ofNullable(StringUtils.trimToNull(dto.getEmail()));
		if (emailOptional.isPresent()) {
			List<NutzerEntity> users = nutzerEntityRepository.findAllByEmailIn(List.of(emailOptional.get()));
			if (!users.isEmpty()) {
				if (users.size() > 1) {
					log.debug("found {} users for given e-mail address! Cannot decide which id ({}) you want to update, so I will create a new entry",
						users.size(), users.stream().map(BaseEntity::getId).collect(Collectors.toList()));
					return Optional.empty();
				}
				return Optional.of(users.get(0));
			}
		}
		return Optional.empty();
	}

	private List<NutzerEntityDTO> mapList(Collection<NutzerEntity> userEntities) {
		return CollectionUtils.emptyIfNull(userEntities)
			.stream()
			.filter(Objects::nonNull)
			.map(this::map)
			.collect(Collectors.toList());
	}

	private List<NutzerEntitySaveDTO> mapSaveList(Collection<NutzerEntity> userEntities) {
		return CollectionUtils.emptyIfNull(userEntities)
			.stream()
			.filter(Objects::nonNull)
			.map(this::mapSave)
			.collect(Collectors.toList());
	}

	private NutzerEntityDTO map(NutzerEntity userEntity) {
		return mapper.mapDto(userEntity);
	}

	private NutzerEntitySaveDTO mapSave(NutzerEntity userEntity) {
		return mapper.mapSaveDto(userEntity);
	}

	/**
	 * this mapper can be used by inherited classes
	 *
	 * @return user mapper
	 */
	protected NutzerMapper getMapper() {
		return mapper;
	}

	/**
	 * sets Ausschuss of user
	 *
	 * @param nutzerId user id
	 * @param ausschuss    ausschuss
	 * @return true if ausschuss has been saved
	 */
	public boolean setAusschuss(UUID nutzerId, String ausschuss) {
		Optional<NutzerEntity> optionalUser = nutzerEntityRepository.findById(nutzerId);
		if (optionalUser.isEmpty()) {
			throw new EntityNotFoundException("unable to find user with id " + nutzerId);
		}
		BtAusschussType btAusschuss = ausschuss==null?null:BtAusschussType.valueOf(ausschuss);
		return nutzerEntityRepository.setAusschuss(nutzerId, btAusschuss) > 0;
	}


}
