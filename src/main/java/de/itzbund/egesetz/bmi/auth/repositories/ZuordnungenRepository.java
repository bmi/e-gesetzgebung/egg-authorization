// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("authDataSource")
@Conditional(EnabledCondition.class)
public interface ZuordnungenRepository extends JpaRepository<ZuordnungEntity, Long>, JpaSpecificationExecutor<ZuordnungEntity> {

    // CREATE
    // -- inherit

    // READ
    // Einfache Suchen

    Optional<ZuordnungEntity> findByBezeichnung(String bezeichnung);

    List<ZuordnungEntity> findByRessourceIdOrderById(String ressourceId);

    Optional<List<ZuordnungEntity>> findByRessourceIdAndRolle(String ressourceId, RolleEntity rolle);

    Optional<List<ZuordnungEntity>> findByRessourceTypAndBenutzerId(RessourceTypEntity ressourceTyp, String userId);

    Optional<List<ZuordnungEntity>> findByRessourceTypAndRessourceIdAndBenutzerIdAndDeaktiviert(RessourceTypEntity ressourceTyp, String ressourceId,
        String userId, boolean deaktiviert);

    Optional<List<ZuordnungEntity>> findByRessourceTypAndBenutzerIdAndRolle(RessourceTypEntity ressourceTyp, String userId, RolleEntity rolle);

    Optional<List<ZuordnungEntity>> findByRessourceTypAndBenutzerIdAndRolleAndDeaktiviert(RessourceTypEntity ressourceTyp, String userId, RolleEntity rolle,
        boolean deaktiviert);

    Optional<ZuordnungEntity> findByRessourceTypAndBenutzerIdAndRessourceIdAndRolle(RessourceTypEntity resourceTypEntity, String benutzerId, String ressourceId,
        RolleEntity rolle);

    List<ZuordnungEntity> findByRessourceTypAndBenutzerIdAndRessourceId(RessourceTypEntity resourceTypEntity, String benutzerId, String ressourceId);

    // Löschen mit vier Parametern
    @Transactional
    int deleteByRessourceTypAndBenutzerIdAndRessourceIdAndRolle(RessourceTypEntity resourceTypEntity, String benutzerId, String ressourceId,
        RolleEntity rolle);

    // UPDATE
    // -- inherit

    // DELETE
    void deleteByRessourceIdAndRessourceTypAndRolle(String resourceId, RessourceTypEntity ressourceTyp, RolleEntity rolle);

    default Specification<ZuordnungEntity> createSpecification(
        Optional<Collection<String>> benutzerIds,
        Optional<Collection<AktionEntity>> aktionen,
        Optional<Collection<RessourceTypEntity>> ressourceTypen,
        Optional<Collection<String>> ressourcenIds,
        Optional<Collection<RolleEntity>> rollen) {
        return Specification.where((root, query, cb) -> {
            Predicate benutzerIdsPredicate;
            if (benutzerIds.isPresent()) {
                CriteriaBuilder.In<String> in = cb.in(root.get("benutzerId"));
                benutzerIds.get().forEach(in::value);
                benutzerIdsPredicate = in;
            } else {
                benutzerIdsPredicate = cb.conjunction();
            }

            Predicate ressourcenIdsPredicate;
            if (ressourcenIds.isPresent()) {
                CriteriaBuilder.In<String> in = cb.in(root.get("ressourceId"));
                ressourcenIds.get().forEach(in::value);
                ressourcenIdsPredicate = in;
            } else {
                ressourcenIdsPredicate = cb.conjunction();
            }

            Predicate ressourceTypenPredicate;
            if (ressourceTypen.isPresent()) {
                CriteriaBuilder.In<RessourceTypEntity> in = cb.in(root.get("ressourceTyp"));
                ressourceTypen.get().forEach(in::value);
                ressourceTypenPredicate = in;
            } else {
                ressourceTypenPredicate = cb.conjunction();
            }

            Predicate rollenPredicate;
            if (rollen.isPresent()) {
                CriteriaBuilder.In<RolleEntity> in = cb.in(root.get("rolle"));
                rollen.get().forEach(in::value);
                rollenPredicate = in;
            } else {
                rollenPredicate = cb.conjunction();
            }

            Predicate aktionenPredicate;
            if (aktionen.isPresent()) {
                Join<ZuordnungEntity, RolleEntity> subRolle = root.join("rolle");
                Join<ZuordnungEntity, RegelEntity> subRegel = subRolle.join("regel");
                CriteriaBuilder.In<AktionEntity> in = cb.in(subRegel.get("aktion"));
                aktionen.get().forEach(in::value);
                aktionenPredicate = in;
            } else {
                aktionenPredicate = cb.conjunction();
            }

            query.distinct(true);
            return cb.and(benutzerIdsPredicate, ressourcenIdsPredicate, ressourceTypenPredicate, rollenPredicate, aktionenPredicate);
        });
    }
}
