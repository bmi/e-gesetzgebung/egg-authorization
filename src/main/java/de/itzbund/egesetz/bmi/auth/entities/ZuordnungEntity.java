// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

/**
 * A domain entity representing a comment with some metadata.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "zuordnungen")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ZuordnungEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@EqualsAndHashCode.Exclude
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ressource_typ_id", nullable = false)
	private RessourceTypEntity ressourceTyp;

	@EqualsAndHashCode.Exclude
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rolle_id", nullable = false)
	private RolleEntity rolle;

	@EqualsAndHashCode.Exclude
	@Column(name = "ressource_id")
	private String ressourceId;

	@EqualsAndHashCode.Exclude
	@Column(name = "benutzer_id")
	private String benutzerId;

	@EqualsAndHashCode.Exclude
	private String bezeichnung;

	@EqualsAndHashCode.Exclude
	private boolean deaktiviert;

	@EqualsAndHashCode.Exclude
	private Instant befristung;

	@EqualsAndHashCode.Exclude
	private Long fallbackZuordnung;

	@EqualsAndHashCode.Exclude
	@Column(name = "anmerkungen")
	private String anmerkungen;
}
