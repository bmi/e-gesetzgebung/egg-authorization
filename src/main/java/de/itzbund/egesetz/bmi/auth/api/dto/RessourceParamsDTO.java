// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.dto;

import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RessourceParamsDTO {

	RessourceTypEntity ressourceTypEntity;
	RolleEntity rolleEntity;

}
