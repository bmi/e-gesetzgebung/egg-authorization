// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.StatusEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("authDataSource")
@Conditional(EnabledCondition.class)
public interface RegelnRepository extends JpaRepository<RegelEntity, Long> {

	Optional<RegelEntity> findByBezeichnung(String bezeichnung);

	Optional<List<RegelEntity>> findByRessourceTypAndRolleAndStatusIn(RessourceTypEntity ressourceTyp, RolleEntity rolle,
		Collection<StatusEntity> status);

	Optional<List<RegelEntity>> findByRessourceTyp(RessourceTypEntity ressourceTyp);

	Optional<List<RegelEntity>> findByRessourceTypAndAktion(RessourceTypEntity ressourceTyp, AktionEntity aktion);

}
