// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.StatusEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.Collection;
import java.util.Optional;

@Repository
@Qualifier("authDataSource")
@Conditional(EnabledCondition.class)
public interface AktionenRepository extends JpaRepository<AktionEntity, Long>, JpaSpecificationExecutor<AktionEntity> {

	Optional<AktionEntity> findByBezeichnung(String bezeichnung);

	default Specification<AktionEntity> createSpecification(
		RessourceTypEntity ressourceTyp,
		Collection<RolleEntity> rollen,
		Optional<Collection<StatusEntity>> status,
		Optional<Collection<AktionEntity>> aktionen) {
		return Specification.where((root, query, cb) -> {
			Join<AktionEntity, RegelEntity> subRegel = root.join("regel");
			Predicate ressourceTypPredicate = cb.equal(subRegel.get("ressourceTyp"), ressourceTyp);

			CriteriaBuilder.In<RolleEntity> inRolle = cb.in(subRegel.get("rolle"));
			rollen.forEach(inRolle::value);
			Predicate rollenPredicate = inRolle;

			Predicate statusPredicate;
			if (status.isPresent()) {
				CriteriaBuilder.In<StatusEntity> in = cb.in(subRegel.get("status"));
				status.get().forEach(in::value);
				statusPredicate = in;
			} else {
				statusPredicate = cb.conjunction();
			}

			Predicate aktionenPredicate;
			if (aktionen.isPresent()) {
				CriteriaBuilder.In<AktionEntity> in = cb.in(root);
				aktionen.get().forEach(in::value);
				aktionenPredicate = in;
			} else {
				aktionenPredicate = cb.conjunction();
			}

			query.distinct(true);
			return cb.and(ressourceTypPredicate, rollenPredicate, statusPredicate, aktionenPredicate);
		});
	}

}
