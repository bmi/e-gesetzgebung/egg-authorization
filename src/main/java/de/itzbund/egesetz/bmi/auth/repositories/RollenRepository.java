// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Qualifier("authDataSource")
@Conditional(EnabledCondition.class)
public interface RollenRepository extends JpaRepository<RolleEntity, Long> {

    Optional<RolleEntity> findByBezeichnung(String bezeichnung);

}
