// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.configuration;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Log4j2
@Configuration
@EnableTransactionManagement
@ComponentScan("de.itzbund.egesetz.bmi.user")
@EnableJpaRepositories(
	basePackages = {
		"de.itzbund.egesetz.bmi.auth.repositories",
		"de.itzbund.egesetz.bmi.user.repositories"
	},
	entityManagerFactoryRef = "authEntityManagerFactory",
	transactionManagerRef = "authTransactionManager"
)
public class AuthJpaConfiguration {

	@Value("${spring.datasource.packages:de.itzbund.egesetz.bmi}")
	private String primaryDatasourcePpackages;

	protected Map<String, Object> jpaProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put("hibernate.physical_naming_strategy", SpringPhysicalNamingStrategy.class.getName());
		props.put("hibernate.implicit_naming_strategy", SpringImplicitNamingStrategy.class.getName());
		return props;
	}

	@Bean
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
		@Qualifier("springDataSource") DataSource dataSource,
		EntityManagerFactoryBuilder builder) {
		log.debug("EntityManager für primäre Spring Datenbank");
		return builder
			.dataSource(dataSource)
			.properties(jpaProperties())
			.packages(primaryDatasourcePpackages)
			.build();
	}


	@Bean
	@Conditional(EnabledCondition.class)
	public LocalContainerEntityManagerFactoryBean authEntityManagerFactory(
		@Qualifier("authDataSource") DataSource dataSource,
		EntityManagerFactoryBuilder builder) {
		log.debug("EntityManager für sekundäre Datenbank");
		return builder
			.dataSource(dataSource)
			.properties(jpaProperties())
			.packages(
				"de.itzbund.egesetz.bmi.auth.entities",
				"de.itzbund.egesetz.bmi.user.entities"
			)
			.build();
	}


	@Bean
	@Conditional(EnabledCondition.class)
	public PlatformTransactionManager authTransactionManager(
		@Qualifier("authEntityManagerFactory") LocalContainerEntityManagerFactoryBean authEntityManagerFactory) {
		log.debug("Transaktionsmanager für sekundäre Datenbank");
		return new JpaTransactionManager(Objects.requireNonNull(authEntityManagerFactory.getObject()));
	}


	@Bean
	public PlatformTransactionManager transactionManager(
		@Qualifier("entityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactory) {
		log.debug("Transaktionsmanager für primäre Spring Datenbank");
		return new JpaTransactionManager(Objects.requireNonNull(entityManagerFactory.getObject()));
	}

}
