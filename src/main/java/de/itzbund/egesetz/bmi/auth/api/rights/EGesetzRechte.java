// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.rights;

import de.itzbund.egesetz.bmi.auth.api.dto.PermissionDTO;
import de.itzbund.egesetz.bmi.auth.api.dto.StatusResponseDTO;
import de.itzbund.egesetz.bmi.auth.api.enums.GetPermissionOptionType;
import de.itzbund.egesetz.bmi.auth.api.enums.UpdateResultType;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface EGesetzRechte {


	/**
	 * The status is held in the main application, so it must provide it be implementing this abstract class.
	 *
	 * @param ressourcenIds The ids of the resources.
	 * @param ressourcenTyp The type of the resource, e.g. Dokument
	 * @return The Status of the resource, like DRAFT or similar
	 */
	List<StatusResponseDTO> getStatusForResourceType(String ressourcenTyp, Set<String> ressourcenIds);

	/**
	 * The status is held in the main application, so it must provide it be implementing this abstract class.
	 *
	 * @param ressourcenId  The id of the resource.
	 * @param ressourcenTyp The type of the resource, e.g. Dokument
	 * @return The Status of the resource, like DRAFT or similar
	 */
	String getStatusForResourceType(String ressourcenTyp, String ressourcenId);

	/**
	 * Checks if action is allowed by the given parameters. As status is provided by the main application #getStatusForResourceType(String, String) must be
	 * implemented to present the information.
	 *
	 * @param benutzerId    Id of the user
	 * @param aktionen      Actions he wants to do
	 * @param ressourcenTyp Type of resource, e.g. Dokument
	 * @param ressourceId   The id of that resource
	 * @param optionType    check status of entities? (potentially costly operation!)
	 * @return if action is allowed or not
	 */
	Optional<PermissionDTO> getPermission(String benutzerId, Collection<String> aktionen, String ressourcenTyp, String ressourceId,
		GetPermissionOptionType optionType);

	/**
	 * Checks if action is allowed by the given parameters. As status is provided by the main application #getStatusForResourceType(String, String) must be
	 * implemented to present the information.
	 *
	 * @param benutzerId    Id of the user
	 * @param aktion        Action he wants to do
	 * @param ressourcenTyp Type of resource, e.g. Dokument
	 * @param ressourcenId  The id of that resource
	 * @return if action is allowed or not
	 */
	boolean isAllowed(String benutzerId, String aktion, String ressourcenTyp, String ressourcenId);

	/**
	 * Allows to update role of existing permission.
	 *
	 * @param benutzerId    The GID of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param neueRolle     The role of this permission
	 * @return If it was successfully or not
	 */
	UpdateResultType updateRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String neueRolle);


	/**
	 * Allows to add some new relations between new resources.
	 *
	 * @param benutzerId    The GID of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param rolle         The role of this permission
	 * @return If it was successfully or not
	 */
	boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle);

	/**
	 * Allows to add some new relations between new resources temporarily restricted.
	 *
	 * @param benutzerId    The GID of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param rolle         The role of this permission
	 * @param befristung    The time until relation is invalid
	 * @return If it was successfully or not
	 */
	boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle, Instant befristung);

	/**
	 * Gets the ids depending of ressource type for a given user
	 *
	 * @param benutzerId    The Id of the user
	 * @param ressourcenTyp The ressource type, e.g. DOKUMENTE
	 * @return A list of ids of the given ressource type
	 */
	List<String> getAlleRessourcenFuerBenutzerUndRessourceTyp(String benutzerId, String ressourcenTyp);

	/**
	 * Gets the ressource ids depending of ressource type and actions for a given user
	 *
	 * @param benutzerId    The Id of the user
	 * @param ressourcenTyp The ressource type, e.g. DOKUMENTE
	 * @param aktionen      The actions
	 * @return A list of ids of the given ressource type
	 */
	List<PermissionDTO> getAlleRessourcenFuerBenutzerUndRessourceTypAndAktionen(String benutzerId, String ressourcenTyp, Collection<String> aktionen);

	/**
	 * Gets the ids depending of ressource type and role for a given user
	 *
	 * @param benutzerId    The Id of the user
	 * @param ressourcenTyp The ressource type, e.g. DOKUMENTE
	 * @param rolle         The role, e.g. LESER
	 * @return A list of ids of the given ressource type
	 */
	@Deprecated
	List<String> getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(String benutzerId, String rolle, String ressourcenTyp);

	/**
	 * Gets the ids depending of ressource type and role for a given user, filter by deactivated entries.
	 *
	 * @param benutzerId       The Id of the user
	 * @param ressourcenTyp    The ressource type, e.g. DOKUMENTE
	 * @param rolle            The role, e.g. LESER
	 * @param ohneDeaktivierte Enable or disable filter for deactivated entries
	 * @return A list of ids of the given ressource type
	 */
	List<String> getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(String benutzerId, String rolle, String ressourcenTyp, boolean ohneDeaktivierte);

	/**
	 * Gets the set of user ids which are allowed to perform a given action (e.g. LESEN/SCHREIBEN) on a given resource (specified by its id)).
	 *
	 * @param ressourcenId Id of the ressource
	 * @param aktion       Action which shall be evaluated
	 * @return A set of user ids which are allowed to perform the given action
	 */
	Set<String> getAlleBenutzerIdsByRessourceIdAndAktion(String ressourcenId, String aktion);

	/**
	 * Gets the list of permissions by ressource id(s), role(s) or user id(s) filter parameters
	 *
	 * @param benutzerIds    Ids of users
	 * @param ressourcenIds  Ids of the ressource
	 * @param ressourceTypen Name of ressource types
	 * @param aktionen       Name of actions
	 * @param rollen         Name of roles
	 * @param optionType     check status of entities? (potentially costly operation!)
	 * @return A set of user ids which are allowed to perform the given action
	 */
	List<PermissionDTO> getPermissions(
		Optional<Collection<String>> benutzerIds,
		Optional<Collection<String>> aktionen,
		Optional<Collection<String>> ressourceTypen,
		Optional<Collection<String>> ressourcenIds,
		Optional<Collection<String>> rollen,
		GetPermissionOptionType optionType);

	List<String> getAlleBenutzerIdsByRessourceIdAndRolle(String ressourcenId, String rolle);

	/**
	 * @param benutzerId    The GID of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param rolle         The role of this permission
	 * @return If it was successfully or not
	 */
	boolean removeRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle);

	/**
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param rolle         The role of this permission
	 * @return If it was successfully or not
	 */
	boolean removeRessourcePermission(String ressourcenTyp, String ressourceId, String rolle);

	/**
	 * @param benutzerId The GID of the user
	 * @return The ressort kurzbezeichung for the given user
	 */
	public String getRessortKurzbezeichnungForNutzer(String benutzerId);

	// ------------ FOR TESTING ----------

	HashMap<String, Set<String>> getAlleDokumenteWithPermissionsbyBenutzerId(String benutzerId,
		String ressourcenTyp);

	/**
	 * Checks if action is allowed by the given parameters.
	 *
	 * @param benutzerGid    The GID of the user
	 * @param aktion        Action he wants to do
	 * @param angefragteRolle the desired role of the user
	 * @return if action is allowed or not
	 */
	boolean isAllowedAdministration(String benutzerGid, String aktion, String angefragteRolle);
}
