// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.enums;

/**
 * indicates outcome of update permission attempt
 */
public enum UpdateResultType {
	UPDATED,
	NOT_CHANGED,
	NOT_FOUND
}
