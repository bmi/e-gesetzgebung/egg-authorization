// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.possibilities;

import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.repositories.AktionenRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Component
public class Aktionen {

    @Autowired
    private AktionenRepository aktionenRepository;

    private List<String> actions = null;


    List<String> getAktionen() {
        if (actions == null) {
            List<AktionEntity> alleAktionen = aktionenRepository.findAll();
            actions = alleAktionen.stream().map(a -> a.getBezeichnung()).collect(Collectors.toList());
        }

        log.debug("Provided Aktionen: {}", actions);
        return actions;
    }

}
