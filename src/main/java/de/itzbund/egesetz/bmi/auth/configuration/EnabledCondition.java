// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.configuration;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

@Configuration
public class EnabledCondition extends SpringBootCondition {

    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Environment env = context.getEnvironment();
        String dbEnabledVal = StringUtils.trimToEmpty(env.getProperty("auth.db.enabled"));
        if (StringUtils.isBlank(dbEnabledVal)) {
            return new ConditionOutcome(true,
                "Database is enabled as fallback, because value is not set or is empty");
        }
        boolean dbEnabled = BooleanUtils.toBoolean(dbEnabledVal);
        return new ConditionOutcome(dbEnabled, "Database enabled is set to " + dbEnabledVal);
    }
}