// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.configuration;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Log4j2
@Configuration
public class AuthDatasourceConfiguration {

    @Bean
    @ConfigurationProperties("auth.datasource")
    @Conditional(EnabledCondition.class)
    public DataSourceProperties authDataSourceProperties() {
        log.debug("Zusätzlich Datenbankkonfiguration");
        return new DataSourceProperties();
    }


    @Bean
    @ConfigurationProperties("spring.datasource")
    @Conditional(EnabledCondition.class)
    public DataSourceProperties springDataSourceProperties() {
        log.debug("Spring Datenbankkonfiguration");
        return new DataSourceProperties();
    }


    @Bean
    @ConfigurationProperties("auth.datasource")
    @Conditional(EnabledCondition.class)
    public DataSource authDataSource() {
        log.debug("Zusätzliche Datenbank");
        return authDataSourceProperties()
            .initializeDataSourceBuilder()
            .build();
    }


    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource")
    @Conditional(EnabledCondition.class)
    public DataSource springDataSource() {
        log.debug("Spring primäre Datenbank");
        return springDataSourceProperties()
            .initializeDataSourceBuilder()
            .build();
    }

}
