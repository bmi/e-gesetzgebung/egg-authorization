// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * represents permission data
 */
@Data
@Builder
public class PermissionDTO {

	@NotNull
	private String benutzerId;
	@NotNull
	private String ressourceId;
	@NotNull
	private String ressourceTyp;
	private String rolle;
	private List<String> aktionen;
}
