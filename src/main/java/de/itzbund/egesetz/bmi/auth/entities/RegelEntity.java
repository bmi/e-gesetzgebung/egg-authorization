// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * A domain entity representing a comment with some metadata.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "regeln")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RegelEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@EqualsAndHashCode.Exclude
	@ManyToOne
	@JoinColumn(name = "ressource_typ_id", foreignKey = @ForeignKey(name = "FK_ressourcetype_regel__regeln_id"))
	private RessourceTypEntity ressourceTyp;

	@EqualsAndHashCode.Exclude
	@ManyToOne
	@JoinColumn(name = "rolle_id", foreignKey = @ForeignKey(name = "FK_rolle_regel__regel_id"))
	private RolleEntity rolle;

	@EqualsAndHashCode.Exclude
	@ManyToOne
	@JoinColumn(name = "status_id", foreignKey = @ForeignKey(name = "FK_status_regel__regel_id"))
	private StatusEntity status;

	@EqualsAndHashCode.Exclude
	@ManyToOne
	@JoinColumn(name = "aktion_id", foreignKey = @ForeignKey(name = "FK_aktion_regel__regel_id"))
	private AktionEntity aktion;

	@EqualsAndHashCode.Exclude
	private String bezeichnung;

}
