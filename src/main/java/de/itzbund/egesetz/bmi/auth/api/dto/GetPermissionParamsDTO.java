// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.dto;

import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;
import java.util.Optional;

@Data
@Builder
public class GetPermissionParamsDTO {

    private Optional<Collection<String>> benutzerIds;

    private Optional<Collection<AktionEntity>> aktionen;

    private Optional<Collection<RessourceTypEntity>> ressourceTypen;

    private Optional<Collection<String>> ressourcenIds;
    
    private Optional<Collection<RolleEntity>> rollen;

}
