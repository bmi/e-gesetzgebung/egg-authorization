// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.enums;

/**
 * gives further control about data to be analyzed
 */
public enum GetPermissionOptionType {
	FAST_NO_STATUS_NO_AKTIONEN,
	SLOW_CHECK_STATUS_GET_AKTIONEN
}
