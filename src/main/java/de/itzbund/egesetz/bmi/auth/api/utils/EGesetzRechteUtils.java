// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.utils;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import de.itzbund.egesetz.bmi.auth.api.dto.PermissionDTO;
import de.itzbund.egesetz.bmi.auth.api.dto.StatusResponseDTO;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungGlobaleRolleEntity;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

@Log4j2
@UtilityClass
public class EGesetzRechteUtils {

	public final String KEINE_ZUORDNUNGEN_GEFUNDEN_FUER_BENUTZER_ID_UND_RESSOURCEN_TYP =
		"Keine Zuordnungen gefunden für benutzerId {} und ressourcenTyp {}";
	public final String FOUND_RESSOURCE_IDS = "Found ressourceIds: {}";
	public final String SCHREIBRECHTE_ENTFERNT = "SCHREIBRECHTE_ENTFERNT";
	public final String SCHREIBRECHTE_HINZU = "SCHREIBRECHTE_HINZU";
	public final String LESERECHTE_HINZU = "LESERECHTE_HINZU";
	public final String AKTION_LESEN = "LESEN";

	public void validateRessourceId(String ressourceId, List<String> errors) {
		validateRessourcenIds(Optional.of(Collections.singletonList(ressourceId)), errors);
	}

	/**
	 * Filters list of ressource ids in the way that duplicates are removes and an error occurs if at least no non-blank values is in the list.
	 *
	 * @param ressourcenIds List of ressource id
	 * @param errors        Error is the way described above
	 * @return A list of errors
	 */
	public List<String> validateRessourcenIds(Optional<Collection<String>> ressourcenIds, List<String> errors) {
		String errorMessage = "Ressourcen-IDs wurden nicht gesetzt";
		return validateIds(ressourcenIds, errors, errorMessage);
	}

	public void validateBenutzerId(String benutzerId, List<String> errors) {
		validateBenutzerIds(Optional.of(Collections.singletonList(benutzerId)), errors);
	}

	/**
	 * Filters list of ressource ids in the way that duplicates are removes and an error occurs if at least no non-blank values is in the list.
	 *
	 * @param benutzerIds
	 * @param errors
	 * @return
	 */
	public List<String> validateBenutzerIds(Optional<Collection<String>> benutzerIds, List<String> errors) {
		String errorMessage = "Benutzer-IDs wurden nicht gesetzt";
		return validateIds(benutzerIds, errors, errorMessage);
	}

	/**
	 * Filters list of  ids in the way that duplicates are removes and an error occurs if at least no non-blank values is in the list.
	 */
	private List<String> validateIds(Optional<Collection<String>> ids, List<String> errors, String errorMessage) {
		List<String> idsTmp = null;
		if (ids.isPresent()) {
			idsTmp = ids.get().stream()
				.filter(Objects::nonNull)
				.filter(StringUtils::isNotBlank)
				.distinct()
				.collect(Collectors.toList());
			if (idsTmp.isEmpty()) {
				errors.add(errorMessage);
			}
		}
		return idsTmp;
	}

	// v---------------------------------------------------------------------------------

	// ^---------------------------------------------------------------------------------

	/**
	 * Creates the 'Not found' error message
	 *
	 * @param typ         The type
	 * @param bezeichnung Another part of the message
	 * @return The message
	 */
	public String createNotFoundErrorMsg(String typ, String bezeichnung) {
		return MessageFormat.format("{0} ''{1}'' wurde nicht gefunden", typ, bezeichnung);
	}

	/**
	 * Creates an exception for the list of errors
	 *
	 * @param errors List of errors
	 * @return The exception
	 */
	public IllegalArgumentException createValidationException(List<String> errors) {
		String errorString = String.join(", ", errors);
		throw new IllegalArgumentException("Ein oder mehrere Felder fehlen oder wurden falsch gesetzt: " + errorString);
	}

	/**
	 * Extracts the root cause of an exception
	 *
	 * @param ex The exception
	 * @return Root cause of the given exception
	 */
	public Throwable getRootCause(Throwable ex) {
		while (ex.getCause() != null) {
			ex = ex.getCause();
		}
		return ex;
	}

	// ----------------------------------------------------------------------------------

	/**
	 * Extracts the 'Zuordnungen' for the role of 'Schreiber'
	 *
	 * @param existingZuordnungEntities All 'Zuordnungen'
	 * @return Only 'Zuordnungen' for role of 'Schreiber'
	 */
	public List<ZuordnungEntity> filterSchreiber(List<ZuordnungEntity> existingZuordnungEntities) {
		return existingZuordnungEntities.stream().filter(e -> "SCHREIBER".equals(e.getRolle().getBezeichnung())).collect(Collectors.toList());
	}

	/**
	 * Extracts the 'Zuordnungen' for the role of 'Ersteller' and 'Schreiber'
	 *
	 * @param existingZuordnungEntities All 'Zuordnungen'
	 * @return Only 'Zuordnungen' for role of 'Schreiber'
	 */
	public List<ZuordnungEntity> filterErstellerAndSchreiber(List<ZuordnungEntity> existingZuordnungEntities) {
		return existingZuordnungEntities.stream()
			.filter(e -> "SCHREIBER".equals(e.getRolle().getBezeichnung())
				|| "ERSTELLER".equals(e.getRolle().getBezeichnung()))
			.collect(Collectors.toList());
	}

	/**
	 * Extracts the user id for the role of 'Leser' from 'Zuordnungen'
	 *
	 * @param existingZuordnungEntities All 'Zuordnungen'
	 * @return Only 'Zuordnungen' for role of 'Schreiber'
	 */
	public List<String> filterLeser(List<ZuordnungEntity> existingZuordnungEntities) {
		return existingZuordnungEntities.stream().filter(e -> "LESER".equals(e.getRolle().getBezeichnung())).map(ZuordnungEntity::getBenutzerId)
			.collect(Collectors.toList());
	}

	// ----------------------------------------------------------------------------------

	/**
	 * Extracts the first 'useable' status response of a list of status responses. It logs on emptiness as well of multi occurrences.
	 *
	 * @param zuordnung
	 * @param statusResponses
	 * @return
	 */
	public Optional<StatusResponseDTO> pickStatusResponse(ZuordnungEntity zuordnung, List<StatusResponseDTO> statusResponses) {
		List<StatusResponseDTO> statusResponseList = CollectionUtils.emptyIfNull(statusResponses)
			.stream()
			.filter(e -> StringUtils.equalsIgnoreCase(e.getRessourcenId(), zuordnung.getRessourceId()))
			.collect(Collectors.toList());
		if (statusResponseList.isEmpty()) {
			log.warn("no status response found for Zuordnung with id {}", zuordnung.getId());
			return Optional.empty();
		}
		if (statusResponseList.size() > 1) {
			log.warn("found {} status responses for Zuordnung with id {}", statusResponseList.size(), zuordnung.getId());
		}
		return Optional.of(IterableUtils.first(statusResponseList));
	}

	public List<String> clearStringList(Collection<String> stringRepresentation, List<String> errors, String errorMessage) {
		List<String> stringList = CollectionUtils.emptyIfNull(stringRepresentation)
			.stream()
			.map(StringUtils::trimToNull)
			.filter(Objects::nonNull)
			.distinct()
			.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(stringList)) {
			errors.add(errorMessage);
		}
		return stringList;
	}

	public PermissionDTO buildPermissionDTO(ZuordnungEntity zuordnung, Collection<AktionEntity> aktionenList) {
		return PermissionDTO.builder()
			.benutzerId(zuordnung.getBenutzerId())
			.ressourceId(zuordnung.getRessourceId())
			.ressourceTyp(Optional.ofNullable(zuordnung.getRessourceTyp())
				.map(RessourceTypEntity::getBezeichnung).orElseGet(() -> {
					log.warn("Zuordnung with id {} has no ressource type!", zuordnung.getId());
					return null;
				}))
			.rolle(Optional.ofNullable(zuordnung.getRolle())
				.map(RolleEntity::getBezeichnung).orElseGet(() -> {
					log.warn("Zuordnung with id {} has no role!", zuordnung.getId());
					return null;
				}))
			.aktionen(Optional.ofNullable(aktionenList)
				.map(l -> l.stream().map(AktionEntity::getBezeichnung).collect(Collectors.toList())).orElse(null))
			.build();
	}

	public PermissionDTO buildPermissionDTOForGlobaleRollen(ZuordnungGlobaleRolleEntity zuordnungGlobaleRolle, NutzerEntity nutzer) {
		return PermissionDTO.builder()
			.benutzerId(nutzer.getGid())
			.ressourceId(zuordnungGlobaleRolle.getRessourceId())
			.ressourceTyp(Optional.ofNullable(zuordnungGlobaleRolle.getRessourceTyp())
				.map(RessourceTypEntity::getBezeichnung).orElseGet(() -> {
					log.warn("ZuordnungGlobaleRolle with id {} has no ressource type!", zuordnungGlobaleRolle.getId());
					return null;
				}))
			.rolle(Optional.ofNullable(zuordnungGlobaleRolle.getGlobaleRolle())
				.map(RolleEntity::getBezeichnung).orElseGet(() -> {
					log.warn("ZuordnungGlobaleRolle with id {} has no role!", zuordnungGlobaleRolle.getId());
					return null;
				}))
			.aktionen(List.of(AKTION_LESEN))
			.build();
	}

}
