// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.possibilities;

import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.repositories.RessourceTypenRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Component
public class RessourceTypen {

    @Autowired
    private RessourceTypenRepository ressourceTypenRepository;

    private List<String> ressourceTyps = null;


    List<String> getRessourceTypen() {
        if (ressourceTyps == null) {
            List<RessourceTypEntity> alleRessourceTypen = ressourceTypenRepository.findAll();
            ressourceTyps = alleRessourceTypen.stream().map(r -> r.getBezeichnung()).collect(Collectors.toList());
        }

        log.debug("Provided RessourceTypen: {}", ressourceTyps);
        return ressourceTyps;
    }

}
