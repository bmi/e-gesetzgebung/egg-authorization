// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.configuration;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Component
@Conditional(EnabledCondition.class)
public class DatabaseMigration implements ApplicationListener<ContextRefreshedEvent> {

    public static final String VENDOR = "{vendor}";
    public static final String MYSQL = "mysql";
    private final AuthDatasourceConfiguration databaseConfigurations;

    @Value("${auth.flyway.locations:}")
    String authFlywayAdditionalPath;
    @Value("${auth.flyway.enable:false}")
    boolean authFlywayEnabled;
    @Value("${auth.flyway.baselineOnMigrate:false}")
    boolean authFlywayBaselineOnMigrate;


    public DatabaseMigration(AuthDatasourceConfiguration databaseConfigurations) {
        this.databaseConfigurations = databaseConfigurations;
    }


    private static List<String> getPathReplacedAdditionalLocations(List<String> locations, String vendor) {
        List<String> results = new ArrayList<>();

        if (vendor != null) {
            for (String path : locations) {
                if (path.contains(VENDOR)) {
                    path = path.replace(VENDOR, vendor);
                }

                results.add(path);
            }
        }

        return results;
    }


    private static String getVendorFromDatabaseConnection(DataSource dataSource) {
        String vendor = null;
        if (dataSource instanceof HikariDataSource) {
            HikariDataSource hikariDataSource = (HikariDataSource) dataSource;
            String jdbcUrl = hikariDataSource.getJdbcUrl();
            if (jdbcUrl.contains(MYSQL)) {
                vendor = MYSQL;
            } else if (jdbcUrl.contains("h2")) {
                vendor = "h2";
            }
        }
        return vendor;
    }


    /**
     * If context is loaded we can do the flyway migration manually for auth-part.
     *
     * @param contextRefreshedEvent the event to respond to
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (authFlywayEnabled) {
            log.debug("Sekundäre Flyway Migration");
            DataSource dataSource = databaseConfigurations.authDataSource();
            List<String> locations = new ArrayList<>();

            String[] vendorBasedLocations = replaceDatabaseVendor(dataSource, locations);
            log.debug("Lokations der Datenbankskripte: {}", vendorBasedLocations);

            Flyway flyway = Flyway.configure()
                .dataSource(dataSource)
                .locations(vendorBasedLocations)
                .baselineOnMigrate(authFlywayBaselineOnMigrate)
                // Additional Flyway configurations can be added here
                .load();

            // Initialize or migrate the database
            flyway.migrate();
            log.debug("Flyway Migration abgeschlossen.");
        }

    }


    /**
     * Quick and dirty
     */
    private String[] replaceDatabaseVendor(DataSource dataSource, List<String> currentLocations) {
        List<String> locations = getListFromString(authFlywayAdditionalPath);
        currentLocations.addAll(locations);

        String vendor = getVendorFromDatabaseConnection(dataSource);
        List<String> additionalLocations =
            getPathReplacedAdditionalLocations(currentLocations, vendor);

        return additionalLocations.toArray(new String[0]);
    }


    private List<String> getListFromString(String flywayAdditionalPath) {
        List<String> results = new ArrayList<>();
        if (flywayAdditionalPath != null) {
            String[] split = flywayAdditionalPath.split(",");
            for (String s : split) {
                results.add(s.trim());
            }

        }

        return results;
    }

}
