// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungGlobaleRolleEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("authDataSource")
@Conditional(EnabledCondition.class)
public interface ZuordnungenGlobaleRollenRepository extends JpaRepository<ZuordnungGlobaleRolleEntity, Long> {

	Optional<List<ZuordnungGlobaleRolleEntity>> findByRessourceIdAndAndRolleId(String ressourceId, Long rolleId);

	Optional<ZuordnungGlobaleRolleEntity> findByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle(RessourceTypEntity resourceTypEntity, Long rolleId,
		String ressourceId,
		RolleEntity globaleRolle);

	Optional<List<ZuordnungGlobaleRolleEntity>> findByRessourceTypAndRolleIdAndGlobaleRolle(RessourceTypEntity ressourceTyp, Long rolleId, RolleEntity globaleRolle);

	@Transactional
	int deleteByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle(RessourceTypEntity resourceTypEntity, Long rolleId, String ressourceId,
		RolleEntity globaleRolle);

}
