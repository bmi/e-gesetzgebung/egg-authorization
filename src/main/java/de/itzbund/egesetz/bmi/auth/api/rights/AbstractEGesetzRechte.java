// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.rights;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import de.itzbund.egesetz.bmi.auth.api.dto.AlleRessourcenFuerBenutzerUndRessourceTypParamsDTO;
import de.itzbund.egesetz.bmi.auth.api.dto.GetPermissionParamsDTO;
import de.itzbund.egesetz.bmi.auth.api.dto.PermissionDTO;
import de.itzbund.egesetz.bmi.auth.api.dto.RessourceParamsDTO;
import de.itzbund.egesetz.bmi.auth.api.dto.StatusResponseDTO;
import de.itzbund.egesetz.bmi.auth.api.dto.ZuordnungAktionenDTO;
import de.itzbund.egesetz.bmi.auth.api.enums.GetPermissionOptionType;
import de.itzbund.egesetz.bmi.auth.api.enums.UpdateResultType;
import de.itzbund.egesetz.bmi.auth.api.utils.EGesetzRechteUtils;
import de.itzbund.egesetz.bmi.auth.configuration.EnabledCondition;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelAdministrationEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.StatusEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungGlobaleRolleEntity;
import de.itzbund.egesetz.bmi.auth.repositories.AktionenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RegelnAdministrationRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RegelnRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RessourceTypenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RollenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.StatusRepository;
import de.itzbund.egesetz.bmi.auth.repositories.ZuordnungenGlobaleRollenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.ZuordnungenRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Conditional(EnabledCondition.class)
public abstract class AbstractEGesetzRechte implements EGesetzRechte {

	@Autowired(required = false)
	private RegelnRepository regelnRepository;
	@Autowired(required = false)
	private RessourceTypenRepository ressourceTypenRepository;
	@Autowired(required = false)
	private StatusRepository statusRepository;
	@Autowired(required = false)
	private ZuordnungenRepository zuordnungenRepository;
	@Autowired(required = false)
	private RollenRepository rollenRepository;
	@Autowired(required = false)
	private AktionenRepository aktionenRepository;
	@Autowired(required = false)
	private ZuordnungenGlobaleRollenRepository zuordnungenGlobalerollenRepository;
	@Autowired(required = false)
	private NutzerEntityRepository nutzerEntityRepository;

	@Autowired(required = false)
	private RegelnAdministrationRepository regelnAdministrationRepository;

	private List<RessourceTypEntity> alleRessourceTypen = new ArrayList<>();
	private List<RolleEntity> alleRollen = new ArrayList<>();

	// v---------------------------------------------------------------------------------

	/**
	 * The status of a ressource is held in the main application. An implementation of this abstract class helps to additional act to this type of state.
	 *
	 * @param ressourcenId  The id of the resource.
	 * @param ressourcenTyp The type of the resource, e.g. Dokument
	 * @return The Status of the resource, like DRAFT or similar
	 */
	public abstract String getStatusForResourceType(String ressourcenTyp, String ressourcenId); // NOSONAR

	/**
	 * Gets a list of status for several ids of one specific ressource type.
	 *
	 * @param ressourcenTyp The type of the resource, e.g. Dokument
	 * @param ressourcenIds The ids of this specific resource type.
	 * @return A list of pairs of resource id and status
	 */
	@Override
	public List<StatusResponseDTO> getStatusForResourceType(String ressourcenTyp, Set<String> ressourcenIds) {
		return CollectionUtils.emptyIfNull(ressourcenIds)
			.stream()
			.filter(Objects::nonNull)
			.map(ressourcenId -> StatusResponseDTO.builder()
				.ressourcenId(ressourcenId)
				.status(getStatusForResourceType(ressourcenTyp, ressourcenId))
				.build())
			.collect(Collectors.toList());
	}

	// ^---------------------------------------------------------------------------------
	// v---------------------------------------------------------------------------------

	// @formatter:off
    /**
     * Erzeugt ein PermissionDTO mit BenutzerId, RessourceId und RessourceType für die angegebenen Parameter.
     *
     * Optional: Aktionen können berücksichtigt werden oder NICHT
     *
     * @param benutzerId    Id of the user
     * @param aktionen      Actions he wants to do
     * @param ressourcenTyp Type of resource, e.g. Dokument
     * @param ressourceId   The id of that resource
     * @param optionType    check status of entities? (potentially costly operation!)
     * @return The first Permission of the list
     */
    // @formatter:on
	@Override
	public Optional<PermissionDTO> getPermission(String benutzerId, Collection<String> aktionen, String ressourcenTyp, String ressourceId,
		GetPermissionOptionType optionType) {

		List<PermissionDTO> permissions = getPermissions(
			Optional.of(Stream.of(benutzerId).collect(Collectors.toList())),
			Optional.of(aktionen),
			Optional.of(Stream.of(ressourcenTyp).collect(Collectors.toList())),
			Optional.of(Stream.of(ressourceId).collect(Collectors.toList())),
			Optional.empty(),
			optionType,
			Optional.of(Pageable.ofSize(1))
		);

		if (permissions.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(IterableUtils.first(permissions));
	}

	/**
	 * Checks if action is allowed by the given parameters. As status is provided by the main application #getStatusForResourceType(String, String) must be
	 * implemented to present the information.
	 *
	 * @param rolle         rolle of the user
	 * @param aktionen      Actions he wants to do
	 * @param ressourcenTyp Type of resource, e.g. Dokument
	 * @param ressourceId   The id of that resource
	 * @param optionType    check status of entities? (potentially costly operation!)
	 * @return if action is allowed or not
	 */
	public Optional<PermissionDTO> getPermissionGlobalerolle(String rolle, Collection<String> aktionen, String ressourcenTyp, String ressourceId,
		GetPermissionOptionType optionType) {
		List<PermissionDTO> permissions = getPermissionsGlobalerolle(rolle, ressourcenTyp, ressourceId);
		if (permissions.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(IterableUtils.first(permissions));
	}

	/**
	 * Checks if action is allowed by the given parameters. As status is provided by the main application #getStatusForResourceType(String, String) must be
	 * implemented to present the information.
	 *
	 * @param benutzerId    gId of the user
	 * @param rolle         rolle of the user
	 * @param ressourcenTyp Type of resource, e.g. Dokument
	 * @param ressourceId   The id of that resource
	 * @return if action is allowed or not
	 */
	public Optional<PermissionDTO> getPermissionGlobalerolleByBenutzerId(String benutzerId, String rolle, String ressourcenTyp, String ressourceId) {
		List<PermissionDTO> permissions = getPermissionsGlobalerolle(rolle, ressourcenTyp, ressourceId);
		List<PermissionDTO> permissionsbybenutzerId = permissions.stream().filter(p -> p.getBenutzerId().equals(benutzerId)).collect(Collectors.toList());
		if (permissionsbybenutzerId.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(IterableUtils.first(permissionsbybenutzerId));
	}


	/**
	 * Checks if action is allowed by the given parameters. As status is provided by the main application #getStatusForResourceType(String, String) must be
	 * implemented to present the information.
	 *
	 * @param rolle         rolle of the user
	 * @param ressourcenTyp Type of resource, e.g. Dokument
	 * @param ressourceId   The id of that resource
	 * @return a list of {@link PermissionDTO}
	 */
	public List<PermissionDTO> getPermissionsGlobalerolle(String rolle, String ressourcenTyp, String ressourceId) {
		Optional<RolleEntity> rolleEntity = rollenRepository.findByBezeichnung(rolle);
		Optional<List<ZuordnungGlobaleRolleEntity>> zuordnungGlobalerolleEntitiesOptional = zuordnungenGlobalerollenRepository.findByRessourceIdAndAndRolleId(
			ressourceId, rolleEntity.get().getId());
		if (zuordnungGlobalerolleEntitiesOptional.isPresent()) {
			List<ZuordnungGlobaleRolleEntity> zuordnungGlobalerolleEntities = zuordnungGlobalerolleEntitiesOptional.get();
			List<RolleEntity> globaleRollen = zuordnungGlobalerolleEntities.stream().map(ZuordnungGlobaleRolleEntity::getGlobaleRolle)
				.collect(Collectors.toList());
			List<NutzerEntity> nutzerEntity = nutzerEntityRepository.findAllByRolleIn(globaleRollen);
			List<PermissionDTO> permissions = nutzerEntity.stream().map(
				nutzer -> PermissionDTO.builder().benutzerId(nutzer.getGid()).rolle(nutzer.getRolle().getBezeichnung()).ressourceTyp(ressourcenTyp)
					.ressourceId(ressourceId).build()).collect(
				Collectors.toList());
			if (permissions.isEmpty()) {
				return new ArrayList<>();
			}
			return permissions;
		}
		return new ArrayList<>();
	}

	/**
	 * Checks if action is allowed by the given parameters.
	 *
	 * @param benutzerId    benutzerId of the user
	 * @param rolle         role of the user
	 * @param ressourcenTyp Type of resource, e.g. Dokument
	 * @return a list of {@link PermissionDTO}
	 */
	public List<PermissionDTO> getPermissionsForGlobalerolle(String benutzerId, String rolle, String ressourcenTyp) {
		Optional<RolleEntity> rolleEntityLocal = rollenRepository.findByBezeichnung(rolle); // federfüherer Rolle
		Optional<NutzerEntity> sessionUser = nutzerEntityRepository.findByGid(benutzerId); // session User
		if (rolleEntityLocal.isPresent() && sessionUser.isPresent()) {
			Optional<List<ZuordnungGlobaleRolleEntity>> zuordnungGlobalerolleEntitiesOptional = zuordnungenGlobalerollenRepository
				.findByRessourceTypAndRolleIdAndGlobaleRolle(getRessourceTypEntity(ressourcenTyp).get(), rolleEntityLocal.get().getId(),
					sessionUser.get().getRolle());
			return zuordnungGlobalerolleEntitiesOptional.get().stream().map(ge -> {
				return EGesetzRechteUtils.buildPermissionDTOForGlobaleRollen(ge, sessionUser.get());
			}).collect(
				Collectors.toList());
		}
		return new ArrayList<>();
	}

	// @formatter:off
    /**
     * Liefert für die angegebenen Parameter eine Liste an PermissionDTO-Objekten mit der Benutzer Id, der Ressource (Typ u. Id), die Rolle des Benutzers und
     * eine Liste von Aktionen, die Nutzer ausüben darf.
     *
     * Optional: Aktionen können berücksichtigt werden oder NICHT
     *
     * @param benutzerIds    Eine Liste von Benutzer id
     * @param aktionen       Eine Liste von Aktionen
     * @param ressourceTypen Eine Liste von RessourcenTypen
     * @param ressourcenIds  Eine Liste von RessourcenIds
     * @param rollen         Eine Liste von Rollen
     * @param optionType     Eine Option, ob Aktion/Status im Ergebnis berücksichtigt werden sollen oder nicht
     * @param pageable       Die Option der Paginierung
     * @return Eine Liste von PermissionDTOs
     */
    // @formatter:on
	protected List<PermissionDTO> getPermissions(Optional<Collection<String>> benutzerIds, Optional<Collection<String>> aktionen,
		Optional<Collection<String>> ressourceTypen, Optional<Collection<String>> ressourcenIds, Optional<Collection<String>> rollen,
		GetPermissionOptionType optionType, Optional<Pageable> pageable) {

		GetPermissionParamsDTO params = validateGetPermissions(benutzerIds, aktionen, ressourceTypen, ressourcenIds, rollen);

		Specification<ZuordnungEntity> spec = zuordnungenRepository.createSpecification(
			params.getBenutzerIds(),
			params.getAktionen(),
			params.getRessourceTypen(),
			params.getRessourcenIds(),
			params.getRollen()
		);

		List<ZuordnungEntity> zuordnungen;
		if (pageable.isPresent()) {
			Page<ZuordnungEntity> page = zuordnungenRepository.findAll(spec, pageable.get());
			zuordnungen = page.getContent();
		} else {
			zuordnungen = zuordnungenRepository.findAll(spec);
		}
		if (zuordnungen.isEmpty()) {
			return new ArrayList<>();
		}

		List<ZuordnungAktionenDTO> zuordnungenAndAktionen;
		if (optionType == GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN) {
			//summary: no further filtering, no expensive calls to the main application
			//pro: obviously the fastest option
			//con: with this option you will not receive the list of actions (LESEN, SCHREIBEN...)
			//     and it's your responsibility to carefully do the status filtering in your main application
			zuordnungenAndAktionen = zuordnungen.stream()
				.map(z -> ZuordnungAktionenDTO.builder()
					.zuordnung(z)
					.aktionen(null)
					.build())
				.collect(Collectors.toList());
		} else if (optionType == GetPermissionOptionType.SLOW_CHECK_STATUS_GET_AKTIONEN) {
			//summary: carefully considers the current status of the resource, that's why we have to make calls to the main application to get the status first
			//pro: you will get a complete and reliable list of permissions
			//con: this branch is pretty expensive! Only use it for very small chunks of requested permissions
			zuordnungenAndAktionen = filterByStatusAndAktionen(zuordnungen, params.getAktionen());
		} else {
			throw new IllegalArgumentException("option type is empty or not implemented yet: " + optionType);
		}

		return zuordnungenAndAktionen.stream()
			.map(e -> {
				ZuordnungEntity zuordnung = e.getZuordnung();
				Collection<AktionEntity> aktionenList = e.getAktionen();

				return EGesetzRechteUtils.buildPermissionDTO(zuordnung, aktionenList);
			})
			.collect(Collectors.toList());
	}

	// ^---------------------------------------------------------------------------------
	// v---------------------------------------------------------------------------------

	/**
	 * Validiert eine Liste von Aktionen und gibt eine Liste an gültigen Aktion-Entities.
	 *
	 * @param aktionen Aktionen als String repräsentiert.
	 * @param errors   Eine liste mit ggf. Fehlern
	 * @return List of Aktionen (Entities)
	 */
	private List<AktionEntity> validateAktionen(Optional<Collection<String>> aktionen, List<String> errors) {
		List<AktionEntity> aktionenTmp = null;
		if (aktionen.isPresent()) {
			aktionenTmp = validateAktionen(aktionen.get(), errors);
		}
		return aktionenTmp;
	}

	private List<AktionEntity> validateAktionen(Collection<String> aktionen, List<String> errors) {
		List<String> aktionenList = EGesetzRechteUtils.clearStringList(aktionen, errors, "Aktionen wurden nicht gesetzt");
		List<AktionEntity> result = new ArrayList<>();

		for (String aktion : aktionenList) {
			getAktionEntity(aktion)
				.ifPresentOrElse(
					result::add,
					() -> errors.add(EGesetzRechteUtils.createNotFoundErrorMsg("Aktion", aktion)));
		}
		return result;
	}

	// ^---------------------------------------------------------------------------------
	// v---------------------------------------------------------------------------------

	private List<RolleEntity> validateRollen(Optional<Collection<String>> rollen, List<String> errors) {
		List<RolleEntity> rollenTmp = null;
		if (rollen.isPresent()) {
			List<String> tmp = rollen.get().stream()
				.filter(StringUtils::isNotBlank)
				.distinct()
				.collect(Collectors.toList());
			if (tmp.isEmpty()) {
				errors.add("Rollen wurden nicht gesetzt");
			} else {
				rollenTmp = tmp.stream()
					.map(rolle -> validateRolle(rolle, errors))
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
			}
		}
		return rollenTmp;
	}

	private RolleEntity validateRolle(String rolle, List<String> errors) {
		RolleEntity rolleEntity = null;
		if (StringUtils.isBlank(rolle)) {
			errors.add("Rolle wurde nicht gesetzt");
		} else {
			rolleEntity = getRolleEntity(rolle)
				.orElseGet(() -> {
					errors.add(EGesetzRechteUtils.createNotFoundErrorMsg("Rolle", rolle));
					return null;
				});
		}
		return rolleEntity;
	}

	// ^---------------------------------------------------------------------------------
	// v---------------------------------------------------------------------------------

	private List<RessourceTypEntity> validateRessourcenTypen(Optional<Collection<String>> ressourceTypen, List<String> errors) {
		List<RessourceTypEntity> ressourceTypenTmp = null;
		if (ressourceTypen.isPresent()) {
			List<String> tmp = ressourceTypen.get().stream()
				.filter(StringUtils::isNotBlank)
				.distinct()
				.collect(Collectors.toList());
			if (tmp.isEmpty()) {
				errors.add("Ressource-Typen wurden nicht gesetzt");
			} else {
				ressourceTypenTmp = tmp.stream()
					.map(ressourceTyp -> validateResourcenTyp(ressourceTyp, errors))
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
			}
		}
		return ressourceTypenTmp;
	}

	private RessourceTypEntity validateResourcenTyp(String ressourcenTyp, List<String> errors) {
		RessourceTypEntity ressourceTypEntity = null;
		if (StringUtils.isBlank(ressourcenTyp)) {
			errors.add("Ressourcen-Typ wurde nicht gesetzt");
		} else {
			ressourceTypEntity = getRessourceTypEntity(ressourcenTyp)
				.orElseGet(() -> {
					errors.add(EGesetzRechteUtils.createNotFoundErrorMsg("Ressource-Typ", ressourcenTyp));
					return null;
				});
		}
		return ressourceTypEntity;
	}

	// ^---------------------------------------------------------------------------------

	// =========================================================

	/**
	 * Nach Validierung der Eingabewerte.
	 */
	private GetPermissionParamsDTO validateGetPermissions(Optional<Collection<String>> benutzerIds, Optional<Collection<String>> aktionen,
		Optional<Collection<String>> ressourceTypen, Optional<Collection<String>> ressourcenIds, Optional<Collection<String>> rollen) {

		List<String> errors = new ArrayList<>();

		List<String> benutzerIdsTmp = EGesetzRechteUtils.validateBenutzerIds(benutzerIds, errors);
		List<AktionEntity> aktionenTmp = validateAktionen(aktionen, errors);
		List<RessourceTypEntity> ressourceTypenTmp = validateRessourcenTypen(ressourceTypen, errors);
		List<String> ressourcenIdsTmp = EGesetzRechteUtils.validateRessourcenIds(ressourcenIds, errors);
		List<RolleEntity> rollenTmp = validateRollen(rollen, errors);

		if (errors.isEmpty()) {
			return GetPermissionParamsDTO.builder()
				.benutzerIds(Optional.ofNullable(benutzerIdsTmp))
				.aktionen(Optional.ofNullable(aktionenTmp))
				.ressourceTypen(Optional.ofNullable(ressourceTypenTmp))
				.ressourcenIds(Optional.ofNullable(ressourcenIdsTmp))
				.rollen(Optional.ofNullable(rollenTmp))
				.build();
		} else {
			throw EGesetzRechteUtils.createValidationException(errors);
		}
	}

	private RessourceParamsDTO validateResourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle) {
		List<String> errors = new ArrayList<>();
		EGesetzRechteUtils.validateBenutzerId(benutzerId, errors);
		EGesetzRechteUtils.validateRessourceId(ressourceId, errors);
		RessourceTypEntity ressourceTypEntity = validateResourcenTyp(ressourcenTyp, errors);
		RolleEntity rolleEntity = validateRolle(rolle, errors);

		if (errors.isEmpty()) {
			return RessourceParamsDTO.builder()
				.ressourceTypEntity(ressourceTypEntity)
				.rolleEntity(rolleEntity)
				.build();
		} else {
			throw EGesetzRechteUtils.createValidationException(errors);
		}
	}

	private AlleRessourcenFuerBenutzerUndRessourceTypParamsDTO validateAlleRessourcenFuerBenutzerUndRessourceTyp(String benutzerId, String ressourcenTyp) {
		List<String> errors = new ArrayList<>();
		EGesetzRechteUtils.validateBenutzerId(benutzerId, errors);
		RessourceTypEntity ressourceTypEntity = validateResourcenTyp(ressourcenTyp, errors);

		if (errors.isEmpty()) {
			return AlleRessourcenFuerBenutzerUndRessourceTypParamsDTO.builder()
				.ressourceTypEntity(ressourceTypEntity)
				.build();
		} else {
			throw EGesetzRechteUtils.createValidationException(errors);
		}
	}

	// v---------------------------------------------------------------------------------

	/**
	 * Checks if action is allowed by the given parameters. As status is provided by the main application #getStatusForResourceType(String, String) must be
	 * implemented to present the information.
	 *
	 * @param benutzerId    Id of the user
	 * @param aktion        Action he wants to do
	 * @param ressourcenTyp Type of resource, e.g. Dokument
	 * @param ressourcenId  The id of that resource
	 * @return if action is allowed or not
	 */
	public boolean isAllowed(String benutzerId, String aktion, String ressourcenTyp, String ressourcenId) {
		// Schreibrechte können befristet weitergegeben werden.
		// wenn die Befristung abgelaufen ist, müssen die 'alten' Rechte wieder gültig werden.
		clearInvalidPermissionsByPreviousSetLimitation(ressourcenId);

		Optional<RessourceTypEntity> resourceTypEntity = getRessourceTypEntity(ressourcenTyp);
		log.debug("RessourceTypen: {}", resourceTypEntity);  // NOSONAR

		if (resourceTypEntity.isPresent()) {
			// Welche Rollen habe ich?
			List<RolleEntity> rollenEntities = getRollenAusZuordnungOrNull(resourceTypEntity.get(), ressourcenId, benutzerId, false);

			// Wenn ich keine Rollen habe, sind sie vielleicht nur deaktiviert
			// in diesem Fall muss LESEN trotzdem möglich sein
			if (rollenEntities.isEmpty()) {

				List<RolleEntity> rollenEntities2 = getRollenAusZuordnungOrNull(resourceTypEntity.get(), ressourcenId, benutzerId, true);
				if (!rollenEntities2.isEmpty() && "LESEN".equals(aktion)) {
					return true;
				}

			} else {

				log.debug("Rollen: {}", rollenEntities);  // NOSONAR
				StatusEntity status = getStatusEntityForResourceType(ressourcenTyp, ressourcenId);
				List<StatusEntity> statusEntities = Stream.of(status).filter(Objects::nonNull).collect(Collectors.toList());
				List<AktionEntity> aktionenEntities = getAktionenAusRegeln(resourceTypEntity.get(), rollenEntities, statusEntities);

				log.debug("Aktionen: {}", aktionenEntities);
				Optional<AktionEntity> firstFoundAktion = aktionenEntities.stream()
					.filter(a -> a.getBezeichnung().equals(aktion)).findFirst();

				log.debug("Übereinstimmung: {}", firstFoundAktion.isPresent());
				return firstFoundAktion.isPresent();

			}

		}

		log.debug("Method isAllowed failed.");
		return false;
	}

	/**
	 * Findet die Rollen zu den gegebenen Parametern. (Wird nur von isAllowed benutzt).
	 */
	private List<RolleEntity> getRollenAusZuordnungOrNull(RessourceTypEntity ressourcenTypEntity, String ressourcenId, String benutzerId,
		boolean isDeactivated) {

		Optional<List<ZuordnungEntity>> optionalZuordnungEntityList = zuordnungenRepository.findByRessourceTypAndRessourceIdAndBenutzerIdAndDeaktiviert(
			ressourcenTypEntity, ressourcenId, benutzerId, isDeactivated);

		if (optionalZuordnungEntityList.isEmpty() || optionalZuordnungEntityList.get().isEmpty()) {
			log.debug("getRollenAusZuordnungOrNull(...) failed");
			return Collections.emptyList();
		}

		List<ZuordnungEntity> zuordnungEntities = optionalZuordnungEntityList.get();

		return zuordnungEntities.parallelStream().map(ZuordnungEntity::getRolle).collect(Collectors.toList());
	}

	// ^---------------------------------------------------------------------------------

	public UpdateResultType updateRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String neueRolle) {
		RessourceParamsDTO params = validateResourcePermission(benutzerId, ressourcenTyp, ressourceId, neueRolle);

		List<ZuordnungEntity> zuordnungEntities =
			zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRessourceId(
				params.getRessourceTypEntity(), benutzerId, ressourceId);

		if (zuordnungEntities.isEmpty()) {
			return UpdateResultType.NOT_FOUND;
		}

		final String ersteller = "ERSTELLER";
		ZuordnungEntity zuordnungEntity;
		if (zuordnungEntities.size() > 1) {
			//found multiple entries, will ignore ERSTELLER entry and entries with rolle=null are deprioritized
			List<ZuordnungEntity> zuordnungEntitiesOhneErsteller = zuordnungEntities.stream()
				.filter(e -> e.getRolle() == null || !StringUtils.equalsIgnoreCase(e.getRolle().getBezeichnung(), ersteller))
				.sorted(Comparator.comparing(o -> Optional.ofNullable(o.getRolle()).map(RolleEntity::getBezeichnung).orElse(null),
					Comparator.nullsLast(Comparator.naturalOrder())))
				.collect(Collectors.toList());
			if (zuordnungEntitiesOhneErsteller.size() > 1) {
				log.warn("found {} assigned roles for user with id {} on ressource with id {}: {}",
					zuordnungEntitiesOhneErsteller.size(), benutzerId, ressourceId,
					zuordnungEntitiesOhneErsteller.stream().map(ZuordnungEntity::getRolle).map(rolle ->
							Optional.ofNullable(rolle)
								.map(RolleEntity::getBezeichnung)
								.orElse(null))
						.collect(Collectors.toList()));
			}
			zuordnungEntity = IterableUtils.first(zuordnungEntitiesOhneErsteller);
		} else {
			zuordnungEntity = IterableUtils.first(zuordnungEntities);
		}

		RolleEntity alteRolleEntity = zuordnungEntity.getRolle();
		RolleEntity neueRolleEntity = params.getRolleEntity();
		if (Objects.equals(alteRolleEntity.getId(), neueRolleEntity.getId())) {
			//role is already assigned to this permission -> do nothing
			return UpdateResultType.NOT_CHANGED;
		} else {
			//update existing permission with another role, but first let's check if this action is permitted
			final List<String> notAllowedRoles = List.of(ersteller);
			checkIfRoleChangeIsAllowed(zuordnungEntity, neueRolleEntity, notAllowedRoles);
			zuordnungEntity.setRolle(neueRolleEntity);
			zuordnungenRepository.save(zuordnungEntity);
			return UpdateResultType.UPDATED;
		}
	}

	private void checkIfRoleChangeIsAllowed(ZuordnungEntity zuordnungEntity, RolleEntity neueRolle, List<String> notAllowedRoles) {
		RolleEntity alteRolle = zuordnungEntity.getRolle();
		notAllowedRoles.forEach(notAllowedRole -> {
			if (StringUtils.equalsIgnoreCase(alteRolle.getBezeichnung(), notAllowedRole)
				|| StringUtils.equalsIgnoreCase(neueRolle.getBezeichnung(), notAllowedRole)) {
				String message = MessageFormat.format("it''s not allowed to remove or set role {0} of an existing permission (zuordnung id {1})",
					notAllowedRole, zuordnungEntity.getId());
				throw new IllegalArgumentException(message);
			}
		});
	}

	// v---------------------------------------------------------------------------------

	/**
	 * Allows to add some new relations between new resources.
	 *
	 * @param benutzerId    The GID of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param rolle         The role of this permission
	 * @return If it was successfully or not
	 */
	public boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle) {
		return addRessourcePermission(benutzerId, ressourcenTyp, ressourceId, rolle, null);
	}

	/**
	 * Allows to add some new relations between new resources.
	 *
	 * @param benutzerId    The GID of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param rolle         The role of this permission
	 * @param befristung    The time limit of this permission
	 * @return If it was successfully or not
	 */
	public boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle, Instant befristung) {
		return addRessourcePermission(benutzerId, ressourcenTyp, ressourceId, rolle, befristung, null);
	}

	/**
	 * Allows to add some new relations between new resources.
	 *
	 * @param benutzerId    The GID of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param rolle         The role of this permission
	 * @param befristung    The time limit of this permission
	 * @param anmerkungen   The comments for this permission
	 * @return If it was successfully or not
	 */
	public boolean addRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle, Instant befristung, String anmerkungen) {
		Map<String, List<String>> details = addRessourcePermissionWithDetails(benutzerId, ressourcenTyp, ressourceId, rolle, befristung, anmerkungen);
		return !details.isEmpty();
	}

	/**
	 * Allows to add some new relations between new resources.
	 *
	 * @param benutzerId    The GID of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param rolle         The role of this permission
	 * @param befristung    The time limit of this permission
	 * @param anmerkungen   The comments for this permission
	 * @return A map with the done things
	 */
	public Map<String, List<String>> addRessourcePermissionWithDetails(String benutzerId, String ressourcenTyp, String ressourceId, String rolle,
		Instant befristung, String anmerkungen) {
		RessourceParamsDTO params = validateResourcePermission(benutzerId, ressourcenTyp, ressourceId, rolle);

		String relationsName = ressourcenTyp + " " + ressourceId + " " + UUID.randomUUID();
		log.debug("Die Relation sollte {} heißen.", relationsName);

		// Die mögliche neue Zuordnung
		ZuordnungEntity zuordnungEntity = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(ressourceId)
			.rolle(params.getRolleEntity())
			.ressourceTyp(params.getRessourceTypEntity())
			.bezeichnung(relationsName)
			.befristung(befristung)
			.anmerkungen(anmerkungen)
			.build();

		// Da Schreibrechte nur einmalig vorkommen können, bedarf es Sonderprüfungen
		if ("SCHREIBER".equals(rolle)) {
			// Finde die alten Zuordnungen
			// Die 'Ressource Id' ist eine UUID, so dass die Prüfung auf 'Ressource Typ' z.Z. nicht nötig ist.
			List<ZuordnungEntity> existingZuordnungEntities = getZuordnungEntities(ressourceId);
			if (existingZuordnungEntities.isEmpty()) {
				// eigentlich kann das nie zutreffen, da min. der Ersteller existiert
				fuegeInDieZuordnungEin(zuordnungEntity);
				zuordnungEntity.setBefristung(null);
				return Map.of(EGesetzRechteUtils.SCHREIBRECHTE_HINZU, List.of(zuordnungEntity.getBenutzerId()));
			} else {
				return fuegeVerteterInDieZuordnungEin(zuordnungEntity, befristung, existingZuordnungEntities);
			}
		} else {
			boolean success = fuegeInDieZuordnungEin(zuordnungEntity);
			if (success) {
				return Map.of(EGesetzRechteUtils.LESERECHTE_HINZU, List.of(zuordnungEntity.getBenutzerId()));
			}
			return Map.of();
		}
	}

	// ^---------------------------------------------------------------------------------

	/**
	 * Allows to add some new relations between new resources.
	 *
	 * @param rolle         The rolle of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param globalerolle  The role of this permission
	 * @return If it was successfully or not
	 */
	public boolean addRessourcePermissionGlobaleRolle(String rolle, String ressourcenTyp, String ressourceId, String globalerolle) {
		Optional<RolleEntity> rolleEntityOptional = getRolleEntity(rolle);
		Optional<RolleEntity> globalerolleEntityOptional = getRolleEntity(globalerolle);
		Optional<RessourceTypEntity> ressourceTypEntityOptional = getRessourceTypEntity(ressourcenTyp);

		if (rolleEntityOptional.isEmpty() || globalerolleEntityOptional.isEmpty() || ressourceTypEntityOptional.isEmpty() || StringUtils.isEmpty(ressourceId)) {
			return false;
		}
		Optional<ZuordnungGlobaleRolleEntity> exists = zuordnungenGlobalerollenRepository.findByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle(
			ressourceTypEntityOptional.get(),
			rolleEntityOptional.get().getId(), ressourceId, globalerolleEntityOptional.get());

		if (exists.isPresent()) {
			log.debug("Die ZuordnungGlobaleRolle '{}' existiert bereits", exists);
			return true;
		}
		ZuordnungGlobaleRolleEntity zuordnungGlobalerolleEntity = ZuordnungGlobaleRolleEntity.builder()
			.rolleId(rolleEntityOptional.get().getId())
			.ressourceId(ressourceId)
			.ressourceTyp(ressourceTypEntityOptional.get())
			.globaleRolle(globalerolleEntityOptional.get())
			.bezeichnung(ressourceTypEntityOptional.get().getBezeichnung() + " " + ressourceId)
			.build();

		log.debug("Gespeichert soll '{}' werden", zuordnungGlobalerolleEntity);
		try {
			ZuordnungGlobaleRolleEntity saved = zuordnungenGlobalerollenRepository.save(zuordnungGlobalerolleEntity);
			log.debug("Gespeichert: {}", saved);

			return true;
		} catch (Exception e) {
			log.debug("nicht gespeichert: {}", zuordnungGlobalerolleEntity);
		}
		return false;
	}

	/**
	 * @param rolle         The role of the user
	 * @param ressourcenTyp The type of resource, e.g. Dokumentenmappe
	 * @param ressourceId   The UUID of the resource
	 * @param globalerolle  The global role of this permission
	 * @return If it was successfully or not
	 */
	public boolean removeRessourcePermissionGlobaleRolle(String rolle, String ressourcenTyp, String ressourceId, String globalerolle) {
		Optional<RolleEntity> rolleEntityOptional = getRolleEntity(rolle);
		Optional<RolleEntity> globalerolleEntityOptional = getRolleEntity(globalerolle);
		Optional<RessourceTypEntity> ressourceTypEntityOptional = getRessourceTypEntity(ressourcenTyp);

		if (rolleEntityOptional.isEmpty() || globalerolleEntityOptional.isEmpty() || ressourceTypEntityOptional.isEmpty() || StringUtils.isEmpty(ressourceId)) {
			return false;
		}

		int result = zuordnungenGlobalerollenRepository.deleteByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle(ressourceTypEntityOptional.get(),
			rolleEntityOptional.get().getId(), ressourceId, globalerolleEntityOptional.get());

		return result > 0;

	}

	@Override
	public List<String> getAlleRessourcenFuerBenutzerUndRessourceTyp(String benutzerId, String ressourcenTyp) {
		AlleRessourcenFuerBenutzerUndRessourceTypParamsDTO params = validateAlleRessourcenFuerBenutzerUndRessourceTyp(benutzerId, ressourcenTyp);

		Optional<List<ZuordnungEntity>> zuordnungen = zuordnungenRepository.findByRessourceTypAndBenutzerId(params.getRessourceTypEntity(), benutzerId);

		if (zuordnungen.isEmpty()) {
			log.debug(EGesetzRechteUtils.KEINE_ZUORDNUNGEN_GEFUNDEN_FUER_BENUTZER_ID_UND_RESSOURCEN_TYP, benutzerId, ressourcenTyp);
			return Collections.emptyList();
		}

		List<ZuordnungEntity> zuordnungEntities = zuordnungen.get();
		List<String> ressourceIds = zuordnungEntities.parallelStream()
			.map(ZuordnungEntity::getRessourceId)
			.filter(Objects::nonNull)
			.distinct()
			.collect(Collectors.toList());
		log.debug(EGesetzRechteUtils.FOUND_RESSOURCE_IDS, ressourceIds);

		return ressourceIds;
	}

	@Override
	public List<PermissionDTO> getAlleRessourcenFuerBenutzerUndRessourceTypAndAktionen(String benutzerId, String ressourcenTyp, Collection<String> aktionen) {
		return getPermissions(
			Optional.of(Stream.of(benutzerId).collect(Collectors.toList())),
			Optional.of(aktionen),
			Optional.of(Stream.of(ressourcenTyp).collect(Collectors.toList())),
			Optional.empty(),
			Optional.empty(),
			GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN,
			Optional.empty()
		).stream()
			.filter(p -> p.getRessourceId() != null)
			.collect(Collectors.toList());
	}

	@Override
	public List<String> getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(String benutzerId, String rolle, String ressourcenTyp) {
		return getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(benutzerId, rolle, ressourcenTyp, false);
	}

	@Override
	public List<String> getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(String benutzerId, String rolle, String ressourcenTyp, boolean ohneDeaktivierte) {
		Optional<RessourceTypEntity> resourceTypEntityOptional = getRessourceTypEntity(ressourcenTyp);

		Optional<RolleEntity> rolleEntityOptional = rollenRepository.findByBezeichnung(rolle);

		if (rolleEntityOptional.isEmpty() || resourceTypEntityOptional.isEmpty()) {
			return Collections.emptyList();
		}

		RessourceTypEntity resourceTypEntity = resourceTypEntityOptional.get();
		RolleEntity rolleEntity = rolleEntityOptional.get();

		Optional<List<ZuordnungEntity>> zuordnungen;
		if (ohneDeaktivierte) {
			// ohne deaktivierte
			zuordnungen = zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRolleAndDeaktiviert(resourceTypEntity, benutzerId, rolleEntity, false);
		} else {
			// alle -- nicht nur aktivierte
			zuordnungen = zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRolle(resourceTypEntity, benutzerId, rolleEntity);
		}

		if (zuordnungen.isEmpty()) {
			log.debug(EGesetzRechteUtils.KEINE_ZUORDNUNGEN_GEFUNDEN_FUER_BENUTZER_ID_UND_RESSOURCEN_TYP, benutzerId, ressourcenTyp);
			return Collections.emptyList();
		}

		List<ZuordnungEntity> zuordnungEntities = zuordnungen.get();
		List<String> rssourceIds = zuordnungEntities.parallelStream()
			.map(ZuordnungEntity::getRessourceId)
			.filter(Objects::nonNull)
			.collect(Collectors.toList());
		log.debug(EGesetzRechteUtils.FOUND_RESSOURCE_IDS, rssourceIds);
		return rssourceIds;
	}

	@Override
	public Set<String> getAlleBenutzerIdsByRessourceIdAndAktion(String ressourcenId, String aktion) {

		AktionEntity aktionEntity = getAktionEntity(aktion).orElse(null);

		List<ZuordnungEntity> zuordnungEntityList = getZuordnungEntities(ressourcenId);

		if (zuordnungEntityList.isEmpty()) {
			return Collections.emptySet();
		}

		//beliebiges element der liste für ressourcetyp nehmen (sollten alle gleich sein)
		RessourceTypEntity ressourceTypEntity = zuordnungEntityList.get(0).getRessourceTyp();

		List<RegelEntity> regelEntityList = getRegelEntities(ressourcenId, aktion, aktionEntity, ressourceTypEntity);

		Set<RolleEntity> rollenSet = regelEntityList.stream().map(RegelEntity::getRolle).collect(Collectors.toSet());

		//bisherige liste filtern, anstatt neu abzufragen
		return zuordnungEntityList.stream()
			.filter(zuordnungEntity -> !zuordnungEntity.isDeaktiviert())
			.filter(x -> rollenSet.contains(x.getRolle()))
			.map(ZuordnungEntity::getBenutzerId)
			.collect(Collectors.toSet());
	}

	@Override
	public List<PermissionDTO> getPermissions(
		Optional<Collection<String>> benutzerIds,
		Optional<Collection<String>> aktionen,
		Optional<Collection<String>> ressourceTypen,
		Optional<Collection<String>> ressourcenIds,
		Optional<Collection<String>> rollen,
		GetPermissionOptionType optionType) {
		return getPermissions(benutzerIds, aktionen, ressourceTypen, ressourcenIds, rollen, optionType, Optional.empty());
	}

	public HashMap<String, Set<String>> getAlleDokumenteWithPermissionsbyBenutzerId(String benutzerId,
		String ressourcenTyp) {
		Optional<RessourceTypEntity> resourceTypEntityOptinal =
			getRessourceTypEntity(ressourcenTyp);

		if (resourceTypEntityOptinal.isEmpty()) {
			return new HashMap<>();
		}

		RessourceTypEntity resourceTypEntity = resourceTypEntityOptinal.get();

		Optional<List<ZuordnungEntity>> zuordnungen =
			zuordnungenRepository.findByRessourceTypAndBenutzerId(resourceTypEntity, benutzerId);

		if (zuordnungen.isEmpty()) {
			log.debug("KEINE_ZUORDNUNGEN_GEFUNDEN_FÜR_BENUTZER_ID_UND_RESSOURCEN_TYP", benutzerId, ressourcenTyp);
			return new HashMap<>();
		}

		return getDocumentIdWithPermissionsMapping(resourceTypEntity, zuordnungen.get());
	}

	private List<ZuordnungAktionenDTO> filterByStatusAndAktionen(List<ZuordnungEntity> zuordnungen, Optional<Collection<AktionEntity>> aktionen) {
		String dontCare = "DONT CARE";
		StatusEntity dontCareStatus = statusRepository.findByBezeichnung(dontCare).orElseThrow(() ->
			new IllegalArgumentException(EGesetzRechteUtils.createNotFoundErrorMsg("Status", dontCare)));

		List<StatusResponseDTO> statusResponses = getStatusResponsesFromZuordnungen(zuordnungen);

		return zuordnungen.stream()
			.map(zuordnung -> {
				Optional<StatusResponseDTO> statusResponse = EGesetzRechteUtils.pickStatusResponse(zuordnung, statusResponses);
				if (statusResponse.isEmpty()) {
					return ZuordnungAktionenDTO.builder()
						.zuordnung(zuordnung)
						.aktionen(null)
						.build();
				}
				String status = statusResponse.get().getStatus();

				StatusEntity statusEntity;
				if (StringUtils.isBlank(status)) {
					statusEntity = null;
				} else {
					Optional<StatusEntity> statusEntityOptional = statusRepository.findByBezeichnung(status);
					if (statusEntityOptional.isPresent()) {
						statusEntity = statusEntityOptional.get();
					} else {
						throw new IllegalArgumentException(EGesetzRechteUtils.createNotFoundErrorMsg("Status", status));
					}
				}

				List<StatusEntity> statusEntities = Stream.of(dontCareStatus, statusEntity)
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
				List<AktionEntity> aktionenEntities = getAktionenAusRegeln(zuordnung.getRessourceTyp(),
					List.of(zuordnung.getRolle()), Optional.of(statusEntities), aktionen);
				return ZuordnungAktionenDTO.builder()
					.zuordnung(zuordnung)
					.aktionen(aktionenEntities)
					.build();
			}).filter(e -> CollectionUtils.isNotEmpty(e.getAktionen()))
			.collect(Collectors.toList());
	}

	private List<StatusResponseDTO> getStatusResponsesFromZuordnungen(List<ZuordnungEntity> zuordnungen) {
		Map<RessourceTypEntity, Set<String>> statusRequests = new HashMap<>();
		zuordnungen.forEach(zuordnung -> {
			Optional<RessourceTypEntity> ressourceTyp = Optional.ofNullable(zuordnung.getRessourceTyp());
			if (ressourceTyp.isEmpty()) {
				log.warn("ressource type of Zuordnung with id {} is null!", zuordnung.getId());
				return;
			}
			if (StringUtils.isBlank(zuordnung.getRessourceId())) {
				log.warn("ressource id of Zuordnung with id {} is empty!", zuordnung.getId());
				return;
			}
			Set<String> ressourcenIds = statusRequests.computeIfAbsent(ressourceTyp.get(), key -> new HashSet<>());
			ressourcenIds.add(zuordnung.getRessourceId());
		});
		List<StatusResponseDTO> result = new ArrayList<>();
		statusRequests.forEach((ressourceTyp, ressourcenIds) -> result.addAll(getStatusForResourceType(ressourceTyp.getBezeichnung(), ressourcenIds)));
		return result;
	}

	@Override
	public List<String> getAlleBenutzerIdsByRessourceIdAndRolle(String ressourcenId, String rolle) {
		Optional<RolleEntity> rolleEntity = rollenRepository.findByBezeichnung(rolle);

		if (rolleEntity.isPresent()) {
			Optional<List<ZuordnungEntity>> zuordnungen = zuordnungenRepository.findByRessourceIdAndRolle(ressourcenId, rolleEntity.get());

			if (zuordnungen.isPresent()) {
				List<ZuordnungEntity> zuordnungEntities = zuordnungen.get();
				return zuordnungEntities.stream().map(ZuordnungEntity::getBenutzerId).collect(Collectors.toList());
			} else {
				log.debug("Keine Benutzer gefunden für rolle {} und ressourceId {}", rolle, ressourcenId);
			}

		} else {
			log.debug("Rolle {} existiert nicht", rolle);
		}

		return Collections.emptyList();
	}

	@Override
	public boolean removeRessourcePermission(String benutzerId, String ressourcenTyp, String ressourceId, String rolle) {
		RessourceParamsDTO params = validateResourcePermission(benutzerId, ressourcenTyp, ressourceId, rolle);
		int result = zuordnungenRepository.deleteByRessourceTypAndBenutzerIdAndRessourceIdAndRolle(
			params.getRessourceTypEntity(), benutzerId, ressourceId, params.getRolleEntity()
		);
		if (result > 1) {
			log.warn("suspicious result. Deleted {} permission entries with these values: benutzerId={}, ressourcenTyp={}, ressourceId={}, rolle={}",
				result, benutzerId, ressourcenTyp, ressourceId, rolle);
		}
		return result > 0;
	}

	@Override
	public boolean removeRessourcePermission(String ressourcenTyp, String ressourceId, String rolle) {
		Optional<RessourceTypEntity> ressourceTypEntity = ressourceTypenRepository.findByBezeichnung(ressourcenTyp);
		Optional<RolleEntity> rolleEntity = rollenRepository.findByBezeichnung(rolle);

		if (ressourceTypEntity.isPresent() && rolleEntity.isPresent()) {
			zuordnungenRepository.deleteByRessourceIdAndRessourceTypAndRolle(ressourceId, ressourceTypEntity.get(),
				rolleEntity.get());
			return true;
		}

		return false;
	}

	/**
	 * Get the ids from alle resource types for the given user and the expected role. A special version is #getAlleDokumenteMitRolle(String, String).
	 *
	 * @param benutzerId    The id of the user
	 * @param ressourcenTyp A ressource type, e.g. Dokument
	 * @param rolle         The role the user should own.
	 * @return A list of dokument ids.
	 */
	public List<String> getAlleRessourceTypenIds(String benutzerId, String ressourcenTyp, String rolle) {

		Optional<List<ZuordnungEntity>> zuordnungEntitiesOptional = zuordnungenRepository.findByRessourceTypAndBenutzerId(
			getRessourceTypEntity(ressourcenTyp).orElse(null), benutzerId);

		log.debug("Zuordnungen gefunden?: {}", zuordnungEntitiesOptional.isPresent());  // NOSONAR
		if (zuordnungEntitiesOptional.isPresent()) {
			List<ZuordnungEntity> zuordnungEntities = zuordnungEntitiesOptional.get();
			log.debug("Zuordnungen: {}", zuordnungEntities);

			return zuordnungEntities.parallelStream()
				.filter(z -> z.getRolle()
					.getBezeichnung()
					.equals(rolle))
				.map(ZuordnungEntity::getRessourceId)
				.collect(Collectors.toList());
		}

		log.debug("Leere Liste, garantiert nicht so gewollt.");
		// return-Werte als UUID wären besser
		return Collections.emptyList();
	}

	private void reaktivierungAlterSchreiber(final Instant befristung, final ZuordnungEntity vorgaenger, final ZuordnungEntity neuerEhemaligerSchreiber) {
		// EIN 'alter' Schreiber wird reaktiviert
		neuerEhemaligerSchreiber.setDeaktiviert(false);

		// Berücksichtige Befristung
		final boolean isErsteller = Objects.equals(neuerEhemaligerSchreiber.getRolle().getBezeichnung(), "ERSTELLER");
		if (befristung != null && !isErsteller) {
			neuerEhemaligerSchreiber.setBefristung(getValideBefristung(vorgaenger, befristung));
		}

		zuordnungenRepository.save(neuerEhemaligerSchreiber);
	}

	private Map<String, List<String>> einfachesAnfuegenVonSchreibrechten(ZuordnungEntity neuerNutzerFuerSchreibrechte, Instant befristung,
		ZuordnungEntity letzterNutzerMitSchreibrechten) {

		Map<String, List<String>> durchgefuehrteAenderungen = new HashMap<>();

		// Deaktiviere die momentan aktive Zuordnung
		letzterNutzerMitSchreibrechten.setDeaktiviert(true);
		zuordnungenRepository.save(letzterNutzerMitSchreibrechten);
		durchgefuehrteAenderungen.put(EGesetzRechteUtils.SCHREIBRECHTE_ENTFERNT, List.of(letzterNutzerMitSchreibrechten.getBenutzerId()));

		// Erstelle Link zwischen alter und neuer Zuordnung
		neuerNutzerFuerSchreibrechte.setFallbackZuordnung(letzterNutzerMitSchreibrechten.getId());
		// Berücksichtige Befristung
		if (befristung != null) {
			neuerNutzerFuerSchreibrechte.setBefristung(getValideBefristung(letzterNutzerMitSchreibrechten, befristung));
		}

		// Füge den NEUEN Schreiber hinzu
		fuegeInDieZuordnungEin(neuerNutzerFuerSchreibrechte);
		durchgefuehrteAenderungen.put(EGesetzRechteUtils.SCHREIBRECHTE_HINZU, List.of(neuerNutzerFuerSchreibrechte.getBenutzerId()));
		setLeserechte(letzterNutzerMitSchreibrechten);
		durchgefuehrteAenderungen.put(EGesetzRechteUtils.LESERECHTE_HINZU, List.of(letzterNutzerMitSchreibrechten.getBenutzerId()));

		return durchgefuehrteAenderungen;
	}

	private void setLeserechte(ZuordnungEntity letzterNutzerMitSchreibrechten) {
		Optional<RolleEntity> leser = getRolleEntity("LESER");
		if (leser.isEmpty()) {
			log.error("Die 'LESER'-Rolle fehlt in der Datenbank.");
			return;
		}

		Optional<ZuordnungEntity> leseBerechtigung = zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRessourceIdAndRolle(
			letzterNutzerMitSchreibrechten.getRessourceTyp(), letzterNutzerMitSchreibrechten.getBenutzerId(), letzterNutzerMitSchreibrechten.getRessourceId(),
			leser.get());
		if (leseBerechtigung.isPresent()) {
			log.debug("Der Nutzer verfügt bereits über Leserechte.");
			return;
		}

		ZuordnungEntity leserechte = ZuordnungEntity.builder()
			.fallbackZuordnung(null)
			.deaktiviert(false)
			.befristung(null)
			.rolle(leser.get())
			.bezeichnung("Leserechte " + letzterNutzerMitSchreibrechten.getRessourceId())
			.benutzerId(letzterNutzerMitSchreibrechten.getBenutzerId())
			.ressourceId(letzterNutzerMitSchreibrechten.getRessourceId())
			.ressourceTyp(letzterNutzerMitSchreibrechten.getRessourceTyp())
			.build();
		zuordnungenRepository.save(leserechte);
	}

	private Instant getValideBefristung(ZuordnungEntity vorgaenger, Instant befristung) {
		Instant alteFrist = vorgaenger.getBefristung();
		if (alteFrist == null) {
			return befristung;
		}

		if (alteFrist.isBefore(befristung)) {
			return alteFrist;
		}

		return befristung;
	}

	// v---------------------------------------------------------------------------------

	/**
	 * Initially loads all resource types, if it's list is empty. Also load could be forced, if some changes occurred.
	 *
	 * @param force Force to (re)load the list.
	 */
	@SuppressWarnings("SameParameterValue")
	protected void loadAllRessourceTypes(boolean force) {
		if (alleRessourceTypen.isEmpty() || force) {
			alleRessourceTypen = ressourceTypenRepository.findAll();
			log.debug("Ressource geladen: {}", alleRessourceTypen);
		}
	}

	/**
	 * Initially loads all rollen, if it's list is empty. Also load could be forced, if some changes occurred.
	 *
	 * @param force Force to (re)load the list.
	 */
	@SuppressWarnings("SameParameterValue")
	protected void loadAllRoles(boolean force) {
		if (alleRollen.isEmpty() || force) {
			alleRollen = rollenRepository.findAll();
			log.debug("Rollen geladen: {}", alleRollen);
		}
	}

	/**
	 * Get the 'aktionEntity' from its string representation.
	 *
	 * @param aktionRepresentation Aktion as string
	 * @return Aktion as entity
	 */
	private Optional<AktionEntity> getAktionEntity(String aktionRepresentation) {

		Optional<AktionEntity> optionalAktionEntity = aktionenRepository.findByBezeichnung(aktionRepresentation);

		log.debug("Aktionen: {}", optionalAktionEntity);
		return optionalAktionEntity;
	}

	/**
	 * Get the 'rolleEntity' from its string representation.
	 *
	 * @param rolleRepresentation Rolle as string
	 * @return Rolle as Entity
	 */
	private Optional<RolleEntity> getRolleEntity(String rolleRepresentation) {
		// We try to load rollen only one time
		loadAllRoles(false);

		Optional<RolleEntity> rollenEntityOptional = alleRollen.parallelStream().filter(rt -> rt.getBezeichnung().equals(rolleRepresentation)).findFirst();

		log.debug("Rollen: {}", rollenEntityOptional);
		return rollenEntityOptional;
	}

	/**
	 * Get the 'ressourceTypEntity' from its string representation.
	 *
	 * @param ressourcenTypRepresentation RessourceTyp as string
	 * @return RessourceTyp as Entity
	 */
	private Optional<RessourceTypEntity> getRessourceTypEntity(String ressourcenTypRepresentation) {
		// we try to load only one time
		loadAllRessourceTypes(false);

		Optional<RessourceTypEntity> ressourceTypEntity = ressourceTypenRepository.findByBezeichnung(ressourcenTypRepresentation);

		log.debug("Ressourcetypen: {}", ressourceTypEntity);
		return ressourceTypEntity;
	}

	/**
	 * Get the 'zuordnungEntity' from its id representation.
	 *
	 * @param ressourcenId Die Id der ressource
	 * @return Die Zuordnung als Entität
	 */
	private List<ZuordnungEntity> getZuordnungEntities(String ressourcenId) {
		List<ZuordnungEntity> zuordnungEntityList = zuordnungenRepository.findByRessourceIdOrderById(ressourcenId);

		log.debug("Zuordnungen: {}", zuordnungEntityList);
		return zuordnungEntityList;
	}

	// ^---------------------------------------------------------------------------------

	private List<AktionEntity> getAktionenAusRegel(RessourceTypEntity ressourceTyp, RolleEntity rolle, Collection<StatusEntity> status) {

		Optional<List<RegelEntity>> optionalRegelEntityList = regelnRepository.findByRessourceTypAndRolleAndStatusIn(
			ressourceTyp, rolle, status);

		if (optionalRegelEntityList.isEmpty()) {
			log.debug("getAktionenAusRegel(...) failed");
			return Collections.emptyList();
		}

		List<RegelEntity> regelEntities = optionalRegelEntityList.get();

		return regelEntities.parallelStream()
			.map(RegelEntity::getAktion)
			.collect(Collectors.toList());
	}

	/***
	 * USES getAktionenAusRegel(RessourceTypEntity ressourceTyp, RolleEntity rolle, StatusEntity status)
	 */
	private List<AktionEntity> getAktionenAusRegeln(RessourceTypEntity ressourceTyp, List<RolleEntity> rollen, List<StatusEntity> status) {

		List<AktionEntity> mergedList = new ArrayList<>();

		for (RolleEntity rolleEntity : rollen) {
			List<AktionEntity> actualList = getAktionenAusRegel(ressourceTyp, rolleEntity, status);

			mergedList.addAll(
				actualList.stream()
					.filter(element -> !mergedList.contains(element))
					.collect(Collectors.toList())
			);
		}

		if (!mergedList.isEmpty()) {
			return mergedList;
		}

		log.debug("getAktionenAusRegeln(...) failed.");
		return Collections.emptyList();
	}

	/***
	 * reworked and performance optimized version of {@link AbstractEGesetzRechte#getAktionenAusRegeln(RessourceTypEntity, List, List)}
	 *
	 * @param ressourceTyp ressource type
	 * @param rollen list of roles
	 * @param status list of status
	 * @param aktionen list of actions
	 * @return list of matching actions
	 */
	private List<AktionEntity> getAktionenAusRegeln(RessourceTypEntity ressourceTyp, List<RolleEntity> rollen,
		Optional<Collection<StatusEntity>> status, Optional<Collection<AktionEntity>> aktionen) {

		if (ressourceTyp == null || CollectionUtils.isEmpty(rollen)
			|| (status.isPresent() && CollectionUtils.isEmpty(status.get()))) {
			return Collections.emptyList();
		}
		Specification<AktionEntity> spec = aktionenRepository.createSpecification(ressourceTyp, rollen, status, aktionen);
		return aktionenRepository.findAll(spec);
	}

	private StatusEntity getStatusEntityForResourceType(String ressourcenTyp, String ressourcenId) {
		// Some RessourceType may not own a status
		String status = getStatusForResourceType(ressourcenTyp, ressourcenId);
		if (StringUtils.isNotBlank(status)) {
			Optional<StatusEntity> statusEntity = statusRepository.findByBezeichnung(status);
			if (statusEntity.isPresent()) {
				return statusEntity.get();
			} else {
				throw new IllegalArgumentException(EGesetzRechteUtils.createNotFoundErrorMsg("Status", status));
			}
		} else {
			log.warn("returned status value for ressourcenTyp={} and ressourcenId={} is null or empty!", ressourcenTyp, ressourcenId);
		}
		return null;
	}

	private List<RegelEntity> getRegelEntities(String ressourcenId, String aktion, AktionEntity aktionEntity,
		RessourceTypEntity ressourceTypEntity) {
		Optional<List<RegelEntity>> optionalRegelEntityList =
			regelnRepository.findByRessourceTypAndAktion(ressourceTypEntity, aktionEntity);
		if (optionalRegelEntityList.isEmpty()) {
			log.warn("Keine Regeln gefunden für aktion {} und ressourceId {}", aktion, ressourcenId);
			throw new IllegalArgumentException(
				"Keine Regeln gefunden für aktion " + aktion + " und ressourceId " + ressourcenId);
		}
		return optionalRegelEntityList.get();
	}

	private HashMap<String, Set<String>> getDocumentIdWithPermissionsMapping(RessourceTypEntity resourceTypEntity,
		List<ZuordnungEntity> zuordnungen) {
		HashMap<String, Set<String>> ret = new HashMap<>();
		Optional<List<RegelEntity>> alleRegeln = regelnRepository.findByRessourceTyp(resourceTypEntity);
		if (alleRegeln.isPresent()) {
			for (ZuordnungEntity zuordnung : zuordnungen) {
				Set<String> aktionen = alleRegeln.get()
					.stream()
					.filter(r -> r.getRolle()
						.equals(zuordnung.getRolle()))
					.map(r -> r.getAktion()
						.getBezeichnung())
					.collect(
						Collectors.toSet());

				if (!ret.containsKey(zuordnung.getRessourceId())) {
					ret.put(zuordnung.getRessourceId(), aktionen);
				} else {
					Set<String> set = ret.get(zuordnung.getRessourceId());
					set.addAll(aktionen);
				}

			}
		}
		return ret;
	}

	// v============ Use Case: (befristete) Schreibrechtevergabe ===================

	/**
	 * Wenn Befristungen abgelaufen sind, werden die Rechte wieder zurückgegeben. Zusätzlich erhält derjenige aber Leserechte, der vorher die Schreibrechte
	 * hatte.
	 */
	public void clearInvalidPermissionsByPreviousSetLimitation(String ressourcenId) {
		List<ZuordnungEntity> zuordnungEntities = zuordnungenRepository.findByRessourceIdOrderById(ressourcenId);

		// Die Id einer Ressource ist einmalig, Befristungen können also nur existieren, falls es mehr als einen Treffer gibt
		if (zuordnungEntities.size() > 1) {

			// Wir trennen jetzt nach Ersteller, Leserechten und Schreibrechten auf (mit Leserechten müssen wir z.Z. nichts machen)
			ZuordnungEntity ersteller = zuordnungEntities.get(0);
			List<ZuordnungEntity> schreiber = zuordnungEntities.stream()
				.filter(zuordnungEntity -> zuordnungEntity.getRolle().getBezeichnung().equals("SCHREIBER")).collect(Collectors.toList());

			// Wenn die Schreibrechte mehrfach vergeben sind
			if (!schreiber.isEmpty()) {

				// Der letzte Schreiber muss seine Rechte an den Vorgänger zurückgeben
				for (int i = schreiber.size() - 1; i > 0; i--) {
					ZuordnungEntity aktuell = schreiber.get(i);
					ZuordnungEntity vorgaenger = schreiber.get(i - 1);
					setZuordnungsEntitesForInvalidTimeValues(aktuell, vorgaenger);
				}

				// Der 1. Schreiber muss die Rechte an den Ersteller zurückgeben
				ZuordnungEntity firstWriter = schreiber.get(0);
				setZuordnungsEntitesForInvalidTimeValues(firstWriter, ersteller);

			}

		}
	}

	private void setZuordnungsEntitesForInvalidTimeValues(ZuordnungEntity aktuell, ZuordnungEntity vorgaenger) {
		Instant befristung = aktuell.getBefristung();

		Optional<RolleEntity> leser = getRolleEntity("LESER");
		if (leser.isEmpty()) {
			log.error("Die 'LESER'-Rolle fehlt in der Datenbank.");
			return;
		}

		if (befristung != null && befristung.isBefore(Instant.now())) {
			// setze aktuellen auf Leserechte, entferne Frist und Referenz
			aktuell.setDeaktiviert(false);
			aktuell.setRolle(leser.get());
			aktuell.setBefristung(null);
			aktuell.setFallbackZuordnung(null);
			aktuell.setAnmerkungen("auf Leserechte zurückgestuft");
			zuordnungenRepository.save(aktuell);

			// aktiviere Vorgänger
			vorgaenger.setDeaktiviert(false);
			zuordnungenRepository.save(vorgaenger);
		}
	}

	private Map<String, List<String>> fuegeVerteterInDieZuordnungEin(ZuordnungEntity zuordnungEntity, Instant befristung,
		List<ZuordnungEntity> existingZuordnungEntities) {
		// Context: - die Anzahl der existierenden Zuordnungen muss min. 1 betragen, da der Ersteller mit enthalten ist

		// Vorbereitung: Den Ersteller und alle Personen mit (ehemals) Schreibrechten und Leserechten herausfinden
		final List<ZuordnungEntity> erstellerUndAlleEhemaligenSchreiber = EGesetzRechteUtils.filterErstellerAndSchreiber(existingZuordnungEntities);
		final List<String> alleLeser = EGesetzRechteUtils.filterLeser(existingZuordnungEntities);

		// Dem Ersteller sollte man keine Frist geben
		if (erstellerUndAlleEhemaligenSchreiber.size() == 1) {
			befristung = null;
		}

		// Enthält die Liste der Schreiber bereits den NEUEN Schreiber ...
		final Optional<ZuordnungEntity> enthalten = erstellerUndAlleEhemaligenSchreiber.stream()
			.filter(z -> z.getBenutzerId().equals(zuordnungEntity.getBenutzerId()))
			.findFirst();

		// ... dann müssen alle Nachfolger in der Liste zum Löschen vorgemerkt werden
		final List<ZuordnungEntity> loeschKandidaten = enthalten
			.map(entity -> erstellerUndAlleEhemaligenSchreiber.stream()
				.skip(erstellerUndAlleEhemaligenSchreiber.lastIndexOf(entity) + 1L)
				.collect(Collectors.toList()))
			.orElseGet(Collections::emptyList);

		// Finde denjenigen, der gerade die Schreibrechte hat
		final ZuordnungEntity letzterAktiverVertreter = erstellerUndAlleEhemaligenSchreiber.get(erstellerUndAlleEhemaligenSchreiber.size() - 1);

		if (enthalten.isEmpty()) {
			// der NEUE ist nicht schon einmal SCHREIBER gewesen

			return einfachesAnfuegenVonSchreibrechten(zuordnungEntity, befristung, letzterAktiverVertreter);
		} else {
			// Der NEUE hatte bereits einmal SCHREIBRECHTE

			final Map<String, List<String>> durchgefuehrteAenderungen = new HashMap<>();

			final List<String> leserechteGewinner = new ArrayList<>();
			int listenpositionDesAltenSchreibers = gewaehreLeserechteFuerEhemaligeSchreibrechte(
				existingZuordnungEntities, enthalten.get(), alleLeser, leserechteGewinner);
			durchgefuehrteAenderungen.put(EGesetzRechteUtils.LESERECHTE_HINZU, leserechteGewinner);

			final ZuordnungEntity vorgaenger = existingZuordnungEntities.get(listenpositionDesAltenSchreibers);
			reaktivierungAlterSchreiber(befristung, vorgaenger, enthalten.get());
			durchgefuehrteAenderungen.put(EGesetzRechteUtils.SCHREIBRECHTE_HINZU, List.of(enthalten.get().getBenutzerId()));

			// Aufräumen - Alle gemerkten Schreibrechte 'nach' dem NEUEN Schreiber löschen
			loeschKandidaten.forEach(zuordnungenRepository::delete);
			durchgefuehrteAenderungen.put(
				EGesetzRechteUtils.SCHREIBRECHTE_ENTFERNT,
				loeschKandidaten.stream().map(ZuordnungEntity::getBenutzerId).collect(Collectors.toList()));

			return durchgefuehrteAenderungen;
		}
	}

	private boolean fuegeInDieZuordnungEin(ZuordnungEntity zuordnungEntity) {
		log.debug("Gespeichert soll '{}' werden", zuordnungEntity);

		try {
			ZuordnungEntity saved = zuordnungenRepository.save(zuordnungEntity);
			log.debug("Gespeichert: {}", saved);

			return true;
		} catch (Exception e) {
			//idea: don't throw an exception when we try to add an already existing permission (normally we don't care, right?)
			//solution with possible
			// but it is fast and reliable (on MySQL), even under conditions with concurrent calls

			if (e instanceof DataIntegrityViolationException) {
				Throwable cause = EGesetzRechteUtils.getRootCause(e);
				String exceptionMessage = cause.getMessage();
				String logMessage = "";

				if (exceptionMessage != null) {
					logMessage = MessageFormat.format(
						"Diese Berechtigung existiert bereits: benutzerId={0}, ressourcenTyp={1}, ressourceId={2}, rolle={3}",
						zuordnungEntity.getBenutzerId(),
						Optional.ofNullable(zuordnungEntity.getRessourceTyp()).map(RessourceTypEntity::getBezeichnung).orElse(null),
						zuordnungEntity.getRessourceId(),
						Optional.ofNullable(zuordnungEntity.getRolle()).map(RolleEntity::getBezeichnung).orElse(null)
					);
				}

				if (cause instanceof java.sql.SQLIntegrityConstraintViolationException) {

					if (StringUtils.containsIgnoreCase(exceptionMessage, "Duplicate entry") && StringUtils.containsIgnoreCase(exceptionMessage,
						"for key 'zuordnungen.UK_zuordnungen_eintraege'")) {
						log.debug(logMessage);
						log.trace((String) null, e);
					}

				} else {
					log.debug("Datenintegrität verletzt: {}", logMessage);
				}

				return false;
			}

			throw e;

		}

	}

	private int gewaehreLeserechteFuerEhemaligeSchreibrechte(
		List<ZuordnungEntity> existingZuordnungEntities, ZuordnungEntity ehemaligerSchreiber, List<String> alleLeser, List<String> leserechteGewinner) {

		// bei denen, die Schreibrechte verloren haben, werden Leserechte eingeräumt
		int positionInListe = existingZuordnungEntities.indexOf(ehemaligerSchreiber);
		for (int i = existingZuordnungEntities.size() - 1; i > positionInListe; i--) {
			ZuordnungEntity zuordnungEntity1 = existingZuordnungEntities.get(i);

			if (!alleLeser.contains(zuordnungEntity1.getBenutzerId()) && "SCHREIBER".equals(zuordnungEntity1.getRolle().getBezeichnung())) {

				Optional<RolleEntity> leser = getRolleEntity("LESER");
				if (leser.isEmpty()) {
					log.error("Die Rolle 'LESER' fehlt in der Datenbank.");
					return 0;
				}

				zuordnungEntity1.setFallbackZuordnung(null);
				zuordnungEntity1.setDeaktiviert(false);
				zuordnungEntity1.setBefristung(null);
				zuordnungEntity1.setRolle(leser.get());

				try {
					zuordnungenRepository.save(zuordnungEntity1);
				} catch (Exception e) {
					log.debug("Duplicate Entry");
				}

				leserechteGewinner.add(zuordnungEntity1.getBenutzerId());
			}


		}
		return positionInListe;
	}

	@Override
	public String getRessortKurzbezeichnungForNutzer(String benutzerId) {
		Optional<NutzerEntity> optionalNutzerEntity = nutzerEntityRepository.findByGid(benutzerId);
		if (optionalNutzerEntity.isEmpty()) {
			return null;
		}
		return optionalNutzerEntity.get().getRessortKurzbezeichnung();
	}

	@Override
	public boolean isAllowedAdministration(String benutzerGid, String aktion, String angefragteRolle) {
		Optional<NutzerEntity> optionalNutzerEntity = nutzerEntityRepository.findByGid(benutzerGid);
		if (optionalNutzerEntity.isEmpty()) {
			return false;
		}
		if (null == optionalNutzerEntity.get().getRolle()) {
			return false;
		}
		RolleEntity rolleEntity = optionalNutzerEntity.get().getRolle();
		Optional<AktionEntity> optionalAktionEntity = aktionenRepository.findByBezeichnung(aktion);
		if (optionalAktionEntity.isEmpty()) {
			return false;
		}
		Optional<RolleEntity> optionalRolleEntity = rollenRepository.findByBezeichnung(angefragteRolle);
		if (optionalRolleEntity.isEmpty()) {
			return false;
		}
		Optional<RegelAdministrationEntity> regelAdministrationEntity = regelnAdministrationRepository.findByRolleAndAktionAndAngefragteRolleId(
			rolleEntity, optionalAktionEntity.get(), optionalRolleEntity.get().getId());
		if (regelAdministrationEntity.isPresent()) {
			return true;
		}
		return false;
	}
}

