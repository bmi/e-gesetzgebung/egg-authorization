// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.mapper;

import de.itzbund.egesetz.bmi.user.dto.NutzerEntityDTO;
import de.itzbund.egesetz.bmi.user.dto.NutzerEntitySaveDTO;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {NutzerMapperImpl.class})
class NutzerMapperTest {

	@Autowired
	NutzerMapper mapper;

	private static NutzerEntity createEntity() {
		return NutzerEntity.builder()
			.name("Name")
			.gid("GID")
			.abteilung("Abteilung")
			.referat("Referat")
			.ressortId(99L)
			.email("E-Mail")
			.ressortKurzbezeichnung("Ressort")
			.telefon("Telefon")
			.anrede("Anrede")
			.titel("Titel")
			.erstelltAm(Instant.now().minus(1, DAYS))
			.bearbeitetAm(Instant.now().minus(2, DAYS))
			.lastModified(Instant.now().minus(3, DAYS))
			.deleted(true)
			.aktiv(true)
			.build();
	}

	private static NutzerEntityDTO createDto() {
		return NutzerEntityDTO.builder()
			.name("Name")
			.gid("GID")
			.abteilung("Abteilung")
			.referat("Referat")
			.ressortId(99L)
			.email("E-Mail")
			.ressortKurzbezeichnung("Ressort")
			.telefon("Telefon")
			.anrede("Anrede")
			.titel("Titel")
			.deleted(true)
			.aktiv(true)
			.build();
	}

	private static NutzerEntitySaveDTO createSaveDto() {
		return NutzerEntitySaveDTO.builder()
			.name("Name")
			.gid("GID")
			.abteilung("Abteilung")
			.referat("Referat")
			.ressortId(99L)
			.email("E-Mail")
			.ressortKurzbezeichnung("Ressort")
			.telefon("Telefon")
			.anrede("Anrede")
			.titel("Titel")
			.lastModified(Instant.now().minus(3, DAYS))
			.deleted(true)
			.aktiv(true)
			.build();
	}

	@Test
	void testEntityToDtoMap() {
		NutzerEntity nutzerEntity = createEntity();

		NutzerEntityDTO dto = mapper.mapDto(nutzerEntity);
		assertEquals(nutzerEntity.getName(), dto.getName());
		assertEquals(nutzerEntity.getGid(), dto.getGid());
		assertEquals(nutzerEntity.getAbteilung(), dto.getAbteilung());
		assertEquals(nutzerEntity.getReferat(), dto.getReferat());
		assertEquals(nutzerEntity.getRessortId(), dto.getRessortId());
		assertEquals(nutzerEntity.getEmail(), dto.getEmail());
		assertEquals(nutzerEntity.getRessortKurzbezeichnung(), dto.getRessortKurzbezeichnung());
		assertEquals(nutzerEntity.getTelefon(), dto.getTelefon());
		assertEquals(nutzerEntity.getAnrede(), dto.getAnrede());
		assertEquals(nutzerEntity.getTitel(), dto.getTitel());
		assertEquals(nutzerEntity.isDeleted(), dto.isDeleted());
		assertEquals(nutzerEntity.isAktiv(), dto.isAktiv());
	}

	@Test
	void testEntityToSaveDtoMap() {
		NutzerEntity nutzerEntity = createEntity();

		NutzerEntitySaveDTO dto = mapper.mapSaveDto(nutzerEntity);
		assertEquals(nutzerEntity.getName(), dto.getName());
		assertEquals(nutzerEntity.getGid(), dto.getGid());
		assertEquals(nutzerEntity.getAbteilung(), dto.getAbteilung());
		assertEquals(nutzerEntity.getReferat(), dto.getReferat());
		assertEquals(nutzerEntity.getRessortId(), dto.getRessortId());
		assertEquals(nutzerEntity.getEmail(), dto.getEmail());
		assertEquals(nutzerEntity.getRessortKurzbezeichnung(), dto.getRessortKurzbezeichnung());
		assertEquals(nutzerEntity.getTelefon(), dto.getTelefon());
		assertEquals(nutzerEntity.getAnrede(), dto.getAnrede());
		assertEquals(nutzerEntity.getTitel(), dto.getTitel());
		assertEquals(nutzerEntity.getLastModified(), dto.getLastModified());
		assertEquals(nutzerEntity.isDeleted(), dto.isDeleted());
		assertEquals(nutzerEntity.isAktiv(), dto.isAktiv());
	}

	@Test
	void testSaveDtoToEntityMerge() {
		NutzerEntitySaveDTO dto = createSaveDto();
		NutzerEntity nutzerEntity = NutzerEntity.builder().build();
		nutzerEntity = mapper.merge(dto, nutzerEntity);

		assertEquals(dto.getName(), nutzerEntity.getName());
		assertEquals(dto.getGid(), nutzerEntity.getGid());
		assertEquals(dto.getAbteilung(), nutzerEntity.getAbteilung());
		assertEquals(dto.getReferat(), nutzerEntity.getReferat());
		assertEquals(dto.getRessortId(), nutzerEntity.getRessortId());
		assertEquals(dto.getEmail(), nutzerEntity.getEmail());
		assertEquals(dto.getRessortKurzbezeichnung(), nutzerEntity.getRessortKurzbezeichnung());
		assertEquals(dto.getTelefon(), nutzerEntity.getTelefon());
		assertEquals(dto.getAnrede(), nutzerEntity.getAnrede());
		assertEquals(dto.getTitel(), nutzerEntity.getTitel());
		assertEquals(dto.getLastModified(), nutzerEntity.getLastModified());
		assertEquals(dto.isAktiv(), nutzerEntity.isAktiv());
		assertEquals(dto.isDeleted(), nutzerEntity.isDeleted());
	}

}