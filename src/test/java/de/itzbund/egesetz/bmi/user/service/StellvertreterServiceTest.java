// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import de.itzbund.egesetz.bmi.user.dto.StellvertreterEntityDTO;
import de.itzbund.egesetz.bmi.user.entities.StellvertreterEntity;
import de.itzbund.egesetz.bmi.user.mapper.StellvertreterMapper;
import de.itzbund.egesetz.bmi.user.repositories.StellvertreterEntityRepository;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class StellvertreterServiceTest {

	@InjectMocks
	private StellvertreterService service;

	@Mock
	private StellvertreterEntityRepository repository;

	@Mock
	private StellvertreterMapper mapper;

	@Test
	void testGetStellvertreter() {
		UUID nutzerId = UUID.randomUUID();
		List<StellvertreterEntity> entities = List.of(
			StellvertreterEntity.builder().nutzerId(nutzerId).stellvertreterId(UUID.randomUUID()).build(),
			StellvertreterEntity.builder().nutzerId(nutzerId).stellvertreterId(UUID.randomUUID()).build()
		);

		List<StellvertreterEntityDTO> dtos = List.of(
			StellvertreterEntityDTO.builder().nutzerId(nutzerId).stellvertreterId(UUID.randomUUID()).build(),
			StellvertreterEntityDTO.builder().nutzerId(nutzerId).stellvertreterId(UUID.randomUUID()).build()
		);

		when(repository.getStellvertreter(nutzerId)).thenReturn(entities);
		when(mapper.mapDtoList(entities)).thenReturn(dtos);

		List<StellvertreterEntityDTO> result = service.getStellvertreter(nutzerId);

		verify(repository, times(1)).getStellvertreter(any());
		assertEquals(2, result.size());
	}

	@Test
	void testGetStellvertretung() {
		UUID nutzerId = UUID.randomUUID();
		UUID stellvertreterId = UUID.randomUUID();
		StellvertreterEntity entity = StellvertreterEntity.builder().nutzerId(nutzerId).stellvertreterId(stellvertreterId).build();

		when(repository.getStellvertretung(nutzerId, stellvertreterId)).thenReturn(Optional.of(entity));
		when(mapper.mapDto(entity)).thenReturn(new StellvertreterEntityDTO());

		StellvertreterEntityDTO result = service.getStellvertretung(nutzerId, stellvertreterId);

		verify(repository, times(1)).getStellvertretung(any(), any());
		assertNotNull(result);
	}

	@Test
	void testGetStellvertretungen() {
		UUID nutzerId = UUID.randomUUID();
		UUID stellvertreterId = UUID.randomUUID();
		StellvertreterEntity entity = StellvertreterEntity.builder().nutzerId(nutzerId).stellvertreterId(stellvertreterId).build();

		when(repository.getStellvertretungen(nutzerId)).thenReturn(List.of(entity));
		when(mapper.mapDto(entity)).thenReturn(new StellvertreterEntityDTO());

		List<StellvertreterEntityDTO> result = service.getStellvertretungen(nutzerId);

		verify(repository, times(1)).getStellvertretungen(any());
		assertNotNull(result);
		assertEquals(1, result.size());
	}

	@Test
	void testIsStellvertreter() {
		UUID nutzerId = UUID.randomUUID();
		UUID stellvertreterId = UUID.randomUUID();

		when(repository.getStellvertretung(nutzerId, stellvertreterId)).thenReturn(Optional.of(StellvertreterEntity.builder().build()));

		boolean result = service.isStellvertreter(nutzerId, stellvertreterId);

		verify(repository, times(1)).getStellvertretung(any(), any());
		assertTrue(result);
	}

	@Test
	void testSetStellvertreter() {
		List<StellvertreterEntityDTO> toDelete = List.of(new StellvertreterEntityDTO());
		List<StellvertreterEntityDTO> toAdd = List.of(new StellvertreterEntityDTO());

		service.setStellvertreter(toDelete, toAdd);
		verify(repository, times(1)).deleteAll(any());
		verify(repository, times(1)).saveAll(any());
	}
}