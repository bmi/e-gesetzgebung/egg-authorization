// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.entities;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.Instant;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class NutzerEntityTest {

	@Autowired
	private TestEntityManager entityManager;

	private static NutzerEntity createEntity() {
		return NutzerEntity.builder()
			.name("Name")
			.gid("GID")
			.abteilung("Abteilung")
			.referat("Referat")
			.ressortId(99L)
			.email("E-Mail")
			.ressortKurzbezeichnung("Ressort")
			.telefon("Telefon")
			.anrede("Anrede")
			.titel("Titel")
			.erstelltAm(Instant.now().minus(1, DAYS))
			.bearbeitetAm(Instant.now().minus(2, DAYS))
			.lastModified(Instant.now().minus(3, DAYS))
			.deleted(true)
			.aktiv(true)
			.build();
	}

	@Test
	public void testNutzerEntitySaveAndRetrieve() {
		NutzerEntity nutzerEntity = createEntity();

		NutzerEntity savedNutzerEntity = entityManager.persistAndFlush(nutzerEntity);

		NutzerEntity retrievedNutzer = entityManager.find(NutzerEntity.class, savedNutzerEntity.getId());

		assertNotNull(retrievedNutzer);
		assertEquals(nutzerEntity.getName(), retrievedNutzer.getName());
		assertEquals(nutzerEntity.getGid(), retrievedNutzer.getGid());
		assertEquals(nutzerEntity.getAbteilung(), retrievedNutzer.getAbteilung());
		assertEquals(nutzerEntity.getReferat(), retrievedNutzer.getReferat());
		assertEquals(nutzerEntity.getRessortId(), retrievedNutzer.getRessortId());
		assertEquals(nutzerEntity.getEmail(), retrievedNutzer.getEmail());
		assertEquals(nutzerEntity.getRessortKurzbezeichnung(), retrievedNutzer.getRessortKurzbezeichnung());
		assertEquals(nutzerEntity.getTelefon(), retrievedNutzer.getTelefon());
		assertEquals(nutzerEntity.getAnrede(), retrievedNutzer.getAnrede());
		assertEquals(nutzerEntity.getTitel(), retrievedNutzer.getTitel());
		assertEquals(nutzerEntity.getLastModified(), retrievedNutzer.getLastModified());
		assertEquals(nutzerEntity.isAktiv(), retrievedNutzer.isAktiv());
		assertEquals(nutzerEntity.isDeleted(), retrievedNutzer.isDeleted());
		assertEquals(nutzerEntity.getRolle(), retrievedNutzer.getRolle());
	}

	@Test
	void testAdminIs() {
		NutzerEntity nutzerEntity1 = NutzerEntity.builder()
			.email("admin@user.de")
			.build();
		assertTrue(nutzerEntity1.isAdmin());

		NutzerEntity nutzerEntity2 = NutzerEntity.builder()
			.email("another@user.de")
			.build();
		assertFalse(nutzerEntity2.isAdmin());

		NutzerEntity nutzerEntity3 = NutzerEntity.builder()
			.email(null)
			.build();
		assertFalse(nutzerEntity3.isAdmin());
	}
}