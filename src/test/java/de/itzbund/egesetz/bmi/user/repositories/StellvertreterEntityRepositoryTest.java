// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import de.itzbund.egesetz.bmi.user.entities.StellvertreterEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class StellvertreterEntityRepositoryTest {

	@Autowired
	private StellvertreterEntityRepository repository;

	@Test
	void testGetStellvertretung() {
		UUID nutzerId = UUID.randomUUID();
		UUID stellvertreterId = UUID.randomUUID();

		StellvertreterEntity entity = StellvertreterEntity.builder()
			.nutzerId(nutzerId)
			.stellvertreterId(stellvertreterId)
			.build();

		repository.save(entity);

		Optional<StellvertreterEntity> result = repository.getStellvertretung(nutzerId, stellvertreterId);
		assertTrue(result.isPresent());
		assertEquals(nutzerId, result.get().getNutzerId());
		assertEquals(stellvertreterId, result.get().getStellvertreterId());
	}

	@Test
	void testGetStellvertretungen() {
		UUID nutzerId = UUID.randomUUID();
		UUID stellvertreterId = UUID.randomUUID();

		StellvertreterEntity entity = StellvertreterEntity.builder()
			.nutzerId(nutzerId)
			.stellvertreterId(stellvertreterId)
			.build();

		repository.save(entity);

		List<StellvertreterEntity> result = repository.getStellvertretungen(stellvertreterId);
		assertEquals(1, result.size());
		assertEquals(nutzerId, result.get(0).getNutzerId());
		assertEquals(stellvertreterId, result.get(0).getStellvertreterId());
	}

	@Test
	void testGetStellvertreter() {
		UUID nutzerId = UUID.randomUUID();
		UUID stellvertreterId = UUID.randomUUID();

		StellvertreterEntity entity = StellvertreterEntity.builder()
			.nutzerId(nutzerId)
			.stellvertreterId(stellvertreterId)
			.build();

		repository.save(entity);

		List<StellvertreterEntity> result = repository.getStellvertreter(nutzerId);
		assertEquals(1, result.size());
		assertEquals(nutzerId, result.get(0).getNutzerId());
		assertEquals(stellvertreterId, result.get(0).getStellvertreterId());
	}
}