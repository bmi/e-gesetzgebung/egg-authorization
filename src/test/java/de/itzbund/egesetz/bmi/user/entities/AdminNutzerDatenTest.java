// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.entities;

import org.junit.jupiter.api.Test;

import static de.itzbund.egesetz.bmi.user.entities.AdminNutzerDaten.ADMIN_EMAIL;
import static de.itzbund.egesetz.bmi.user.entities.AdminNutzerDaten.ADMIN_GID;
import static de.itzbund.egesetz.bmi.user.entities.AdminNutzerDaten.ADMIN_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AdminNutzerDatenTest {

	@Test
	void testAdminNutzerEntityCreate() {
		NutzerEntity nutzerEntity = AdminNutzerDaten.createAdminNutzerEntity();

		assertNotNull(nutzerEntity);
		assertEquals(ADMIN_EMAIL, nutzerEntity.getEmail());
		assertEquals(ADMIN_GID, nutzerEntity.getGid());
		assertEquals(ADMIN_NAME, nutzerEntity.getName());
	}

}