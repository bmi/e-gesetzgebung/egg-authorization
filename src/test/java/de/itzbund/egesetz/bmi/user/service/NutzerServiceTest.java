// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.service;

import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.repositories.RollenRepository;
import de.itzbund.egesetz.bmi.user.dto.NutzerEntityDTO;
import de.itzbund.egesetz.bmi.user.dto.NutzerEntitySaveDTO;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.mapper.NutzerMapper;
import de.itzbund.egesetz.bmi.user.mapper.NutzerMapperImpl;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class NutzerServiceTest {

	@Spy
	NutzerMapper nutzerMapper = new NutzerMapperImpl();
	@InjectMocks
	NutzerService nutzerService;
	@Mock
	private NutzerEntityRepository nutzerEntityRepository;
	@Mock
	private RollenRepository rollenRepository;

	@BeforeEach
	void setup() throws IllegalAccessException {
		FieldUtils.writeField(nutzerService, "mapper", nutzerMapper, true);
	}

	@Test
	void testNutzerByGidGet() {
		String gid = "gid";

		when(nutzerEntityRepository.findByGid(any())).thenReturn(
			Optional.of(NutzerEntity.builder().build()));

		nutzerService.getNutzerByGid(gid);

		verify(nutzerEntityRepository).findByGid(gid);
		verify(nutzerMapper).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerByGidsGet() {
		List<String> gids = List.of("gid1", "gid2");

		when(nutzerEntityRepository.findAllByGidIn(any())).thenReturn(List.of(
			NutzerEntity.builder().build(),
			NutzerEntity.builder().build()
		));

		nutzerService.getNutzerByGids(gids);

		verify(nutzerEntityRepository).findAllByGidIn(gids);
		verify(nutzerMapper, times(gids.size())).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerByIdGet() {
		UUID id = UUID.randomUUID();

		when(nutzerEntityRepository.findById(any())).thenReturn(
			Optional.of(NutzerEntity.builder().build()));

		nutzerService.getNutzerById(id);

		verify(nutzerEntityRepository).findById(id);
		verify(nutzerMapper).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerByIdsGet() {
		List<UUID> ids = List.of(UUID.randomUUID(), UUID.randomUUID());

		when(nutzerEntityRepository.findAllById(any())).thenReturn(List.of(
			NutzerEntity.builder().build(),
			NutzerEntity.builder().build()
		));

		nutzerService.getNutzerByIds(ids);

		verify(nutzerEntityRepository).findAllById(ids);
		verify(nutzerMapper, times(ids.size())).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerPick() {
		NutzerEntity nutzer1 = NutzerEntity.builder()
			.email("unique-email-address1-no-gid")
			.gid(null)
			.build();

		NutzerEntity nutzer2 = NutzerEntity.builder()
			.email("unique-email-address2-with-gid")
			.gid("unique-gid1")
			.build();

		NutzerEntity nutzer3 = NutzerEntity.builder()
			.email("ambigious-email-address-both-no-gid")
			.gid(null)
			.build();
		NutzerEntity nutzer4 = NutzerEntity.builder()
			.email("ambigious-email-address-both-no-gid")
			.gid(null)
			.build();

		NutzerEntity nutzer5 = NutzerEntity.builder()
			.email("same-email-address-one-with-gid")
			.gid(null)
			.build();
		NutzerEntity nutzer6 = NutzerEntity.builder()
			.email("same-email-address-one-with-gid")
			.gid("unique-gid2")
			.build();

		NutzerEntity nutzer7 = NutzerEntity.builder()
			.email("ambigious-email-address-but-with-ambiguous-gid")
			.gid("unique-gid3")
			.build();
		NutzerEntity nutzer8 = NutzerEntity.builder()
			.email("ambigious-email-address-but-with-ambiguous-gid")
			.gid("unique-gid4")
			.build();

		List<NutzerEntity> nutzer = List.of(nutzer1, nutzer2, nutzer3, nutzer4, nutzer5, nutzer6, nutzer7, nutzer8);

		//test 1: pass empty list -> empty result
		assertThat(NutzerService.pickNutzer(new ArrayList<>(), nutzer1.getEmail()))
			.isEmpty();

		//test 2: e-mail is not in list -> empty result
		assertThat(NutzerService.pickNutzer(nutzer, "email-address-not-in-list"))
			.isEmpty();

		//test 3: e-mail is unique (but without gid, which is irrelevant here)
		assertThat(NutzerService.pickNutzer(nutzer, nutzer1.getEmail()))
			.isPresent().hasValue(nutzer1);

		//test 4: e-mail is unique (but with gid, which is irrelevant here)
		assertThat(NutzerService.pickNutzer(nutzer, nutzer2.getEmail()))
			.isPresent().hasValue(nutzer2);

		//test 5: e-mail is not unique (both users without gid) -> return first entry
		assertThat(NutzerService.pickNutzer(nutzer, nutzer3.getEmail()))
			.isPresent().hasValue(nutzer3);

		//test 6: e-mail is not unique (but one user has a gid) -> return user with gid
		assertThat(NutzerService.pickNutzer(nutzer, nutzer5.getEmail()))
			.isPresent().hasValue(nutzer6);

		//test 7: e-mail is not unique and both have different gids -> return first entry
		assertThat(NutzerService.pickNutzer(nutzer, nutzer7.getEmail()))
			.isPresent().hasValue(nutzer7);
	}


	@Test
	void testNutzerByEmailGet() {
		String emailAddress = "mail-address";

		when(nutzerEntityRepository.findAllByEmailIn(any())).thenReturn(
			List.of(NutzerEntity.builder()
				.email(emailAddress)
				.build()));

		nutzerService.getNutzerByEmail(emailAddress);

		verify(nutzerEntityRepository).findAllByEmailIn(List.of(emailAddress));
		verify(nutzerMapper).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerByEmailsGet() {
		String emailAdress1 = "mail-address-1";
		String emailAdress2 = "mail-address-2";
		List<String> emails = List.of(emailAdress1, emailAdress2);

		when(nutzerEntityRepository.findAllByEmailIn(any())).thenReturn(List.of(
			NutzerEntity.builder()
				.email(emailAdress1)
				.build(),
			NutzerEntity.builder()
				.email(emailAdress2)
				.build()
		));

		nutzerService.getNutzerByEmails(emails);

		verify(nutzerEntityRepository).findAllByEmailIn(emails);
		verify(nutzerMapper, times(emails.size())).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testAllNutzerGet() {
		List<NutzerEntity> repoResult = List.of(
			NutzerEntity.builder().build(),
			NutzerEntity.builder().build()
		);
		when(nutzerEntityRepository.findAll()).thenReturn(repoResult);

		List<NutzerEntityDTO> result = nutzerService.getAllNutzer();
		assertEquals(repoResult.size(), result.size());

		verify(nutzerEntityRepository).findAll();
		verify(nutzerMapper, times(2)).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerByRessortGet() {
		List<NutzerEntity> repoResult = List.of(
			NutzerEntity.builder().build(),
			NutzerEntity.builder().build()
		);
		when(nutzerEntityRepository.findAllByRessortKurzbezeichnungIgnoreCase(any())).thenReturn(repoResult);

		List<NutzerEntityDTO> result = nutzerService.getNutzerByRessort("BT");
		assertEquals(repoResult.size(), result.size());

		verify(nutzerEntityRepository, never()).findAll();
		verify(nutzerEntityRepository).findAllByRessortKurzbezeichnungIgnoreCase("BT");
		verify(nutzerMapper, times(2)).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerForSavingByGidsGet() {
		List<String> gids = List.of("gid1", "gid2");

		when(nutzerEntityRepository.findAllByGidIn(any())).thenReturn(List.of(
			NutzerEntity.builder().build(),
			NutzerEntity.builder().build()
		));

		nutzerService.getUsersForSaving(gids);

		verify(nutzerEntityRepository).findAllByGidIn(gids);
		verify(nutzerMapper, times(gids.size())).mapSaveDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerForSavingGet() {
		String gid = "gid";
		String email = "email";

		when(nutzerEntityRepository.findAllByEmailIn(any())).thenReturn(List.of(
			NutzerEntity.builder().build()
		));

		nutzerService.getUserForSaving(gid, email);

		verify(nutzerEntityRepository).findByGid(gid);
		verify(nutzerEntityRepository).findAllByEmailIn(any());
		verify(nutzerMapper).mapSaveDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzerSave() {
		String gid = "gid";
		NutzerEntitySaveDTO nutzerDto = NutzerEntitySaveDTO.builder()
			.gid(gid)
			.build();

		Runnable reset = () -> {
			reset(nutzerMapper);
			reset(nutzerEntityRepository);
			when(nutzerEntityRepository.save(any())).thenAnswer(i -> i.getArgument(0));
		};

		//test 1: save new user
		reset.run();
		NutzerEntityDTO result1 = nutzerService.saveNutzer(nutzerDto);
		assertNull(result1.getId());
		assertEquals(gid, result1.getGid());

		verify(nutzerEntityRepository).save(any());
		verify(nutzerMapper).merge(any(NutzerEntitySaveDTO.class), any());
		verify(nutzerMapper).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);

		//test 2: update existing user
		reset.run();
		UUID id = UUID.randomUUID();
		NutzerEntity nutzerEntity = NutzerEntity.builder()
			.id(id)
			.gid(gid)
			.build();
		when(nutzerEntityRepository.findByGid(gid)).thenReturn(Optional.of(nutzerEntity));
		NutzerEntityDTO result2 = nutzerService.saveNutzer(nutzerDto);
		assertEquals(id, result2.getId());
		assertEquals(gid, result2.getGid());

		verify(nutzerEntityRepository).save(any());
		verify(nutzerMapper).merge(any(NutzerEntitySaveDTO.class), any());
		verify(nutzerMapper).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testNutzersSave() {
		List<NutzerEntitySaveDTO> nutzerDtos = List.of(
			NutzerEntitySaveDTO.builder().build(),
			NutzerEntitySaveDTO.builder().build()
		);

		nutzerService.saveNutzer(nutzerDtos);

		verify(nutzerEntityRepository).saveAll(any());
		verify(nutzerMapper, times(nutzerDtos.size())).merge(any(NutzerEntitySaveDTO.class), any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testUpUsersWithoutGidClean() {
		List<UUID> ids = List.of(UUID.randomUUID(), UUID.randomUUID());

		nutzerService.cleanUpUsersWithoutGid(ids);

		verify(nutzerEntityRepository).deleteByIdInAndGidIsNull(ids);
	}

	@Test
	void testRolleSet() {
		UUID nutzerId = UUID.randomUUID();
		String rolle = "SACHBEARBEITUNG";
		String unknownRole = "UNKNOWN_ROLE";
		RolleEntity rolleEntity = RolleEntity.builder()
			.bezeichnung(rolle)
			.build();

		//test 1: set invalid role -> throw error
		assertThrows(IllegalArgumentException.class, () -> nutzerService.setRolle(nutzerId, unknownRole));
		verify(rollenRepository).findByBezeichnung(unknownRole);

		//test 2: set valid role
		when(rollenRepository.findByBezeichnung(any())).thenReturn(Optional.of(rolleEntity));
		assertDoesNotThrow(() -> nutzerService.setRolle(nutzerId, rolle));

		//test 3: unset role -> result=true
		assertDoesNotThrow(() -> nutzerService.setRolle(nutzerId, null));

		verify(rollenRepository).findByBezeichnung(rolle);
		verify(nutzerEntityRepository).setRolle(nutzerId, rolleEntity);
	}

	@Test
	void testRolleGet() {
		UUID nutzerId = UUID.randomUUID();
		String rolle = "SACHBEARBEITUNG";
		RolleEntity rolleEntity = RolleEntity.builder()
			.bezeichnung(rolle)
			.build();

		when(nutzerEntityRepository.getRolle(any())).thenReturn(Optional.of(rolleEntity));

		Optional<String> result = nutzerService.getRolle(nutzerId);

		assertTrue(result.isPresent());
		assertEquals(rolle, result.get());
		verify(nutzerEntityRepository).getRolle(nutzerId);
	}

	@Test
	void testAdminNutzerGet() {
		nutzerService.getAdminNutzer();

		verify(nutzerEntityRepository).getOrCreateAdmin();
		verify(nutzerMapper).mapDto(any());
		verifyNoMoreInteractions(nutzerMapper);
	}

	@Test
	void testUserForUpdateFind() {
		UUID id = UUID.randomUUID();
		String gid = "gid";
		String email = "email";
		NutzerEntity nutzerEntity = NutzerEntity.builder()
			.id(id)
			.build();

		NutzerEntitySaveDTO nutzerDto = NutzerEntitySaveDTO.builder()
			.id(null)
			.gid(null)
			.email(null)
			.build();

		//test 1: user has no relevant data -> negative result
		reset(nutzerEntityRepository);
		Optional<NutzerEntity> result = nutzerService.findUserForUpdate(nutzerDto);
		assertTrue(result.isEmpty());
		verify(nutzerEntityRepository, never()).findById(id);
		verify(nutzerEntityRepository, never()).findByGid(gid);
		verify(nutzerEntityRepository, never()).findAllByEmailIn(List.of(email));
		verifyNoMoreInteractions(nutzerEntityRepository);

		nutzerDto = NutzerEntitySaveDTO.builder()
			.id(null)
			.gid(gid)
			.email(email)
			.build();

		//test 2: cannot find user -> negative result
		reset(nutzerEntityRepository);
		result = nutzerService.findUserForUpdate(nutzerDto);
		assertTrue(result.isEmpty());
		verify(nutzerEntityRepository, never()).findById(id);
		verify(nutzerEntityRepository).findByGid(gid);
		verify(nutzerEntityRepository).findAllByEmailIn(List.of(email));
		verifyNoMoreInteractions(nutzerEntityRepository);

		//test 3: find by e-mail (ambiguous result) -> negative result
		reset(nutzerEntityRepository);
		when(nutzerEntityRepository.findAllByEmailIn(List.of(email))).thenReturn(List.of(nutzerEntity, nutzerEntity));
		result = nutzerService.findUserForUpdate(nutzerDto);
		assertTrue(result.isEmpty());
		verify(nutzerEntityRepository, never()).findById(id);
		verify(nutzerEntityRepository).findByGid(gid);
		verify(nutzerEntityRepository).findAllByEmailIn(List.of(email));
		verifyNoMoreInteractions(nutzerEntityRepository);

		//test 4: find by e-mail (non-ambiguous result) -> positive result
		reset(nutzerEntityRepository);
		when(nutzerEntityRepository.findAllByEmailIn(List.of(email))).thenReturn(List.of(nutzerEntity));
		result = nutzerService.findUserForUpdate(nutzerDto);
		assertTrue(result.isPresent());
		verify(nutzerEntityRepository, never()).findById(id);
		verify(nutzerEntityRepository).findByGid(gid);
		verify(nutzerEntityRepository).findAllByEmailIn(List.of(email));
		verifyNoMoreInteractions(nutzerEntityRepository);

		//test 5: find by gid -> positive result
		reset(nutzerEntityRepository);
		when(nutzerEntityRepository.findByGid(gid)).thenReturn(Optional.of(nutzerEntity));
		result = nutzerService.findUserForUpdate(nutzerDto);
		assertTrue(result.isPresent());
		verify(nutzerEntityRepository, never()).findById(id);
		verify(nutzerEntityRepository).findByGid(gid);
		verify(nutzerEntityRepository, never()).findAllByEmailIn(List.of(email));
		verifyNoMoreInteractions(nutzerEntityRepository);

		//test 6: find by egg id -> positive result
		reset(nutzerEntityRepository);
		when(nutzerEntityRepository.findById(id)).thenReturn(Optional.of(nutzerEntity));
		nutzerDto.setId(id);
		result = nutzerService.findUserForUpdate(nutzerDto);
		assertTrue(result.isPresent());
		verify(nutzerEntityRepository).findById(id);
		verify(nutzerEntityRepository, never()).findByGid(gid);
		verify(nutzerEntityRepository, never()).findAllByEmailIn(List.of(email));
		verifyNoMoreInteractions(nutzerEntityRepository);

		//test 7: find by egg id -> negative result
		reset(nutzerEntityRepository);
		nutzerDto.setId(id);
		NutzerEntityDTO nutzerDtoIdNotFound = nutzerDto;
		assertThrows(EntityNotFoundException.class, () -> nutzerService.findUserForUpdate(nutzerDtoIdNotFound));
		verify(nutzerEntityRepository).findById(id);
		verify(nutzerEntityRepository, never()).findByGid(gid);
		verify(nutzerEntityRepository, never()).findAllByEmailIn(List.of(email));
		verifyNoMoreInteractions(nutzerEntityRepository);
	}

	@Test
	void testMapperGet() {
		assertNotNull(nutzerService.getMapper());
	}

	@Test
	void testUserCountForRollen() {
		// given
		List<String> emails = new ArrayList<>();
		String email1 = "testuser1@gmx.de";
		String email2 = "testuser2@gmx.de";
		emails.add(email1);
		emails.add(email2);
		List<String> roles = new ArrayList<>();
		String rolle1 = "BT_FACHADMINISTRATION_AUSSCHUESSE";
		String rolle2 = "BR_FACHADMINISTRATION_P1";
		roles.add(rolle1);
		roles.add(rolle2);
		when(nutzerEntityRepository.countByRolle_BezeichnungIn(roles)).thenReturn(2);

		// when
		Integer usersWithRoleCount = nutzerService.getUserCountForRoles(roles);

		assertThat(usersWithRoleCount).isEqualTo(2);
	}
}