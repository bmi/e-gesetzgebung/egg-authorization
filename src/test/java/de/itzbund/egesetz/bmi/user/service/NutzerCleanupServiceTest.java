// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import de.itzbund.egesetz.bmi.user.repositories.NutzerIds;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NutzerCleanupServiceTest {

	@Mock
	private NutzerEntityRepository nutzerEntityRepository;

	@InjectMocks
	private NutzerCleanupService nutzerCleanupService;

	private UUID nutzerId;

	@BeforeEach
	void setUp() {
		nutzerId = UUID.randomUUID();
	}

	@Test
	void testSetOrphanedNutzerPlattformNoRef() {
		doNothing().when(nutzerEntityRepository).setPlattformNoRef(any(UUID.class));

		nutzerCleanupService.setOrphanedNutzerPlattformNoRef(nutzerId);

		verify(nutzerEntityRepository).setPlattformNoRef(nutzerId);
	}

	@Test
	void testFindNutzerWithoutStellvertreterOrZuordnung_WhenUsersExist() {
		List<NutzerIds> mockNutzerIds = List.of(
			new NutzerIdsImpl(UUID.randomUUID(), null),
			new NutzerIdsImpl(UUID.randomUUID(), null)
		);
		when(nutzerEntityRepository.findDeletedNutzerWithoutStellvertreterOrZuordnung()).thenReturn(mockNutzerIds);

		List<NutzerIds> result = nutzerCleanupService.findNutzerWithoutStellvertreterOrZuordnung();

		assertEquals(mockNutzerIds.size(), result.size());
		assertEquals(mockNutzerIds, result);
	}

	@Test
	void testFindNutzerWithoutStellvertreterOrZuordnung_WhenNoUsersExist() {
		when(nutzerEntityRepository.findDeletedNutzerWithoutStellvertreterOrZuordnung()).thenReturn(Collections.emptyList());

		List<NutzerIds> result = nutzerCleanupService.findNutzerWithoutStellvertreterOrZuordnung();

		assertTrue(result.isEmpty());
	}

	@Test
	void testFindNutzerWithoutStellvertreterOrZuordnung_WhenExceptionOccurs() {
		when(nutzerEntityRepository.findDeletedNutzerWithoutStellvertreterOrZuordnung()).thenThrow(new RuntimeException("Database error"));

		List<NutzerIds> result = nutzerCleanupService.findNutzerWithoutStellvertreterOrZuordnung();

		assertTrue(result.isEmpty());
	}

	@RequiredArgsConstructor
	@Value
	private static class NutzerIdsImpl implements NutzerIds {

		UUID id;
		String gid;
	}
}
