// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.mapper;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import de.itzbund.egesetz.bmi.user.dto.StellvertreterEntityDTO;
import de.itzbund.egesetz.bmi.user.entities.StellvertreterEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {StellvertreterMapperImpl.class})
class StellvertreterMapperTest {

	@Autowired
	StellvertreterMapper mapper;

	private static StellvertreterEntity createEntity() {
		return StellvertreterEntity.builder()
			.stellvertreterId(UUID.randomUUID())
			.nutzerId(UUID.randomUUID())
			.build();
	}

	private static StellvertreterEntityDTO createDto() {
		return StellvertreterEntityDTO.builder()
			.stellvertreterId(UUID.randomUUID())
			.nutzerId(UUID.randomUUID())
			.build();
	}

	@Test
	void testEntityToDtoMap() {
		StellvertreterEntity entity = createEntity();

		StellvertreterEntityDTO dto = mapper.mapDto(entity);
		assertEquals(entity.getNutzerId(), dto.getNutzerId());
		assertEquals(entity.getStellvertreterId(), dto.getStellvertreterId());
	}

	@Test
	void testEntityToSaveDtoMap() {
		StellvertreterEntityDTO dto = createDto();

		StellvertreterEntity entity = mapper.map(dto);
		assertEquals(dto.getNutzerId(), entity.getNutzerId());
		assertEquals(dto.getStellvertreterId(), entity.getStellvertreterId());
	}
}