// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.repositories;

import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.repositories.RollenRepository;
import de.itzbund.egesetz.bmi.user.entities.AdminNutzerDaten;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.enums.BtAusschussType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class NutzerEntityRepositoryTest {

	@Autowired
	private NutzerEntityRepository nutzerEntityRepository;
	@Autowired
	private RollenRepository rollenRepository;

	@Test
	void testAdminGet() {
		NutzerEntity adminEntity = AdminNutzerDaten.createAdminNutzerEntity();
		adminEntity.setErstelltAm(Instant.now());
		adminEntity.setBearbeitetAm(Instant.now());

		//test 1: admin does not exist -> create and return admin
		assertTrue(nutzerEntityRepository.findByGid(adminEntity.getGid()).isEmpty());
		NutzerEntity retrievedAdminEntity1 = nutzerEntityRepository.getOrCreateAdmin();
		assertNotNull(retrievedAdminEntity1.getId());
		assertTrue(nutzerEntityRepository.findByGid(adminEntity.getGid()).isPresent());

		//test 2: admin already exists after test 1 -> simply return entry
		assertTrue(nutzerEntityRepository.findByGid(adminEntity.getGid()).isPresent());
		NutzerEntity retrievedAdminEntity2 = nutzerEntityRepository.getOrCreateAdmin();
		assertNotNull(retrievedAdminEntity2.getId());
		assertTrue(nutzerEntityRepository.findByGid(adminEntity.getGid()).isPresent());
	}

	@Test
	void testNutzerByRessortFind() {
		NutzerEntity nutzer1 = NutzerEntity.builder()
			.ressortKurzbezeichnung("BMI")
			.erstelltAm(Instant.now())
			.bearbeitetAm(Instant.now())
			.email("user1")
			.build();
		NutzerEntity nutzer2 = NutzerEntity.builder()
			.ressortKurzbezeichnung("BT")
			.erstelltAm(Instant.now())
			.bearbeitetAm(Instant.now())
			.email("user2")
			.build();
		NutzerEntity nutzer3 = NutzerEntity.builder()
			.ressortKurzbezeichnung("bt")
			.erstelltAm(Instant.now())
			.bearbeitetAm(Instant.now())
			.email("user3")
			.build();

		nutzerEntityRepository.save(nutzer1);
		nutzerEntityRepository.save(nutzer2);
		nutzerEntityRepository.save(nutzer3);

		//test 1: Bundestag: correct spelling
		List<NutzerEntity> result1 = nutzerEntityRepository.findAllByRessortKurzbezeichnungIgnoreCase("BT");
		assertThat(result1).hasSize(2);
		result1.forEach(n -> assertThat(n.getRessortKurzbezeichnung()).isEqualToIgnoringCase("BT"));

		//test 2: Bundestag: ignore case
		List<NutzerEntity> result2 = nutzerEntityRepository.findAllByRessortKurzbezeichnungIgnoreCase("bt");
		assertThat(result2).hasSize(2);
		result2.forEach(n -> assertThat(n.getRessortKurzbezeichnung()).isEqualToIgnoringCase("BT"));

		//test 3: test BMI
		List<NutzerEntity> result3 = nutzerEntityRepository.findAllByRessortKurzbezeichnungIgnoreCase("BMI");
		assertThat(result3).hasSize(1);
		result3.forEach(n -> assertThat(n.getRessortKurzbezeichnung()).isEqualToIgnoringCase("BMI"));

		//test 4: test BMJ -> no result
		List<NutzerEntity> result4 = nutzerEntityRepository.findAllByRessortKurzbezeichnungIgnoreCase("BMJ");
		assertThat(result4).isEmpty();
	}

	@Test
	void testRolleSetAndGet() {
		NutzerEntity nutzerEntity = nutzerEntityRepository.save(NutzerEntity.builder()
			.email("email")
			.bearbeitetAm(Instant.now())
			.erstelltAm(Instant.now())
			.build());
		RolleEntity rolleEntity = rollenRepository.save(RolleEntity.builder()
			.bezeichnung("SACHBEARBEITUNG")
			.build());

		//test 1: user has no role -> Optional.empty
		Optional<RolleEntity> rolleFound1 = nutzerEntityRepository.getRolle(nutzerEntity.getId());
		assertTrue(rolleFound1.isEmpty());

		//test 2: assign role to known user -> result=1
		int result1 = nutzerEntityRepository.setRolle(nutzerEntity.getId(), rolleEntity);
		assertThat(result1).isEqualTo(1);

		//test 3: check if user actually has assigned role
		Optional<RolleEntity> rolleFound2 = nutzerEntityRepository.getRolle(nutzerEntity.getId());
		assertTrue(rolleFound2.isPresent());
		assertEquals("SACHBEARBEITUNG", rolleFound2.get().getBezeichnung());

		//test 4: unassign role of unknown user -> result=1
		int result2 = nutzerEntityRepository.setRolle(nutzerEntity.getId(), null);
		assertThat(result2).isEqualTo(1);

		//test 5: set role for unknown user -> result=0
		UUID unknownNutzerId = UUID.randomUUID();
		int result3 = nutzerEntityRepository.setRolle(unknownNutzerId, rolleEntity);
		assertThat(result3).isEqualTo(0);
	}

	@Test
	void testAusschussSet() {
		NutzerEntity nutzerEntity = nutzerEntityRepository.save(NutzerEntity.builder()
			.email("email")
			.bearbeitetAm(Instant.now())
			.erstelltAm(Instant.now())
			.build());

		int result1 = nutzerEntityRepository.setAusschuss(nutzerEntity.getId(), BtAusschussType.PA_3);
		assertThat(result1).isEqualTo(1);

	}

}