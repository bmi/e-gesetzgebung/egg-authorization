// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.user.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NutzerEntityDTOTest {

	@Test
	void testBundestagIs() {
		NutzerEntityDTO dto = NutzerEntityDTO.builder().build();
		assertFalse(dto.isBundestagUser());

		dto.setRessortKurzbezeichnung("BMI");
		assertFalse(dto.isBundestagUser());

		dto.setRessortKurzbezeichnung(" bt ");
		assertTrue(dto.isBundestagUser());
	}
}