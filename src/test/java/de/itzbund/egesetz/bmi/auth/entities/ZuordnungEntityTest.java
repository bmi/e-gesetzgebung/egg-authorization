// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
class ZuordnungEntityTest {

    @Autowired
    private TestEntityManager entityManager;

    private ZuordnungEntity zuordnung;


    @BeforeEach
    public void setUp() {
        zuordnung = new ZuordnungEntity();
        zuordnung.setBezeichnung("TestZuordnung");
        zuordnung.setRessourceId("TestRessourceId");
        zuordnung.setBenutzerId("TestBenutzerId");

        RessourceTypEntity ressourceTyp = new RessourceTypEntity();
        ressourceTyp.setBezeichnung("TestRessourceTyp");
        entityManager.persistAndFlush(ressourceTyp);

        zuordnung.setRessourceTyp(ressourceTyp);

        RolleEntity rolle = new RolleEntity();
        rolle.setBezeichnung("TestRolle");
        entityManager.persistAndFlush(rolle);

        zuordnung.setRolle(rolle);
        entityManager.persistAndFlush(zuordnung);
    }


    @Test
    void testZuordnungEntitySaveAndRetrieve() {
        entityManager.persistAndFlush(zuordnung);

        ZuordnungEntity retrievedZuordnung = entityManager.find(ZuordnungEntity.class, zuordnung.getId());

        assertNotNull(retrievedZuordnung);
        assertEquals("TestZuordnung", retrievedZuordnung.getBezeichnung());
        assertEquals("TestRessourceId", retrievedZuordnung.getRessourceId());
        assertEquals("TestBenutzerId", retrievedZuordnung.getBenutzerId());
        assertNotNull(retrievedZuordnung.getRessourceTyp());
        assertNotNull(retrievedZuordnung.getRolle());
    }


    @Test
    void testZuordnungEntityWithNullBezeichnung() {
        zuordnung.setBezeichnung(null);
        entityManager.persistAndFlush(zuordnung);

        ZuordnungEntity retrievedZuordnung = entityManager.find(ZuordnungEntity.class, zuordnung.getId());

        assertNotNull(retrievedZuordnung);
        assertNull(retrievedZuordnung.getBezeichnung());
    }


    @Test
    void testZuordnungEntityWithNullRessourceTyp() {
        zuordnung.setRessourceTyp(null);
 		assertThrows(PersistenceException.class, () -> {
			entityManager.persistAndFlush(zuordnung);
		});
	}
}
