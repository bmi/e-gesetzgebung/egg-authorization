// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.StatusEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@SqlGroup({
//        @Sql(scripts = "/schema.sql"),
//        @Sql(scripts = "/data.sql")
})
@SpringBootTest
class ZuordnungenRepositoryTest {

	@Autowired
	private RessourceTypenRepository ressourceTypenRepository;
	@Autowired
	private RollenRepository rollenRepository;
	@Autowired
	private AktionenRepository aktionenRepository;
	@Autowired
	private StatusRepository statusRepository;

	@Autowired
	private ZuordnungenRepository zuordnungenRepository;

	private List<RessourceTypEntity> ressourceTypen = null;
	private List<RolleEntity> rollen = null;
	private List<AktionEntity> aktionen = null;
	private List<StatusEntity> statusList = null;

	private static boolean equals(String s1, String s2) {
		return StringUtils.equalsIgnoreCase(StringUtils.trim(s1), StringUtils.trim(s2));
	}

	@BeforeEach
	void init() {
		ressourceTypen = ressourceTypenRepository.findAll();
		rollen = rollenRepository.findAll();
		aktionen = aktionenRepository.findAll();
		statusList = statusRepository.findAll();
	}

	private RessourceTypEntity getRessourceTyp(String bezeichnung) {
		return ressourceTypen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Ressource-Typ '" + bezeichnung + "' nicht gefunden!"));
	}

	private RolleEntity getRolle(String bezeichnung) {
		return rollen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Rolle '" + bezeichnung + "' nicht gefunden!"));
	}

	private AktionEntity getAktion(String bezeichnung) {
		return aktionen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Aktion '" + bezeichnung + "' nicht gefunden!"));
	}

	private StatusEntity getStatus(String bezeichnung) {
		return statusList.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Status '" + bezeichnung + "' nicht gefunden!"));
	}


	@Test
	void whenCreateOneRelation_thenItShouldBePossible() {
		String relationsName = "Zuordnung 1";
		String benutzerId = "Benutzer 1";
		String resourceId = "Resource 1";

		// Arrange
		ZuordnungEntity zuordnungEntity = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("ERSTELLER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.bezeichnung(relationsName)
			.anmerkungen("Test123")
			.build();

		// Act
		zuordnungenRepository.save(zuordnungEntity);
		Optional<ZuordnungEntity> actual = zuordnungenRepository.findByBezeichnung(relationsName);

		// Assert
		assertThat(actual).isPresent();
		ZuordnungEntity actualRelation = actual.get();

		assertThat(actualRelation.getBezeichnung()).isEqualTo(relationsName);

		String benutzerInRelation = actualRelation.getBenutzerId();
		assertThat(benutzerInRelation).isEqualTo(benutzerId);

		RessourceTypEntity ressourceTypInReleation = actualRelation.getRessourceTyp();
		assertThat(ressourceTypInReleation.getBezeichnung()).isEqualTo(getRessourceTyp("DOKUMENTE").getBezeichnung());

		RolleEntity roleInRelation = actualRelation.getRolle();
		assertThat(roleInRelation.getBezeichnung()).isEqualTo(getRolle("ERSTELLER").getBezeichnung());

		String resourceInRelation = actualRelation.getRessourceId();
		assertThat(resourceInRelation).isEqualTo(resourceId);
	}


	@Test
	void whenCreateDifferentRelation_thenItShouldBePossible() {

		String relationsName1 = "Zuordnung 2";
		String relationsName2 = "Zuordnung 3";
		String benutzerId = "Benutzer 1";
		String resourceId = "Resource 1";

		// Arrange
		ZuordnungEntity zuordnungEntity1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("ERSTELLER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTENMAPPE"))
			.bezeichnung(relationsName1)
			.anmerkungen("Test123")
			.build();

		ZuordnungEntity zuordnungEntity2 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("LESER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTENMAPPE"))
			.bezeichnung(relationsName2)
			.anmerkungen("Test123")
			.build();

		// Act
		zuordnungenRepository.save(zuordnungEntity1);
		zuordnungenRepository.save(zuordnungEntity2);

		Optional<ZuordnungEntity> presents = zuordnungenRepository.findByBezeichnung(relationsName1);
		Optional<ZuordnungEntity> actual = zuordnungenRepository.findByBezeichnung(relationsName2);

		// Assert
		// Proofed by first test, but must exist
		assertThat(presents).isPresent();

		// Proof of this methode
		assertThat(actual).isPresent();
		ZuordnungEntity actualRelation = actual.get();

		assertThat(actualRelation.getBezeichnung()).isEqualTo(relationsName2);

		String benutzerInRelation = actualRelation.getBenutzerId();
		assertThat(benutzerInRelation).isEqualTo(benutzerId);

		RessourceTypEntity ressourceTypInReleation = actualRelation.getRessourceTyp();
		assertThat(ressourceTypInReleation.getBezeichnung()).isEqualTo(getRessourceTyp("DOKUMENTENMAPPE").getBezeichnung());

		RolleEntity roleInRelation = actualRelation.getRolle();
		assertThat(roleInRelation.getBezeichnung()).isEqualTo(getRolle("LESER").getBezeichnung());

		String resourceInRelation = actualRelation.getRessourceId();
		assertThat(resourceInRelation).isEqualTo(resourceId);
	}


	@Test
	void whenCreateSameRuleByContent_thenAnErrorShouldBeShown() {
		String relationsName1 = "Zuordnung 4";
		String relationsName2 = "Zuordnung 5";
		String benutzerId = "Benutzer 1";
		String resourceId = "Resource 1";

		// Arrange
		ZuordnungEntity zuordnungEntity1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("LESER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.bezeichnung(relationsName1)
			.anmerkungen("Test123")
			.build();

		ZuordnungEntity zuordnungEntity2 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("LESER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.bezeichnung(relationsName2)
			.anmerkungen("Test123")
			.build();

		zuordnungenRepository.save(zuordnungEntity1);

		// Assert
		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			zuordnungenRepository.save(zuordnungEntity2);
		});
	}


	@Test
	void findByParameters() {
		String relationsName1 = "Mitarbeiter Rolle";
		String relationsName2 = "Ersteller Rolle";

		String benutzerId = "Benutzer 1";
		String resourceId = "Formular A13";

		// Arrange
		ZuordnungEntity zuordnungEntity1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("LESER")) // Mitarbeiter
			.ressourceTyp(getRessourceTyp("DOKUMENTE")) // Dokument
			.bezeichnung(relationsName1)
			.anmerkungen("Test123")
			.deaktiviert(true)
			.build();

		ZuordnungEntity zuordnungEntity2 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("ERSTELLER")) // Ersteller
			.ressourceTyp(getRessourceTyp("DOKUMENTE")) // Dokument
			.bezeichnung(relationsName2)
			.anmerkungen("Test123")
			.build();

		zuordnungenRepository.save(zuordnungEntity1);
		zuordnungenRepository.save(zuordnungEntity2);

		// Act
		// Welche Rollen habe ich?
		Optional<List<ZuordnungEntity>> actualOptional = zuordnungenRepository.findByRessourceTypAndRessourceIdAndBenutzerIdAndDeaktiviert(
			getRessourceTyp("DOKUMENTE"), resourceId, benutzerId, false);

		assertThat(actualOptional).isPresent();
		List<ZuordnungEntity> actual = actualOptional.get();

		// Ein Eintrag ist 'deaktiviert' und deshalb bleibt nur ein Ergebnis übrig
		assertThat(actual).hasSize(1);
	}

	@Test
	void testFindByRessourceTypAndBenutzerIdAndRessourceIdAndRolle() {
		String relationsName1 = "Mitarbeiter Rolle X";
		String benutzerId = "Benutzer 1X";
		String resourceId = "Formular A13X";

		// Arrange
		ZuordnungEntity zuordnungEntity1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("LESER")) // Mitarbeiter
			.ressourceTyp(getRessourceTyp("DOKUMENTE")) // Dokument
			.bezeichnung(relationsName1)
			.anmerkungen("Test123")
			.build();

		zuordnungenRepository.save(zuordnungEntity1);

		// Act
		// Welche Rollen habe ich?
		Optional<ZuordnungEntity> actualOptional = zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRessourceIdAndRolle(
			getRessourceTyp("DOKUMENTE"),
			benutzerId,
			resourceId,
			getRolle("LESER"));

		assertThat(actualOptional).isPresent();
		ZuordnungEntity actual = actualOptional.get();

		assertThat(actual).isEqualTo(zuordnungEntity1);
	}

	@Test
	void testDeleteEntry() {
		String relationsName1 = "Mitarbeiter Rolle Y";
		String benutzerId = "Benutzer 1Y";
		String resourceId = "Formular A13Y";

		// Arrange
		ZuordnungEntity zuordnungEntity1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.ressourceId(resourceId)
			.rolle(getRolle("LESER")) // Mitarbeiter
			.ressourceTyp(getRessourceTyp("DOKUMENTE")) // Dokument
			.bezeichnung(relationsName1)
			.anmerkungen("Test123")
			.build();

		zuordnungenRepository.save(zuordnungEntity1);
		zuordnungenRepository.delete(zuordnungEntity1);

		// Act
		// Welche Rollen habe ich?
		Optional<ZuordnungEntity> actualOptional = zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRessourceIdAndRolle(
			getRessourceTyp("DOKUMENTE"),
			benutzerId,
			resourceId,
			getRolle("LESER"));

		assertThat(actualOptional).isEmpty();
	}

	@Test
	void findAllBySpecification() {
		Optional<Collection<String>> ressourcenIds = Optional.of(List.of("1", "2"));
		Optional<Collection<String>> benutzerIds = Optional.of(List.of("1", "2"));
		Optional<Collection<RolleEntity>> rollen = Optional.of(List.of(getRolle("ERSTELLER")));
		Optional<Collection<AktionEntity>> aktionen = Optional.of(List.of(getAktion("LESEN")));
		Optional<Collection<RessourceTypEntity>> ressourceTypen = Optional.of(List.of(getRessourceTyp("REGELUNGSVORHABEN")));
		Pageable pageable = Pageable.ofSize(10);

		//test 1: all parameters are empty
		Specification<ZuordnungEntity> spec1 = zuordnungenRepository.createSpecification(
			Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
		List<ZuordnungEntity> result1 = assertDoesNotThrow(() -> zuordnungenRepository.findAll(spec1));
		assertNotNull(result1);

		//test 2: all parameters are set
		Specification<ZuordnungEntity> spec2 = zuordnungenRepository.createSpecification(
			benutzerIds, aktionen, ressourceTypen, ressourcenIds, rollen);
		List<ZuordnungEntity> result2 = assertDoesNotThrow(() -> zuordnungenRepository.findAll(spec2));
		assertNotNull(result2);
	}

	@Test
	void findAktionenBySpecification() {
		RessourceTypEntity ressourceTyp = getRessourceTyp("REGELUNGSVORHABEN");
		List<RolleEntity> rollen = List.of(getRolle("FEDERFUEHRER"));
		Optional<Collection<StatusEntity>> status = Optional.of(List.of(getStatus("IN_ENTWURF")));
		Optional<Collection<AktionEntity>> aktionen = Optional.of(List.of(getAktion("LOESCHEN")));

		//test 1: all optional parameters are set
		Specification<AktionEntity> spec1 = aktionenRepository.createSpecification(ressourceTyp, rollen, Optional.empty(), Optional.empty());
		List<AktionEntity> result1 = assertDoesNotThrow(() -> aktionenRepository.findAll(spec1));
		assertThat(result1).isNotEmpty();

		//test 2: all parameters are set
		Specification<AktionEntity> spec2 = aktionenRepository.createSpecification(ressourceTyp, rollen, status, aktionen);
		List<AktionEntity> result2 = assertDoesNotThrow(() -> aktionenRepository.findAll(spec2));
		assertThat(result2).isNotEmpty();
	}


	/**
	 * In der Run Configuration:
	 * <p>
	 * -Dspring.config.location=classpath:application-aws-dev.properties
	 */
	@Disabled("Spezielle Version für AWS in der IDE")
	@Test
	void runOnAws() {
		String resourceId = "a60b1437-8e78-4a8d-b4c4-4072b289cbfd";
		String benutzerId = "someRandomId";
		Optional<List<ZuordnungEntity>> actualOptional = zuordnungenRepository.findByRessourceTypAndRessourceIdAndBenutzerIdAndDeaktiviert(
			getRessourceTyp("DOKUMENTENMAPPE"), resourceId, benutzerId, false);

		assertThat(actualOptional).isPresent();
	}
}
