// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class RegelAdministrationEntityTest {

    @Autowired
    private TestEntityManager entityManager;

    private RegelAdministrationEntity regelAdministrationEntity;


    @BeforeEach
    public void setUp() {
        regelAdministrationEntity = new RegelAdministrationEntity();

        RolleEntity rolle = new RolleEntity();
        rolle.setBezeichnung("TestRolle");
        entityManager.persistAndFlush(rolle);

        AktionEntity aktion = new AktionEntity();
        aktion.setBezeichnung("TestAktion");
        entityManager.persistAndFlush(aktion);


        regelAdministrationEntity.setRolle(rolle);
        regelAdministrationEntity.setAktion(aktion);
		regelAdministrationEntity.setAngefragteRolleId(1L);

        entityManager.persistAndFlush(regelAdministrationEntity);
    }


    @Test
    public void testRegelAdministrationEntitySaveAndRetrieve() {
        entityManager.persistAndFlush(regelAdministrationEntity);

        RegelAdministrationEntity retrievedRegelAdministrationEntity = entityManager.find(RegelAdministrationEntity.class, regelAdministrationEntity.getId());

        assertNotNull(retrievedRegelAdministrationEntity);
        assertNotNull(retrievedRegelAdministrationEntity.getRolle());
        assertNotNull(retrievedRegelAdministrationEntity.getAktion());
    }
}
