// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class RegelEntityTest {

    @Autowired
    private TestEntityManager entityManager;

    private RegelEntity regel;


    @BeforeEach
    public void setUp() {
        regel = new RegelEntity();
        regel.setBezeichnung("TestRegel");

        RessourceTypEntity ressourceTyp = new RessourceTypEntity();
        ressourceTyp.setBezeichnung("TestRessourceTyp");
        entityManager.persistAndFlush(ressourceTyp);

        RolleEntity rolle = new RolleEntity();
        rolle.setBezeichnung("TestRolle");
        entityManager.persistAndFlush(rolle);

        StatusEntity status = new StatusEntity();
        status.setBezeichnung("TestStatus");
        entityManager.persistAndFlush(status);

        AktionEntity aktion = new AktionEntity();
        aktion.setBezeichnung("TestAktion");
        entityManager.persistAndFlush(aktion);

        regel.setRessourceTyp(ressourceTyp);
        regel.setRolle(rolle);
        regel.setStatus(status);
        regel.setAktion(aktion);

        entityManager.persistAndFlush(regel);
    }


    @Test
    public void testRegelEntitySaveAndRetrieve() {
        entityManager.persistAndFlush(regel);

        RegelEntity retrievedRegel = entityManager.find(RegelEntity.class, regel.getId());

        assertNotNull(retrievedRegel);
        assertEquals("TestRegel", retrievedRegel.getBezeichnung());
        assertNotNull(retrievedRegel.getRessourceTyp());
        assertNotNull(retrievedRegel.getRolle());
        assertNotNull(retrievedRegel.getStatus());
        assertNotNull(retrievedRegel.getAktion());
    }


    @Test
    public void testRegelEntityWithNullBezeichnung() {
        regel.setBezeichnung(null);
        entityManager.persistAndFlush(regel);

        RegelEntity retrievedRegel = entityManager.find(RegelEntity.class, regel.getId());

        assertNotNull(retrievedRegel);
        assertNull(retrievedRegel.getBezeichnung());
    }
}
