// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api.possibilities;

import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.repositories.AktionenRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AktionenTest {

    @Autowired
    private Aktionen aktionen;

    @MockBean
    private AktionenRepository aktionenRepository;


    @Test
    public void testGetAktionen() {
        // Mocking the repository to return a list of AktionEntity objects
        AktionEntity aktion1 = new AktionEntity();
        aktion1.setBezeichnung("Aktion 1");

        AktionEntity aktion2 = new AktionEntity();
        aktion2.setBezeichnung("Aktion 2");

        when(aktionenRepository.findAll()).thenReturn(Arrays.asList(aktion1, aktion2));

        List<String> result = aktionen.getAktionen();

        // Verify that the method returns the expected list of actions
        assertEquals(Arrays.asList("Aktion 1", "Aktion 2"), result);
    }
}
