// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.StatusEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungGlobaleRolleEntity;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@SqlGroup({
//        @Sql(scripts = "/schema.sql"),
//        @Sql(scripts = "/data.sql")
})
@SpringBootTest
class ZuordnungenGlobaleRollenRepositoryTest {

	@Autowired
	private RessourceTypenRepository ressourceTypenRepository;
	@Autowired
	private RollenRepository rollenRepository;
	@Autowired
	private ZuordnungenGlobaleRollenRepository zuordnungenGlobaleRollenRepository;

	private List<RessourceTypEntity> ressourceTypen = null;
	private List<RolleEntity> rollen = null;

	private static boolean equals(String s1, String s2) {
		return StringUtils.equalsIgnoreCase(StringUtils.trim(s1), StringUtils.trim(s2));
	}

	@BeforeEach
	void init() {
		ressourceTypen = ressourceTypenRepository.findAll();
		rollen = rollenRepository.findAll();
	}

	private RessourceTypEntity getRessourceTyp(String bezeichnung) {
		return ressourceTypen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Ressource-Typ '" + bezeichnung + "' nicht gefunden!"));
	}

	private RolleEntity getRolle(String bezeichnung) {
		return rollen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Rolle '" + bezeichnung + "' nicht gefunden!"));
	}


	@Test
	void whenCreateOneRelation_thenItShouldBePossible() {
		String relationsName = "ZuordnungGlobaleRolle 1";
		Long rolleId = 1L;
		String resourceId = "Resource 1";

		// Arrange
		ZuordnungGlobaleRolleEntity zuordnungGlobaleRolleEntity = ZuordnungGlobaleRolleEntity.builder()
			.rolleId(rolleId)
			.ressourceId(resourceId)
			.globaleRolle(getRolle("ERSTELLER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.bezeichnung(relationsName)
			.build();

		// Act
		zuordnungenGlobaleRollenRepository.save(zuordnungGlobaleRolleEntity);
		Optional<List<ZuordnungGlobaleRolleEntity>> actual = zuordnungenGlobaleRollenRepository.findByRessourceIdAndAndRolleId(resourceId, rolleId);

		// Assert
		assertThat(actual).isPresent();
		ZuordnungGlobaleRolleEntity actualRelation = actual.get().get(0);

		assertThat(actualRelation.getBezeichnung()).isEqualTo(relationsName);

		Long rolleInRelation = actualRelation.getRolleId();
		assertThat(rolleInRelation).isEqualTo(rolleId);

		RessourceTypEntity ressourceTypInReleation = actualRelation.getRessourceTyp();
		assertThat(ressourceTypInReleation.getBezeichnung()).isEqualTo(getRessourceTyp("DOKUMENTE").getBezeichnung());

		RolleEntity globaleRolleInRelation = actualRelation.getGlobaleRolle();
		assertThat(globaleRolleInRelation.getBezeichnung()).isEqualTo(getRolle("ERSTELLER").getBezeichnung());

		String resourceInRelation = actualRelation.getRessourceId();
		assertThat(resourceInRelation).isEqualTo(resourceId);
	}

	@Test
	void testFindByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle() {
		String relationsName = "ZuordnungGlobaleRolle 1";
		Long rolleId = 2L;
		String resourceId = "Resource 1";

		// Arrange
		ZuordnungGlobaleRolleEntity zuordnungGlobaleRolleEntity = ZuordnungGlobaleRolleEntity.builder()
			.rolleId(rolleId)
			.ressourceId(resourceId)
			.globaleRolle(getRolle("LESER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.bezeichnung(relationsName)
			.build();

		// Act
		zuordnungenGlobaleRollenRepository.save(zuordnungGlobaleRolleEntity);
		Optional<ZuordnungGlobaleRolleEntity> actual = zuordnungenGlobaleRollenRepository.findByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle
			(getRessourceTyp("DOKUMENTE"), rolleId, resourceId, getRolle("LESER"));

		// Assert
		assertThat(actual).isPresent();
		ZuordnungGlobaleRolleEntity actualRelation = actual.get();

		assertThat(actualRelation).isEqualTo(zuordnungGlobaleRolleEntity);
	}

	@Test
	void testFindByRessourceTypAndRoleIdIdAndGlobaleRolle() {
		String relationsName = "ZuordnungGlobaleRolle 1";
		Long rolleId = 4L;
		String resourceId = "Resource 1";

		// Arrange
		ZuordnungGlobaleRolleEntity zuordnungGlobaleRolleEntity = ZuordnungGlobaleRolleEntity.builder()
			.rolleId(rolleId)
			.ressourceId(resourceId)
			.globaleRolle(getRolle("LESER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.bezeichnung(relationsName)
			.build();

		// Act
		zuordnungenGlobaleRollenRepository.save(zuordnungGlobaleRolleEntity);
		Optional<List<ZuordnungGlobaleRolleEntity>> actual = zuordnungenGlobaleRollenRepository.findByRessourceTypAndRolleIdAndGlobaleRolle(getRessourceTyp("DOKUMENTE"), rolleId, getRolle("LESER"));

		// Assert
		assertThat(actual).isPresent();
		ZuordnungGlobaleRolleEntity actualRelation = actual.get().get(0);

		assertThat(actualRelation.getBezeichnung()).isEqualTo(relationsName);

		Long rolleInRelation = actualRelation.getRolleId();
		assertThat(rolleInRelation).isEqualTo(rolleId);

		RessourceTypEntity ressourceTypInReleation = actualRelation.getRessourceTyp();
		assertThat(ressourceTypInReleation.getBezeichnung()).isEqualTo(getRessourceTyp("DOKUMENTE").getBezeichnung());

		RolleEntity globaleRolleInRelation = actualRelation.getGlobaleRolle();
		assertThat(globaleRolleInRelation.getBezeichnung()).isEqualTo(getRolle("LESER").getBezeichnung());

		String resourceInRelation = actualRelation.getRessourceId();
		assertThat(resourceInRelation).isEqualTo(resourceId);
	}

	@Test
	void testDeleteByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle() {
		String relationsName = "ZuordnungGlobaleRolle 1";
		Long rolleId = 3L;
		String resourceId = "Resource 1";

		// Arrange
		ZuordnungGlobaleRolleEntity zuordnungGlobaleRolleEntity = ZuordnungGlobaleRolleEntity.builder()
			.rolleId(rolleId)
			.ressourceId(resourceId)
			.globaleRolle(getRolle("LESER"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.bezeichnung(relationsName)
			.build();

		// Act
		zuordnungenGlobaleRollenRepository.save(zuordnungGlobaleRolleEntity);
		int result = zuordnungenGlobaleRollenRepository.deleteByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle
			(getRessourceTyp("DOKUMENTE"), rolleId, resourceId, getRolle("LESER"));
		Optional<ZuordnungGlobaleRolleEntity> actual = zuordnungenGlobaleRollenRepository.findByRessourceTypAndRolleIdAndRessourceIdAndGlobaleRolle
			(getRessourceTyp("DOKUMENTE"), rolleId, resourceId, getRolle("LESER"));

		assertThat(actual).isEmpty();

	}
}
