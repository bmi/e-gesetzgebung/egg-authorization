// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class RolleEntityTest {

    @Autowired
    private TestEntityManager entityManager;

    private RolleEntity rolle;


    @BeforeEach
    public void setUp() {
        rolle = new RolleEntity();
        rolle.setBezeichnung("TestRolle");

        Set<RegelEntity> regeln = new HashSet<>();
        RegelEntity regel1 = new RegelEntity();
        regel1.setBezeichnung("TestRegel1");
        entityManager.persistAndFlush(regel1);

        RegelEntity regel2 = new RegelEntity();
        regel2.setBezeichnung("TestRegel2");
        entityManager.persistAndFlush(regel2);

        regeln.add(regel1);
        regeln.add(regel2);

        rolle.setRegel(regeln);
        entityManager.persistAndFlush(rolle);
    }


    @Test
    public void testRolleEntitySaveAndRetrieve() {
        entityManager.persistAndFlush(rolle);

        RolleEntity retrievedRolle = entityManager.find(RolleEntity.class, rolle.getId());

        assertNotNull(retrievedRolle);
        assertEquals("TestRolle", retrievedRolle.getBezeichnung());
        assertEquals(2, retrievedRolle.getRegel().size());
    }


    @Test
    public void testRolleEntityWithNullBezeichnung() {
        rolle.setBezeichnung(null);
        entityManager.persistAndFlush(rolle);

        RolleEntity retrievedRolle = entityManager.find(RolleEntity.class, rolle.getId());

        assertNotNull(retrievedRolle);
        assertNull(retrievedRolle.getBezeichnung());
    }


    @Test
    public void testRolleEntityWithEmptyRegel() {
        rolle.setRegel(new HashSet<>());
        entityManager.persistAndFlush(rolle);

        RolleEntity retrievedRolle = entityManager.find(RolleEntity.class, rolle.getId());

        assertNotNull(retrievedRolle);
        assertEquals(0, retrievedRolle.getRegel().size());
    }
}
