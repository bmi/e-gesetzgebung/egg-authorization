// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class AktionEntityTest {

	@Autowired
	private TestEntityManager entityManager;

	private AktionEntity aktion;


	@BeforeEach
	public void setUp() {
		aktion = new AktionEntity();
		aktion.setBezeichnung("TestAktion");

		Set<RegelEntity> regeln = new HashSet<>();
		RegelEntity regel1 = new RegelEntity();
		regel1.setBezeichnung("Regel 1");
		entityManager.persistAndFlush(regel1);

		RegelEntity regel2 = new RegelEntity();
		regel2.setBezeichnung("Regel 2");
		entityManager.persistAndFlush(regel2);

		regeln.add(regel1);
		regeln.add(regel2);

		aktion.setRegel(regeln);
	}


	@Test
	public void testAktionEntitySaveAndRetrieve() {
		entityManager.persistAndFlush(aktion);

		AktionEntity retrievedAktion = entityManager.find(AktionEntity.class, aktion.getId());

		assertNotNull(retrievedAktion);
		assertEquals("TestAktion", retrievedAktion.getBezeichnung());
		assertEquals(2, retrievedAktion.getRegel().size());
	}


	@Test
	public void testAktionEntityWithNullBezeichnung() {
		aktion.setBezeichnung(null);
		entityManager.persistAndFlush(aktion);

		AktionEntity retrievedAktion = entityManager.find(AktionEntity.class, aktion.getId());

		assertNotNull(retrievedAktion);
		assertNull(retrievedAktion.getBezeichnung());
	}


	@Test
	public void testAktionEntityWithEmptyRegel() {
		aktion.setRegel(new HashSet<>());
		entityManager.persistAndFlush(aktion);

		AktionEntity retrievedAktion = entityManager.find(AktionEntity.class, aktion.getId());

		assertNotNull(retrievedAktion);
		assertEquals(0, retrievedAktion.getRegel().size());
	}

}
