// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api;

import de.itzbund.egesetz.bmi.auth.api.rights.AbstractEGesetzRechte;
import org.springframework.stereotype.Component;

@Component
public class EGesetzRechteTestHelper extends AbstractEGesetzRechte {

	@Override
	public String getStatusForResourceType(String ressourcenTyp, String ressourcenId) {
		return "DRAFT";
	}
}
