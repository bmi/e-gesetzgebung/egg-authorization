// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
class ZuordnungGlobaleRolleEntityTest {

	@Autowired
	private TestEntityManager entityManager;

	private ZuordnungGlobaleRolleEntity zuordnungGlobaleRolle;

	
	@BeforeEach
	public void setUp() {
		zuordnungGlobaleRolle = new ZuordnungGlobaleRolleEntity();
		zuordnungGlobaleRolle.setBezeichnung("TestZuordnungGlobaleRolle");
		zuordnungGlobaleRolle.setRessourceId("TestRessourceId");
		zuordnungGlobaleRolle.setRolleId(1L);

		RessourceTypEntity ressourceTyp = new RessourceTypEntity();
		ressourceTyp.setBezeichnung("TestRessourceTypGlobaleRolle");
		entityManager.persistAndFlush(ressourceTyp);

		zuordnungGlobaleRolle.setRessourceTyp(ressourceTyp);

		RolleEntity rolle = new RolleEntity();
		rolle.setBezeichnung("TestGlobaleRolle");
		entityManager.persistAndFlush(rolle);

		zuordnungGlobaleRolle.setGlobaleRolle(rolle);
		entityManager.persistAndFlush(zuordnungGlobaleRolle);
	}


	@Test
	void testZuordnungEntitySaveAndRetrieve() {
		entityManager.persistAndFlush(zuordnungGlobaleRolle);

		ZuordnungGlobaleRolleEntity retrievedZuordnungGlobaleRolle = entityManager.find(ZuordnungGlobaleRolleEntity.class, zuordnungGlobaleRolle.getId());

		assertNotNull(retrievedZuordnungGlobaleRolle);
		assertEquals("TestZuordnungGlobaleRolle", retrievedZuordnungGlobaleRolle.getBezeichnung());
		assertEquals("TestRessourceId", retrievedZuordnungGlobaleRolle.getRessourceId());
		assertEquals(1L, retrievedZuordnungGlobaleRolle.getRolleId());
		assertNotNull(retrievedZuordnungGlobaleRolle.getRessourceTyp());
		assertNotNull(retrievedZuordnungGlobaleRolle.getGlobaleRolle());
	}


	@Test
	void testZuordnungEntityWithNullBezeichnung() {
		zuordnungGlobaleRolle.setBezeichnung(null);
		entityManager.persistAndFlush(zuordnungGlobaleRolle);

		ZuordnungGlobaleRolleEntity retrievedZuordnungGlobaleRolle = entityManager.find(ZuordnungGlobaleRolleEntity.class, zuordnungGlobaleRolle.getId());

		assertNotNull(retrievedZuordnungGlobaleRolle);
		assertNull(retrievedZuordnungGlobaleRolle.getBezeichnung());
	}


	@Test
	void testZuordnungEntityWithNullRessourceTyp() {
		zuordnungGlobaleRolle.setRessourceTyp(null);
		assertThrows(PersistenceException.class, () -> {
			entityManager.persistAndFlush(zuordnungGlobaleRolle);
		});
	}
}
