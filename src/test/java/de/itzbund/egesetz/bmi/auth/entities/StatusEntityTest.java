// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class StatusEntityTest {

    @Autowired
    private TestEntityManager entityManager;

    private StatusEntity status;


    @BeforeEach
    public void setUp() {
        status = new StatusEntity();
        status.setBezeichnung("TestStatus");

        Set<RegelEntity> regeln = new HashSet<>();
        RegelEntity regel1 = new RegelEntity();
        regel1.setBezeichnung("TestRegel1");
        entityManager.persistAndFlush(regel1);

        RegelEntity regel2 = new RegelEntity();
        regel2.setBezeichnung("TestRegel2");
        entityManager.persistAndFlush(regel2);

        regeln.add(regel1);
        regeln.add(regel2);

        status.setRegel(regeln);
        entityManager.persistAndFlush(status);
    }


    @Test
    public void testStatusEntitySaveAndRetrieve() {
        entityManager.persistAndFlush(status);

        StatusEntity retrievedStatus = entityManager.find(StatusEntity.class, status.getId());

        assertNotNull(retrievedStatus);
        assertEquals("TestStatus", retrievedStatus.getBezeichnung());
        assertEquals(2, retrievedStatus.getRegel().size());
    }


    @Test
    public void testStatusEntityWithNullBezeichnung() {
        status.setBezeichnung(null);
        entityManager.persistAndFlush(status);

        StatusEntity retrievedStatus = entityManager.find(StatusEntity.class, status.getId());

        assertNotNull(retrievedStatus);
        assertNull(retrievedStatus.getBezeichnung());
    }


    @Test
    public void testStatusEntityWithEmptyRegel() {
        status.setRegel(new HashSet<>());
        entityManager.persistAndFlush(status);

        StatusEntity retrievedStatus = entityManager.find(StatusEntity.class, status.getId());

        assertNotNull(retrievedStatus);
        assertEquals(0, retrievedStatus.getRegel().size());
    }
}
