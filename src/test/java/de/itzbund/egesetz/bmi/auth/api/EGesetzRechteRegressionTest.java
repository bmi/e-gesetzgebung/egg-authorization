// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

// Originalverhalten für H2 und Test-Umgebung wollen wir nicht
@SpringBootTest
@Sql("/data-x.sql")
class EGesetzRechteRegressionTest {

    private static final String RESOURCE_TYPE = "DOKUMENTE";
    private static final String BENUTZER_ID = "benutzerId";

    @Autowired
    private EGesetzRechteTestHelper accessRights;

    @Test
    void testAddRessourcePermissionTwiceWillFail() {

        // Act
        accessRights.addRessourcePermission(BENUTZER_ID, RESOURCE_TYPE, "ressource_id", "ERSTELLER");
        boolean actual = accessRights.addRessourcePermission(BENUTZER_ID, RESOURCE_TYPE, "ressource_id", "ERSTELLER");

        // Assert
        assertThat(actual).isFalse();
    }

}
