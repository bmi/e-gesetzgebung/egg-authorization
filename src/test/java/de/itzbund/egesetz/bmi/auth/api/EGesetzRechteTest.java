// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api;

import de.itzbund.egesetz.bmi.auth.api.dto.PermissionDTO;
import de.itzbund.egesetz.bmi.auth.api.dto.StatusResponseDTO;
import de.itzbund.egesetz.bmi.auth.api.enums.GetPermissionOptionType;
import de.itzbund.egesetz.bmi.auth.api.enums.UpdateResultType;
import de.itzbund.egesetz.bmi.auth.api.utils.EGesetzRechteUtils;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelAdministrationEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import de.itzbund.egesetz.bmi.auth.repositories.AktionenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RegelnAdministrationRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RegelnRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RessourceTypenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RollenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.ZuordnungenRepository;
import de.itzbund.egesetz.bmi.user.entities.NutzerEntity;
import de.itzbund.egesetz.bmi.user.repositories.NutzerEntityRepository;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class EGesetzRechteTest {

    private static final String AKTION = "LESEN";
    private static final String RESOURCE_TYPE = "DOKUMENTE";
    private static final String ROLLE_1 = "ERSTELLER";
    private static final String ROLLE_2 = "GUTACHTER";
    private static final String ROLLE_3 = "FEDERFUEHRER";
    private static final String BENUTZER_GID = "benutzerId";
    private static final String RESOURCEN_ID_1 = "resourcenId1";
    private static final String RESOURCEN_ID_2 = "resourcenId2";
    private static final String VERTRETER_ID = "vertreterId";
    private final static Instant now = Instant.now();
    private final List<AktionEntity> aktionen = new ArrayList<>();
    private final List<RessourceTypEntity> ressourceTypen = new ArrayList<>();
    private final List<RolleEntity> rollen = new ArrayList<>();
    private final List<ZuordnungEntity> zuordnungen = new ArrayList<>();
    private final List<RegelEntity> regeln = new ArrayList<>();
    @MockBean
    private RegelnRepository regelnRepository;
    @MockBean
    private RessourceTypenRepository ressourceTypenRepository;
    @MockBean
    private RollenRepository rollenRepository;
    @MockBean
    private ZuordnungenRepository zuordnungenRepository;
    @MockBean
    private AktionenRepository aktionenRepository;
    @Autowired
    private EGesetzRechteTestHelper accessRights;

	@MockBean
	private RegelnAdministrationRepository regelnAdministrationRepository;
	@MockBean
	private NutzerEntityRepository nutzerEntityRepository;

    private static boolean equals(String s1, String s2) {
        return StringUtils.equalsIgnoreCase(StringUtils.trim(s1), StringUtils.trim(s2));
    }

    private AktionEntity getAktion(String bezeichnung) {
        return aktionen.stream()
            .filter(e -> equals(e.getBezeichnung(), bezeichnung))
            .findAny()
            .orElseThrow(() -> new IllegalArgumentException("Aktion '" + bezeichnung + "' nicht gefunden!"));
    }

    private RessourceTypEntity getRessourceTyp(String bezeichnung) {
        return ressourceTypen.stream()
            .filter(e -> equals(e.getBezeichnung(), bezeichnung))
            .findAny()
            .orElseThrow(() -> new IllegalArgumentException("Ressource-Typ '" + bezeichnung + "' nicht gefunden!"));
    }

    private RolleEntity getRolle(String bezeichnung) {
        return rollen.stream()
            .filter(e -> equals(e.getBezeichnung(), bezeichnung))
            .findAny()
            .orElseThrow(() -> new IllegalArgumentException("Rolle '" + bezeichnung + "' nicht gefunden!"));
    }


    @BeforeEach
    void init() {
        reset(zuordnungenRepository);
        reset(ressourceTypenRepository);
        reset(rollenRepository);

        aktionen.add(AktionEntity.builder().bezeichnung(AKTION).build());
        regeln.add(RegelEntity.builder().aktion(getAktion("LESEN")).build());
        ressourceTypen.add(RessourceTypEntity.builder().id(201L).bezeichnung(RESOURCE_TYPE).build());

        rollen.add(RolleEntity.builder().id(101L).bezeichnung(ROLLE_1).build());
        rollen.add(RolleEntity.builder().id(102L).bezeichnung(ROLLE_2).build());
        rollen.add(RolleEntity.builder().id(103L).bezeichnung(ROLLE_3).build());

        zuordnungen.add(ZuordnungEntity.builder()
            .id(1L)
            .benutzerId(BENUTZER_GID)
            .bezeichnung("Zuordnung 1")
            .ressourceTyp(getRessourceTyp("DOKUMENTE"))
            .rolle(getRolle("ERSTELLER"))
            .deaktiviert(true)
            .ressourceId(null)
            .build());
        zuordnungen.add(ZuordnungEntity.builder()
            .id(2L)
            .benutzerId(VERTRETER_ID)
            .bezeichnung("Zuordnung 2")
            .ressourceTyp(getRessourceTyp("DOKUMENTE"))
            .rolle(getRolle("GUTACHTER"))
            .deaktiviert(true)
            .fallbackZuordnung(1L)
            .ressourceId(RESOURCEN_ID_2)
            .build());
        zuordnungen.add(ZuordnungEntity.builder()
            .id(3L)
            .benutzerId(UUID.randomUUID().toString())
            .bezeichnung("Zuordnung 3")
            .ressourceTyp(getRessourceTyp("DOKUMENTE"))
            .befristung(now)
            .rolle(getRolle("GUTACHTER"))
            .fallbackZuordnung(2L)
            .ressourceId(RESOURCEN_ID_1)
            .build());
        zuordnungen.add(ZuordnungEntity.builder()
            .id(4L)
            .benutzerId(UUID.randomUUID().toString())
            .bezeichnung("Zuordnung 4 - empty object to validate robustness")
            .ressourceTyp(null)
            .befristung(null)
            .rolle(null)
            .fallbackZuordnung(null)
            .ressourceId(null)
            .build());

        when(ressourceTypenRepository.findAll()).thenReturn(ressourceTypen);

        when(ressourceTypenRepository.findByBezeichnung("DOKUMENTE")).thenReturn(Optional.of(getRessourceTyp("DOKUMENTE")));

        when(rollenRepository.findAll()).thenReturn(rollen);

        when(rollenRepository.findByBezeichnung(anyString())).thenReturn(Optional.of(getRolle("ERSTELLER")));

        when(regelnRepository.findByRessourceTypAndRolleAndStatusIn(any(), any(), any())).thenReturn(Optional.of(regeln));

        when(aktionenRepository.findByBezeichnung("LESEN")).thenReturn(Optional.of(getAktion("LESEN")));

        when(zuordnungenRepository.findByRessourceTypAndRessourceIdAndBenutzerIdAndDeaktiviert(any(), anyString(),
            anyString(), anyBoolean())).thenReturn(Optional.of(zuordnungen));

        when(zuordnungenRepository.save(any()))
            .thenAnswer(i -> i.getArguments()[0]);

		when(regelnAdministrationRepository.findByRolleAndAktionAndAngefragteRolleId(any(), any(), any())).thenReturn(Optional.of(RegelAdministrationEntity
			.builder().angefragteRolleId(1L).rolle(getRolle("ERSTELLER")).aktion(getAktion("LESEN")).build()));
    }


    @Test
    void whenAllDokumentsForRoleAreRequested_theyShouldBeProvided() {
        // Arrange
        when(zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRolle(any(), anyString(), any())).thenReturn(
            Optional.of(zuordnungen));

        // Act
        List<String> actual1 = accessRights.getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(BENUTZER_GID, ROLLE_1, "DOKUMENTE");
        List<String> actual2 = accessRights.getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(BENUTZER_GID, ROLLE_2, "DOKUMENTE");

        // Assert
        assertThat(actual1).hasSize(2);
        assertThat(actual2).hasSize(2);

        assertThat(actual2.get(0)).isEqualTo(RESOURCEN_ID_2);
    }


    @Test
    void whenGetPermissionWithCorrectValues_thenItShouldReturnPresent() {
        //test 1: no result
        when(zuordnungenRepository.findAll(nullable(Specification.class), any(Pageable.class))).thenReturn(
            new PageImpl<>(new ArrayList<>()));
        Optional<PermissionDTO> result1 = accessRights.getPermission(BENUTZER_GID, List.of(AKTION), RESOURCE_TYPE, RESOURCEN_ID_1,
            GetPermissionOptionType.SLOW_CHECK_STATUS_GET_AKTIONEN);
        assertTrue(result1.isEmpty());

        //test 2: one result
        when(zuordnungenRepository.findAll(nullable(Specification.class), any(Pageable.class))).thenReturn(
            new PageImpl<>(zuordnungen));
        when(aktionenRepository.findAll(nullable(Specification.class))).thenReturn(aktionen);
        Optional<PermissionDTO> result2 = accessRights.getPermission(BENUTZER_GID, List.of(AKTION), RESOURCE_TYPE, RESOURCEN_ID_1,
            GetPermissionOptionType.SLOW_CHECK_STATUS_GET_AKTIONEN);
        assertTrue(result2.isPresent());
        assertEquals(VERTRETER_ID, result2.get().getBenutzerId());
    }


    @Test
    void failGetPermissionByRessourceType() {
        // Arrange
        when(ressourceTypenRepository.findByBezeichnung(anyString())).thenReturn(Optional.empty());

        // Act
        Exception ex = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermission(BENUTZER_GID, List.of(AKTION), "RESOURCE_TYPE", RESOURCEN_ID_1,
                GetPermissionOptionType.SLOW_CHECK_STATUS_GET_AKTIONEN));

        // Assert
        org.hamcrest.MatcherAssert.assertThat(ex.getMessage(), containsString("Ressource-Typ 'RESOURCE_TYPE' wurde nicht gefunden"));
    }


    @Test
    void failGetPermissionByRollen() {
        // Arrange
        when(zuordnungenRepository.findByRessourceTypAndRessourceIdAndBenutzerIdAndDeaktiviert(any(), anyString(),
            anyString(), anyBoolean())).thenReturn(Optional.empty());
        when(zuordnungenRepository.findAll(nullable(Specification.class), any(Pageable.class))).thenReturn(
            new PageImpl<ZuordnungEntity>(new ArrayList<>()));

        // Act
        Optional<PermissionDTO> result = accessRights.getPermission(BENUTZER_GID, List.of(AKTION), RESOURCE_TYPE, RESOURCEN_ID_1,
            GetPermissionOptionType.SLOW_CHECK_STATUS_GET_AKTIONEN);

        // Assert
        assertFalse(result.isPresent());
    }


    @Test
    void failGetPermissionByAktionen() {
        // Arrange
        when(regelnRepository.findByRessourceTypAndRolleAndStatusIn(any(), any(),
            any())).thenReturn(Optional.empty());
        when(zuordnungenRepository.findAll(nullable(Specification.class), any(Pageable.class))).thenReturn(
            new PageImpl<ZuordnungEntity>(new ArrayList<>()));

        // Act
        Optional<PermissionDTO> result = accessRights.getPermission(BENUTZER_GID, List.of(AKTION), RESOURCE_TYPE, RESOURCEN_ID_1,
            GetPermissionOptionType.SLOW_CHECK_STATUS_GET_AKTIONEN);

        // Assert
        assertFalse(result.isPresent());
    }

    @Test
    void testAlleRessourcenFuerBenutzerUndRessourceTypAndAktionenGet() {
        // Arrange
        when(zuordnungenRepository.findAll(nullable(Specification.class))).thenReturn(zuordnungen);

        // Act
        List<PermissionDTO> result = accessRights.getAlleRessourcenFuerBenutzerUndRessourceTypAndAktionen(BENUTZER_GID, RESOURCE_TYPE, List.of(AKTION));

        // Assert
        assertEquals(2, result.size());
        result.forEach(dto -> assertNotNull(dto.getRessourceId()));
    }


    @Test
    void whenIsAllowedWithCorrectValues_thenItShouldReturnTrue() {
        // Arrange
        when(aktionenRepository.findAll(nullable(Specification.class))).thenReturn(aktionen);

        // Act
        boolean actual = accessRights.isAllowed(BENUTZER_GID, AKTION, RESOURCE_TYPE, RESOURCEN_ID_2);

        // Assert
        assertThat(actual).isTrue();
    }


    @Test
    void failIsAllowedByRessourceType() {
        // Arrange
        when(ressourceTypenRepository.findByBezeichnung(anyString())).thenReturn(Optional.empty());

        // Act
        boolean actual = accessRights.isAllowed(BENUTZER_GID, AKTION, "RESOURCE_TYPE", RESOURCEN_ID_1);

        // Assert
        assertThat(actual).isFalse();
    }


    @Test
    void failIsAllowedByRollen() {
        // Arrange
        when(zuordnungenRepository.findByRessourceTypAndRessourceIdAndBenutzerIdAndDeaktiviert(any(), anyString(),
            anyString(), anyBoolean())).thenReturn(Optional.empty());

        // Act
        boolean actual = accessRights.isAllowed(BENUTZER_GID, AKTION, RESOURCE_TYPE, RESOURCEN_ID_1);

        // Assert
        assertThat(actual).isFalse();
    }


    @Test
    void failIsAllowedByAktionen() {
        // Arrange
        when(regelnRepository.findByRessourceTypAndRolleAndStatusIn(any(), any(),
            any())).thenReturn(Optional.empty());

        // Act
        boolean actual = accessRights.isAllowed(BENUTZER_GID, AKTION, RESOURCE_TYPE, RESOURCEN_ID_1);

        // Assert
        assertThat(actual).isFalse();
    }

    @Test
    void testIsDeaktivated_ThanNothingIsAllowed() {

        // Arrange
        when(zuordnungenRepository.findByRessourceTypAndRessourceIdAndBenutzerIdAndDeaktiviert(any(), anyString(),
            anyString(), anyBoolean())).thenReturn(Optional.of(zuordnungen));
        when(regelnRepository.findByRessourceTypAndRolleAndStatusIn(any(), any(),
            any())).thenReturn(Optional.of(regeln));

        // Act
        boolean actual = accessRights.isAllowed(BENUTZER_GID, AKTION, RESOURCE_TYPE, RESOURCEN_ID_1);

    }

    @Test
    void testUpdateRessourcePermission_NotFound() {
        // Arrange

        // Act
        UpdateResultType result = accessRights.updateRessourcePermission(BENUTZER_GID, RESOURCE_TYPE,
            "ressource_id", getRolle("ERSTELLER").getBezeichnung());

        // Assert
        assertThat(result).isEqualTo(UpdateResultType.NOT_FOUND);
    }

    @Test
    void testUpdateRessourcePermission_Updated() {
        // Arrange
        // construct example with multiple roles for this user (should not happen, but code and database do not fully prevent this situation)
        ZuordnungEntity zuordnungEntity1 = ZuordnungEntity.builder()
            .benutzerId(BENUTZER_GID)
            .ressourceTyp(getRessourceTyp(RESOURCE_TYPE))
            .ressourceId("ressource_id")
            .rolle(getRolle("GUTACHTER"))
            .build();
        ZuordnungEntity zuordnungEntity2 = ZuordnungEntity.builder()
            .benutzerId(BENUTZER_GID)
            .ressourceTyp(getRessourceTyp(RESOURCE_TYPE))
            .ressourceId("ressource_id")
            .rolle(getRolle("ERSTELLER"))
            .build();
        ZuordnungEntity zuordnungEntity3 = ZuordnungEntity.builder()
            .benutzerId(BENUTZER_GID)
            .ressourceTyp(getRessourceTyp(RESOURCE_TYPE))
            .ressourceId("ressource_id")
            .rolle(null)
            .build();
        when(zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRessourceId(any(), any(), any())).thenReturn(
            List.of(zuordnungEntity1, zuordnungEntity2, zuordnungEntity3));

        // Act
        UpdateResultType result = accessRights.updateRessourcePermission(BENUTZER_GID, RESOURCE_TYPE,
            "ressource_id", getRolle("FEDERFUEHRER").getBezeichnung());

        // Assert
        assertThat(result).isEqualTo(UpdateResultType.UPDATED);
    }

    @Test
    void testUpdateRessourcePermission_NotChanged() {
        // Arrange
        ZuordnungEntity zuordnungEntity = ZuordnungEntity.builder()
            .benutzerId(BENUTZER_GID)
            .ressourceTyp(getRessourceTyp(RESOURCE_TYPE))
            .ressourceId("ressource_id")
            .rolle(getRolle("ERSTELLER"))
            .build();
        when(zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRessourceId(any(), any(), any())).thenReturn(List.of(zuordnungEntity));

        // Act
        UpdateResultType result = accessRights.updateRessourcePermission(BENUTZER_GID, RESOURCE_TYPE,
            "ressource_id", getRolle("ERSTELLER").getBezeichnung());

        // Assert
        assertThat(result).isEqualTo(UpdateResultType.NOT_CHANGED);
    }

    @Test
    void testUpdateRessourcePermission_Fail() {
        // Arrange
        ZuordnungEntity zuordnungEntity = ZuordnungEntity.builder()
            .benutzerId(BENUTZER_GID)
            .ressourceTyp(getRessourceTyp(RESOURCE_TYPE))
            .ressourceId("ressource_id")
            .rolle(getRolle("FEDERFUEHRER"))
            .build();
        when(zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRessourceId(any(), any(), any())).thenReturn(List.of(zuordnungEntity));

        // Act
        assertThrows(IllegalArgumentException.class, () -> accessRights.updateRessourcePermission(BENUTZER_GID, RESOURCE_TYPE,
            "ressource_id", getRolle("ERSTELLER").getBezeichnung()));
    }


    @Test
    void testAddRessourcePermissionSuccessfully() {
        // Arrange

        // Act
        boolean actual = accessRights.addRessourcePermission(BENUTZER_GID, RESOURCE_TYPE, "ressource_id", getRolle("ERSTELLER").getBezeichnung());

        // Assert
        assertThat(actual).isTrue();
    }


    @Test
    void testAddRessourcePermissionWillFailWithException() {

        // SQLIntegrityConstraintViolationException wird über den Regressionstest getestet
        // Arrange
        when(zuordnungenRepository.save(any()))
            .thenThrow(new DataIntegrityViolationException("Duplicate entry ..."));

        boolean actual = accessRights.addRessourcePermission(BENUTZER_GID, RESOURCE_TYPE, "ressource_id", getRolle("ERSTELLER").getBezeichnung());
        assertFalse(actual);
    }


    @Test
    void testRemoveRessourcePermission1() {
        // Arrange
        doAnswer((i) -> {
            return null;
        }).when(zuordnungenRepository).deleteByRessourceIdAndRessourceTypAndRolle(anyString(), any(), any());

        // Act
        boolean actual = accessRights.removeRessourcePermission(BENUTZER_GID, RESOURCE_TYPE, "ressource_id", getRolle("ERSTELLER").getBezeichnung());

        // Assert
        assertThat(actual).isFalse();
    }


    @Test
    void testRemoveRessourcePermission2() {
        // Arrange
        when(ressourceTypenRepository.findByBezeichnung(anyString())).thenReturn(Optional.of(RessourceTypEntity.builder().build()));
        when(rollenRepository.findByBezeichnung(anyString())).thenReturn(Optional.of(RolleEntity.builder().build()));
        doAnswer((i) -> {
            return null;
        }).when(zuordnungenRepository).deleteByRessourceIdAndRessourceTypAndRolle(anyString(), any(), any());

        // Act
        boolean actual = accessRights.removeRessourcePermission(RESOURCE_TYPE, "ressource_id", getRolle("ERSTELLER").getBezeichnung());

        // Assert
        assertThat(actual).isTrue();
    }


    @Test
    void testRetrieveAllUsersForGivenRessourceAndRole() {
        // Act
        List<String> actual = accessRights.getAlleBenutzerIdsByRessourceIdAndRolle(UUID.randomUUID().toString(),
            "LESER");

        // Assert
        assertThat(actual).isEmpty();
    }

    @Test
    void testGetPermissionsByRessourceIdAndRolleAndBenutzerIdFail() {
        // Arrange
        when(zuordnungenRepository.createSpecification(any(), any(), any(), any(), any())).thenReturn(mock(Specification.class));
        when(zuordnungenRepository.findAll(any(Specification.class))).thenReturn(zuordnungen);

        // Act
        Optional<Collection<String>> ressourcenIds = Optional.of(List.of("1", "2"));
        Optional<Collection<String>> benutzerIds = Optional.of(List.of("1", "2"));
        Optional<Collection<String>> rollen = Optional.of(List.of("ERSTELLER"));
        Optional<Collection<String>> aktionen = Optional.of(List.of("LESEN"));
        Optional<Collection<String>> ressourceTypen = Optional.of(List.of("DOKUMENTE"));

        //test 1: illegal option type throws error
        Assertions.assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(benutzerIds, aktionen, ressourceTypen, ressourcenIds, rollen, null));
    }

    @Test
    void testGetPermissionsByRessourceIdAndRolleAndBenutzerId() {
        // Arrange
        when(zuordnungenRepository.createSpecification(any(), any(), any(), any(), any())).thenReturn(mock(Specification.class));
        when(zuordnungenRepository.findAll(any(Specification.class))).thenReturn(zuordnungen);

        // Act
        Optional<Collection<String>> ressourcenIds = Optional.of(List.of("1", "2"));
        Optional<Collection<String>> benutzerIds = Optional.of(List.of("1", "2"));
        Optional<Collection<String>> rollen = Optional.of(List.of("ERSTELLER"));
        Optional<Collection<String>> aktionen = Optional.of(List.of("LESEN"));
        Optional<Collection<String>> ressourceTypen = Optional.of(List.of("DOKUMENTE"));

        List<PermissionDTO> result = Assertions.assertDoesNotThrow(
            () -> accessRights.getPermissions(benutzerIds, aktionen, ressourceTypen, ressourcenIds, rollen,
                GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        // Assert
        assertNotNull(result);
        assertEquals(zuordnungen.size(), result.size());

        // Verify
        verify(zuordnungenRepository).createSpecification(notNull(), notNull(), notNull(), notNull(), notNull());
        verify(zuordnungenRepository).findAll(any(Specification.class));
    }


    @Test
    void testGetAlleRessourcenFuerBenutzerUndRessourceTyp() {
        // Arrange
        when(ressourceTypenRepository.findByBezeichnung(anyString())).thenReturn(
            Optional.of(RessourceTypEntity.builder().bezeichnung(RESOURCE_TYPE).build()));
        when(zuordnungenRepository.findByRessourceTypAndBenutzerId(any(), anyString())).thenReturn(
            Optional.of(zuordnungen));

        // Act
        List<String> actual = accessRights.getAlleRessourcenFuerBenutzerUndRessourceTyp(BENUTZER_GID, RESOURCE_TYPE);

        // Assert
        assertThat(actual).asList().hasSize(2);
    }


    @Test
    void testGetAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp() {
        // Arrange
        when(ressourceTypenRepository.findByBezeichnung(anyString())).thenReturn(
            Optional.of(RessourceTypEntity.builder().bezeichnung(RESOURCE_TYPE).build()));
        when(zuordnungenRepository.findByRessourceTypAndBenutzerIdAndRolle(any(), anyString(), any())).thenReturn(
            Optional.of(zuordnungen));

        // Act
        List<String> actual = accessRights.getAlleRessourcenFuerBenutzerUndRolleUndRessourceTyp(BENUTZER_GID,
            RESOURCE_TYPE, ROLLE_1);

        // Assert
        assertThat(actual).asList().hasSize(2);
    }

    @Test
    void testResourcePermissionInputValidate() {
        String benutzerId = "user-gid";
        String ressourcenTyp = getRessourceTyp("DOKUMENTE").getBezeichnung();
        String ressourceId = UUID.randomUUID().toString();
        String rolle = getRolle("ERSTELLER").getBezeichnung();

        //test 1: Benutzer-ID is empty or blank
        IllegalArgumentException ex1 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.addRessourcePermission(null, ressourcenTyp, ressourceId, rolle));
        org.hamcrest.MatcherAssert.assertThat(ex1.getMessage(), containsString("Benutzer-ID"));
        org.hamcrest.MatcherAssert.assertThat(ex1.getMessage(), not(containsString("Ressource-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex1.getMessage(), not(containsString("Rolle")));
        org.hamcrest.MatcherAssert.assertThat(ex1.getMessage(), not(containsString("Ressourcen-Typ")));

        //test 2: Ressource-ID is empty or blank
        IllegalArgumentException ex2 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.addRessourcePermission(benutzerId, ressourcenTyp, null, rolle));
        org.hamcrest.MatcherAssert.assertThat(ex2.getMessage(), containsString("Ressourcen-ID"));
        org.hamcrest.MatcherAssert.assertThat(ex2.getMessage(), not(containsString("Benutzer-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex2.getMessage(), not(containsString("Rolle")));
        org.hamcrest.MatcherAssert.assertThat(ex2.getMessage(), not(containsString("Ressourcen-Typ")));

        //test 3: Rolle is empty or blank
        IllegalArgumentException ex3 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.addRessourcePermission(benutzerId, ressourcenTyp, ressourceId, null));
        org.hamcrest.MatcherAssert.assertThat(ex3.getMessage(), not(containsString("Ressource-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex3.getMessage(), not(containsString("Benutzer-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex3.getMessage(), containsString("Rolle"));
        org.hamcrest.MatcherAssert.assertThat(ex3.getMessage(), not(containsString("Ressourcen-Typ")));

        //test 4: Rolle is empty or blank
        IllegalArgumentException ex4 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.addRessourcePermission(benutzerId, null, ressourceId, rolle));
        org.hamcrest.MatcherAssert.assertThat(ex4.getMessage(), not(containsString("Ressource-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex4.getMessage(), not(containsString("Benutzer-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex4.getMessage(), not(containsString("Rolle")));
        org.hamcrest.MatcherAssert.assertThat(ex4.getMessage(), containsString("Ressourcen-Typ"));

        //test 5: Rolle is invalid
        IllegalArgumentException ex5 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.addRessourcePermission(benutzerId, ressourcenTyp, ressourceId, "unbekannte Rolle"));
        org.hamcrest.MatcherAssert.assertThat(ex5.getMessage(), not(containsString("Ressource-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex5.getMessage(), not(containsString("Benutzer-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex5.getMessage(), not(containsString("Ressourcen-Typ")));
        org.hamcrest.MatcherAssert.assertThat(ex5.getMessage(), containsString("unbekannte Rolle"));

        //test 6: Ressourcen-Typ is invalid
        when(ressourceTypenRepository.findByBezeichnung("unbekannter Ressourcen-Typ")).thenReturn(Optional.empty());

        IllegalArgumentException ex6 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.addRessourcePermission(benutzerId, "unbekannter Ressourcen-Typ", ressourceId, rolle));
        org.hamcrest.MatcherAssert.assertThat(ex6.getMessage(), not(containsString("Ressource-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex6.getMessage(), not(containsString("Benutzer-ID")));
        org.hamcrest.MatcherAssert.assertThat(ex6.getMessage(), not(containsString("Rolle")));
        org.hamcrest.MatcherAssert.assertThat(ex6.getMessage(), containsString("unbekannter Ressourcen-Typ"));
    }

    @Test
    void testPermissionsByRessourceIdAndRolleAndBenutzerIdValidate() {
        Optional<Collection<String>> ressourcenIds = Optional.of(List.of("1", "2"));
        Optional<Collection<String>> benutzerIds = Optional.of(List.of("1", "2"));
        Optional<Collection<String>> rollen = Optional.of(List.of("ERSTELLER"));
        Optional<Collection<String>> aktionen = Optional.of(List.of("LESEN"));
        Optional<Collection<String>> ressourceTypen = Optional.of(List.of("DOKUMENTE"));

        when(zuordnungenRepository.findAll(nullable(Specification.class), any(Pageable.class))).thenReturn(
            new PageImpl<>(new ArrayList<>()));

        //test 1: all parameters are empty -> ok
        Assertions.assertDoesNotThrow(
            () -> accessRights.getPermissions(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
                GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 2: all parameters are set -> ok
        Assertions.assertDoesNotThrow(() -> accessRights.getPermissions(benutzerIds, aktionen, ressourceTypen, ressourcenIds, rollen,
            GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 3: ressource ids are empty -> error
        IllegalArgumentException ex1 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.of(Stream.of(" ", null).collect(Collectors.toList())),
                Optional.empty(), GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 5: rollen are empty -> error
        IllegalArgumentException ex2 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.of(Stream.of((String) null).collect(Collectors.toList())), GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 6: rollen are invalid -> error
        IllegalArgumentException ex3 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.of(Stream.of("UNKNOWN_ROLE").collect(Collectors.toList())), GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 7: aktionen are empty -> error
        IllegalArgumentException ex4 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(Optional.empty(),
                Optional.of(Stream.of((String) null).collect(Collectors.toList())),
                Optional.empty(), Optional.empty(), Optional.empty(), GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 8: aktionen are invalid -> error
        IllegalArgumentException ex5 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(Optional.empty(),
                Optional.of(Stream.of("UNKNOWN_ACTION").collect(Collectors.toList())),
                Optional.empty(), Optional.empty(), Optional.empty(), GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 7: ressource types are empty -> error
        IllegalArgumentException ex6 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(Optional.empty(), Optional.empty(),
                Optional.of(Stream.of((String) null).collect(Collectors.toList())),
                Optional.empty(), Optional.empty(), GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 8: ressource types are invalid -> error
        IllegalArgumentException ex7 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(Optional.empty(), Optional.empty(),
                Optional.of(Stream.of("UNKNOWN_RESSOURCE_TYPE").collect(Collectors.toList())),
                Optional.empty(), Optional.empty(), GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));

        //test 11: benutzer ids are empty -> error
        IllegalArgumentException ex8 = assertThrows(IllegalArgumentException.class,
            () -> accessRights.getPermissions(Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.of(Stream.of(" ", null).collect(Collectors.toList())),
                Optional.empty(), GetPermissionOptionType.FAST_NO_STATUS_NO_AKTIONEN));
    }

    @Test
    void testDuplicateCreate() {
        String benutzerId = "user-gid";
        String ressourcenTyp = getRessourceTyp("DOKUMENTE").getBezeichnung();
        String ressourceId = UUID.randomUUID().toString();
        String rolle = getRolle("ERSTELLER").getBezeichnung();

        //test 1: everything is fine
        Boolean result1 = Assertions.assertDoesNotThrow(() -> accessRights.addRessourcePermission(benutzerId, ressourcenTyp, ressourceId, rolle));
        assertTrue(result1);

        //test 2: database not found, table not found or any other db related problem
        when(zuordnungenRepository.save(any())).thenThrow(
            new org.hibernate.exception.JDBCConnectionException("database down", null));

        assertThrows(org.hibernate.exception.JDBCConnectionException.class,
            () -> accessRights.addRessourcePermission(benutzerId, ressourcenTyp, ressourceId, rolle));

        //test 3: unique constraint violation because permission already exists -> ok
        reset(zuordnungenRepository);
        when(zuordnungenRepository.save(any())).thenThrow(
            new DataIntegrityViolationException("something went wrong",
                new java.sql.SQLIntegrityConstraintViolationException("Duplicate entry for key 'zuordnungen.UK_zuordnungen_eintraege'")));

        Boolean result3 = Assertions.assertDoesNotThrow(() -> accessRights.addRessourcePermission(benutzerId, ressourcenTyp, ressourceId, rolle));
        assertFalse(result3);
    }

    @Test
    void StatusResponsePick() {
        String ressourceId1 = "ressourceId1";
        String ressourceId2 = "ressourceId2";
        String ressourceId3 = "ressourceId3";
        ZuordnungEntity zuordnung = ZuordnungEntity.builder()
            .ressourceId(ressourceId2)
            .build();

        List<StatusResponseDTO> list = List.of(
            StatusResponseDTO.builder()
                .ressourcenId(ressourceId1)
                .status("status1")
                .build(),
            StatusResponseDTO.builder()
                .ressourcenId(ressourceId2)
                .status("status2")
                .build(),
            StatusResponseDTO.builder()
                .ressourcenId(ressourceId2)
                .status("status3")
                .build(),
            StatusResponseDTO.builder()
                .ressourcenId(ressourceId3)
                .status("status4")
                .build()
        );

        //test 1: status response list is empty -> Optional.empty()
        Optional<StatusResponseDTO> result1 = Assertions.assertDoesNotThrow(() -> EGesetzRechteUtils.pickStatusResponse(zuordnung, new ArrayList<>()));
        assertTrue(result1.isEmpty());

        //test 2: status response list is ambigious -> return first entry
        Optional<StatusResponseDTO> result2 = assertDoesNotThrow(() -> EGesetzRechteUtils.pickStatusResponse(zuordnung, list));
        assertTrue(result2.isPresent());
        assertThat(result2.get().getStatus()).isEqualTo("status2");

        //test 2: status response list is unique -> return first entry
        zuordnung.setRessourceId(ressourceId3);
        Optional<StatusResponseDTO> result3 = assertDoesNotThrow(() -> EGesetzRechteUtils.pickStatusResponse(zuordnung, list));
        assertTrue(result3.isPresent());
        assertThat(result3.get().getStatus()).isEqualTo("status4");
    }

	@Test
	void whenIsAllowedAdministrationWithCorrectValues_thenItShouldReturnTrue() {
		// Arrange
		when(aktionenRepository.findAll(nullable(Specification.class))).thenReturn(aktionen);
		when(nutzerEntityRepository.findByGid(any())).thenReturn(Optional.of(NutzerEntity.builder().gid(BENUTZER_GID).rolle(getRolle(ROLLE_1)).build()));

		// Act
		boolean actual = accessRights.isAllowedAdministration(BENUTZER_GID, AKTION, ROLLE_1);

		// Assert
		assertThat(actual).isTrue();
	}

}
