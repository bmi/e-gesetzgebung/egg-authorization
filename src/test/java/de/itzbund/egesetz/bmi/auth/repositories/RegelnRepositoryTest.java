// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.api.rights.EGesetzRechte;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.StatusEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//@SqlGroup({
//        @Sql(scripts = "/data.sql")
//})
@SpringBootTest
class RegelnRepositoryTest {

	@Autowired
	private AktionenRepository aktionenRepository;
	@Autowired
	private RessourceTypenRepository ressourceTypenRepository;
	@Autowired
	private RollenRepository rollenRepository;
	@Autowired
	private StatusRepository statusRepository;

	@Autowired
	private EGesetzRechte editorRollenUndRechte;

	@Autowired
	private RegelnRepository regelnRepository;

	private List<AktionEntity> aktionen = null;
	private List<RessourceTypEntity> ressourceTypen = null;
	private List<RolleEntity> rollen = null;
	private List<StatusEntity> status = null;
	@Autowired
	@Qualifier("authDataSource")
	private ZuordnungenRepository zuordnungenRepository;


	@BeforeEach
	void init() {
		aktionen = aktionenRepository.findAll();
		ressourceTypen = ressourceTypenRepository.findAll();
		rollen = rollenRepository.findAll();
		status = statusRepository.findAll();
	}

	private static boolean equals(String s1, String s2) {
		return StringUtils.equalsIgnoreCase(StringUtils.trim(s1), StringUtils.trim(s2));
	}

	private AktionEntity getAktion(String bezeichnung) {
		return aktionen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Aktion '" + bezeichnung + "' nicht gefunden!"));
	}

	private RessourceTypEntity getRessourceTyp(String bezeichnung) {
		return ressourceTypen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Ressource-Typ '" + bezeichnung + "' nicht gefunden!"));
	}

	private RolleEntity getRolle(String bezeichnung) {
		return rollen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Rolle '" + bezeichnung + "' nicht gefunden!"));
	}

	private StatusEntity getStatus(String bezeichnung) {
		return status.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Status '" + bezeichnung + "' nicht gefunden!"));
	}


	@Test
	void checkGetAlleDokumenteWithPermissionsbyBenutzerId() {
		// Arrange
		String regelName = "TestRegel02";
		String regelName2 = "TestRegel03";

		String benutzerId = "12345678-1111-1111-8eb1-774e4fa50b16";

		String bezeichnung1 = "DOKUMENTE12345678-1234-4445-8eb1-774e4fa50b16ERST";
		String ressourceId1 = "12345678-1234-402c-b28a-dcc4dc64b374";

		String bezeichnung2 = "DOKUMENTE12345678-2234-4445-8eb1-774e4fa50b16ERST";
		String ressourceId2 = "12345678-2234-402c-b28a-dcc4dc64b374";

		String bezeichnung3 = "DOKUMENTE12345678-3234-4445-8eb1-774e4fa50b16ERST";
		String ressourceId3 = "12345678-3234-402c-b28a-dcc4dc64b374";

		RegelEntity regelEntity = RegelEntity.builder()
			.aktion(getAktion("DOKUMENTENMAPPEN_STATUS_AENDERN")) //LESEN
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("MITARBEITER")) //LESER
			.status(getStatus("BEREIT_FUER_KABINETTVERFAHREN")) //DONT CARE
			.bezeichnung(regelName)
			.build();

		RegelEntity regelEntity2 = RegelEntity.builder()
			.aktion(getAktion("DOKUMENTE_ERSTELLEN")) //SCHREIBEN
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("ERSTELLER")) //ERSTELLER
			.status(getStatus("BEREIT_FUER_KABINETTVERFAHREN")) //DONT CARE
			.bezeichnung(regelName2)
			.build();

		ZuordnungEntity zuordnungEntityDokument1Benutzer1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.bezeichnung(bezeichnung1)
			.ressourceId(ressourceId1)
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("ERSTELLER")) //ERSTELLER
			.anmerkungen("Test123")
			.build();

		ZuordnungEntity zuordnungEntityDokument2Benutzer1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.bezeichnung(bezeichnung2)
			.ressourceId(ressourceId2)
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("MITARBEITER")) //LESER
			.anmerkungen("Test123")
			.build();

		ZuordnungEntity zuordnungEntityDokument3Benutzer1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.bezeichnung(bezeichnung3)
			.ressourceId(ressourceId3)
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("MITARBEITER")) //LESER
			.anmerkungen("Test123")
			.build();

		//Act
		regelnRepository.save(regelEntity);
		regelnRepository.save(regelEntity2);
		zuordnungenRepository.save(zuordnungEntityDokument1Benutzer1);
		zuordnungenRepository.save(zuordnungEntityDokument2Benutzer1);
		zuordnungenRepository.save(zuordnungEntityDokument3Benutzer1);

		HashMap<String, Set<String>> allowedDocumentsWithPermissions =
			editorRollenUndRechte.getAlleDokumenteWithPermissionsbyBenutzerId(benutzerId, "DOKUMENTE");

		Set<String> keySet = allowedDocumentsWithPermissions.keySet();

		assertThat(keySet).contains(ressourceId1)
			.contains(ressourceId2)
			.contains(ressourceId3);

		assertThat(allowedDocumentsWithPermissions.get(ressourceId1)).contains("SCHREIBEN");
		assertThat(allowedDocumentsWithPermissions.get(ressourceId2)).contains("LESEN");
		assertThat(allowedDocumentsWithPermissions.get(ressourceId3)).contains("LESEN");

		assertThat(allowedDocumentsWithPermissions.get(ressourceId2)).doesNotContain("SCHREIBEN");
		assertThat(allowedDocumentsWithPermissions.get(ressourceId3)).doesNotContain("SCHREIBEN");

	}


	@Test
	void checkGetAlleBenutzerIdsByRessourceIdAndAktion() {
		// Arrange
		String regelName = "Testregel01";

		String benutzerId = "12345678-3f97-4445-8eb1-774e4fa50b16";
		String bezeichnung11 = "DOKUMENTE12345678-3f97-4445-8eb1-774e4fa50b16ERST";
		String ressourceId = "12345678-2d83-402c-b28a-dcc4dc64b374";

		String bezeichnung12 = "DOKUMENTE22345678-3f97-4445-8eb1-774e4fa50b16ERST";

		String benutzer2Id = "22222278-3f97-4445-8eb1-774e4fa50b16";
		String bezeichnung21 = "DOKUMENTE22222278-3f97-4445-8eb1-774e4fa50b16ERST";
		String ressource2Id = "22345678-2d83-402c-b28a-dcc4dc64b374";

		RegelEntity regelEntity = RegelEntity.builder()
			.aktion(getAktion("DOKUMENTENMAPPEN_STATUS_AENDERN")) //LESEN
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("LESER")) //MITARBEITER
			.status(getStatus("FINAL")) //DONT CARE
			.bezeichnung(regelName)
			.build();

		ZuordnungEntity zuordnungEntityDokument1Benutzer1 = ZuordnungEntity.builder()
			.benutzerId(benutzerId)
			.bezeichnung(bezeichnung11)
			.ressourceId(ressourceId)
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("LESER")) //MITARBEITER
			.anmerkungen("Test123")
			.build();

		ZuordnungEntity zuordnungEntityDokument1Benutzer2 = ZuordnungEntity.builder()
			.benutzerId(benutzer2Id)
			.bezeichnung(bezeichnung12)
			.ressourceId(ressourceId)
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("LESER")) //MITARBEITER
			.anmerkungen("Test123")
			.build();

		ZuordnungEntity zuordnungEntityDokument2Benutzer1 = ZuordnungEntity.builder()
			.benutzerId(benutzer2Id)
			.bezeichnung(bezeichnung21)
			.ressourceId(ressource2Id)
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("LESER")) //MITARBEITER
			.anmerkungen("Test123")
			.build();

		//Act
		regelnRepository.save(regelEntity);
		zuordnungenRepository.save(zuordnungEntityDokument1Benutzer1);
		zuordnungenRepository.save(zuordnungEntityDokument1Benutzer2);
		zuordnungenRepository.save(zuordnungEntityDokument2Benutzer1);

		Set<String> allowedIds = editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndAktion(ressourceId, "LESEN");
		Set<String> allowedIds2 = editorRollenUndRechte.getAlleBenutzerIdsByRessourceIdAndAktion(ressource2Id, "LESEN");

		assertThat(allowedIds).contains(benutzerId)
			.contains(benutzer2Id);
		assertThat(allowedIds2).contains(benutzer2Id);
	}


	@Test
	void whenCreateOneRule_thenItShouldBePossible() {
		// Arrange
		String regelName = "Testregel1";

		RegelEntity regelEntity = RegelEntity.builder()
			.aktion(getAktion("DOKUMENTENMAPPEN_STATUS_AENDERN"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("ERSTELLER"))
			.status(getStatus("BEREIT_FUER_KABINETTVERFAHREN"))
			.bezeichnung(regelName)
			.build();

		// Act
		regelnRepository.save(regelEntity);
		Optional<RegelEntity> actual = regelnRepository.findByBezeichnung(regelName);

		// Assert
		assertThat(actual).isPresent();
		RegelEntity actualRule = actual.get();

		assertThat(actualRule.getBezeichnung()).isEqualTo(regelName);

		AktionEntity aktionInRule = actualRule.getAktion();
		assertThat(aktionInRule.getBezeichnung()).isEqualTo(getAktion("DOKUMENTENMAPPEN_STATUS_AENDERN")
			.getBezeichnung());

		RessourceTypEntity ressourceTyp = actualRule.getRessourceTyp();
		assertThat(ressourceTyp.getBezeichnung()).isEqualTo(getRessourceTyp("DOKUMENTE")
			.getBezeichnung());

		RolleEntity roleInRule = actualRule.getRolle();
		assertThat(roleInRule.getBezeichnung()).isEqualTo(getRolle("ERSTELLER")
			.getBezeichnung());

		StatusEntity statusInRule = actualRule.getStatus();
		assertThat(statusInRule.getBezeichnung()).isEqualTo(getStatus("BEREIT_FUER_KABINETTVERFAHREN")
			.getBezeichnung());
	}


	@Test
	void whenCreateDifferentRules_thenItShouldBePossible() {
		// Arrange
		String regelName1 = "Testregel2";
		String regelName2 = "Testregel3";

		RegelEntity regelEntity1 = RegelEntity.builder()
			.aktion(getAktion("DOKUMENTENMAPPEN_STATUS_AENDERN"))
			.ressourceTyp(getRessourceTyp("DOKUMENTENMAPPE"))
			.rolle(getRolle("ERSTELLER"))
			.status(getStatus("DONT CARE"))
			.bezeichnung(regelName1)
			.build();

		RegelEntity regelEntity2 = RegelEntity.builder()
			.aktion(getAktion("DOKUMENTE_ERSTELLEN"))
			.ressourceTyp(getRessourceTyp("DOKUMENTENMAPPE"))
			.rolle(getRolle("LESER"))
			.status(getStatus("DONT CARE"))
			.bezeichnung(regelName2)
			.build();

		// Act
		regelnRepository.save(regelEntity1);
		regelnRepository.save(regelEntity2);

		Optional<RegelEntity> presents = regelnRepository.findByBezeichnung(regelName1);
		Optional<RegelEntity> actual = regelnRepository.findByBezeichnung(regelName2);

		// Assert
		// Proofed by first test, but must exist
		assertThat(presents).isPresent();

		// Proof of this methode
		assertThat(actual).isPresent();
		RegelEntity actualRule = actual.get();

		assertThat(actualRule.getBezeichnung()).isEqualTo(regelName2);

		AktionEntity aktionInRule = actualRule.getAktion();
		assertThat(aktionInRule.getBezeichnung()).isEqualTo(getAktion("DOKUMENTE_ERSTELLEN")
			.getBezeichnung());

		RessourceTypEntity ressourceTyp = actualRule.getRessourceTyp();
		assertThat(ressourceTyp.getBezeichnung()).isEqualTo(getRessourceTyp("DOKUMENTENMAPPE")
			.getBezeichnung());

		RolleEntity roleInRule = actualRule.getRolle();
		assertThat(roleInRule.getBezeichnung()).isEqualTo(getRolle("LESER")
			.getBezeichnung());

		StatusEntity statusInRule = actualRule.getStatus();
		assertThat(statusInRule.getBezeichnung()).isEqualTo(getStatus("DONT CARE")
			.getBezeichnung());
	}


	@Test
	void whenCreateSameRuleByContent_thenAnErrorShouldBeShown() {
		// Arrange
		String regelName1 = "Testregel4";
		String regelName2 = "Testregel5";

		RegelEntity regelEntity1 = RegelEntity.builder()
			.aktion(getAktion("KOMMENTIEREN"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("LESER"))
			.status(getStatus("BEREIT_FUER_KABINETTVERFAHREN"))
			.bezeichnung(regelName1)
			.build();

		RegelEntity regelEntity2 = RegelEntity.builder()
			.aktion(getAktion("KOMMENTIEREN"))
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("LESER"))
			.status(getStatus("BEREIT_FUER_KABINETTVERFAHREN"))
			.bezeichnung(regelName2)
			.build();

		// Act
		regelnRepository.save(regelEntity1);

		// Assert
		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			regelnRepository.save(regelEntity2);
		});
	}


	@Test
	void findByParameters() {
		// Arrange
		String regelName1 = "Regel: Ich darf schreiben";
		String regelName2 = "Regel: Ich darf lesen";

		RegelEntity regelEntity1 = RegelEntity.builder()
			.aktion(getAktion("DOKUMENTE_ERSTELLEN")) // schreiben
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("LESER")) // Mitarbeiter
			.status(getStatus("BEREIT_FUER_KABINETTVERFAHREN")) // DRAFT
			.bezeichnung(regelName1)
			.build();

		RegelEntity regelEntity2 = RegelEntity.builder()
			.aktion(getAktion("DOKUMENTENMAPPEN_STATUS_AENDERN")) // lesen
			.ressourceTyp(getRessourceTyp("DOKUMENTE"))
			.rolle(getRolle("LESER")) // Mitarbeiter
			.status(getStatus("BEREIT_FUER_KABINETTVERFAHREN")) // DRAFT
			.bezeichnung(regelName2)
			.build();

		regelnRepository.save(regelEntity1);
		regelnRepository.save(regelEntity2);

		// Act
		// Was darf ich als Mitarbeiter mit einem Dokument, welches im DRAFT-Status ist, machen?
		Optional<List<RegelEntity>> actualOptinal = regelnRepository.findByRessourceTypAndRolleAndStatusIn(
			getRessourceTyp("DOKUMENTE"), getRolle("LESER"), List.of(getStatus("BEREIT_FUER_KABINETTVERFAHREN")));

		assertThat(actualOptinal).isPresent();
		List<RegelEntity> actual = actualOptinal.get();

		assertThat(actual).hasSize(3);
	}

}
