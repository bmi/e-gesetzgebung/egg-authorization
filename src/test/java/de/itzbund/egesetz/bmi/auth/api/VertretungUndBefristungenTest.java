// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.api;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import de.itzbund.egesetz.bmi.auth.repositories.RegelnRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RessourceTypenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.RollenRepository;
import de.itzbund.egesetz.bmi.auth.repositories.ZuordnungenRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class VertretungUndBefristungenTest {

	private static final String AKTION = "LESEN";
	private static final String RESOURCE_TYPE = "DOKUMENTE";
	private static final String ROLLE_ERSTELLER = "ERSTELLER";
	private static final String ROLLE_SCHREIBER = "SCHREIBER";
	private static final String ROLLE_GUTACHTER = "GUTACHTER";
	private static final String ROLLE_LESER = "LESER";
	private static final String ERSTELLER_BENUTZER_ID = "erstellerId";
	private static final String SCHREIBER_ID = "schreiberId";
	private static final String VERTRETER_ID = "vertreterId";
	private static final String NEUE_BENUTZER_ID = "benutzerIdNeu";
	private static final String RESSOURCE_ID = "ressourceId";
	private final static Instant NOW = Instant.now();
	private final static Instant FIVE_BEFORE_NOW = NOW.minus(5, ChronoUnit.MINUTES);

	private final List<AktionEntity> aktionen = new ArrayList<>();
	private final List<RessourceTypEntity> ressourceTypen = new ArrayList<>();
	private final List<RolleEntity> rollen = new ArrayList<>();
	private final List<ZuordnungEntity> zuordnungen = new ArrayList<>();
	private final List<RegelEntity> regeln = new ArrayList<>();
	private final List<ZuordnungEntity> deletions = new ArrayList<>();

	@MockBean
	private RegelnRepository regelnRepository;
	@MockBean
	private RessourceTypenRepository ressourceTypenRepository;
	@MockBean
	private RollenRepository rollenRepository;
	@MockBean
	private ZuordnungenRepository zuordnungenRepository;
	@Autowired
	private EGesetzRechteTestHelper accessRights;

	private List<ZuordnungEntity> whatShouldSaved;

	private static boolean equals(String s1, String s2) {
		return StringUtils.equalsIgnoreCase(StringUtils.trim(s1), StringUtils.trim(s2));
	}

	@BeforeEach
	void init() {
		whatShouldSaved = new ArrayList<>();
		prepareObjects();
		prepareMocks();
	}

	private AktionEntity getAktion(String bezeichnung) {
		return aktionen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Aktion '" + bezeichnung + "' nicht gefunden!"));
	}

	private RessourceTypEntity getRessourceTyp(String bezeichnung) {
		return ressourceTypen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Ressource-Typ '" + bezeichnung + "' nicht gefunden!"));
	}

	private RolleEntity getRolle(String bezeichnung) {
		return rollen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Rolle '" + bezeichnung + "' nicht gefunden!"));
	}

	private void prepareObjects() {
		aktionen.add(AktionEntity.builder().bezeichnung(AKTION).build());
		regeln.add(RegelEntity.builder().aktion(getAktion("LESEN")).build());
		ressourceTypen.add(RessourceTypEntity.builder().bezeichnung(RESOURCE_TYPE).build());

		rollen.add(RolleEntity.builder().bezeichnung(ROLLE_ERSTELLER).build());
		rollen.add(RolleEntity.builder().bezeichnung(ROLLE_SCHREIBER).build());
		rollen.add(RolleEntity.builder().bezeichnung(ROLLE_GUTACHTER).build());
		rollen.add(RolleEntity.builder().bezeichnung(ROLLE_LESER).build());

		zuordnungen.add(ZuordnungEntity.builder()
			.id(1L)
			.benutzerId(ERSTELLER_BENUTZER_ID)
			.bezeichnung("Zuordnung 1")
			.rolle(getRolle(ROLLE_ERSTELLER))
			.ressourceId(RESSOURCE_ID)
			.deaktiviert(true)
			.build());

		zuordnungen.add(ZuordnungEntity.builder()
			.id(2L)
			.benutzerId(SCHREIBER_ID)
			.bezeichnung("Zuordnung 2")
			.rolle(getRolle(ROLLE_SCHREIBER))
			.fallbackZuordnung(1L)
			.ressourceId(RESSOURCE_ID)
			.deaktiviert(true)
			.build());
		zuordnungen.add(ZuordnungEntity.builder()
			.id(3L)
			.benutzerId(VERTRETER_ID)
			.bezeichnung("Zuordnung 3")
			.befristung(FIVE_BEFORE_NOW)
			.rolle(getRolle(ROLLE_GUTACHTER))
			.deaktiviert(true)
			.fallbackZuordnung(2L)
			.ressourceId(RESSOURCE_ID)
			.build());
		zuordnungen.add(ZuordnungEntity.builder()
			.id(4L)
			.benutzerId(UUID.randomUUID().toString())
			.bezeichnung("Zuordnung 4")
			.befristung(NOW)
			.rolle(getRolle(ROLLE_GUTACHTER))
			.fallbackZuordnung(3L)
			.ressourceId(RESSOURCE_ID)
			.build());

		deletions.clear();
	}

	private void prepareMocks() {

		when(zuordnungenRepository.findByRessourceIdOrderById(anyString())).thenReturn(zuordnungen);

		doAnswer((i) -> {
			deletions.add(i.getArgument(0));
			return null;
		}).when(zuordnungenRepository).delete(any());

		when(ressourceTypenRepository.findByBezeichnung(any())).thenReturn(Optional.of(getRessourceTyp("DOKUMENTE")));

		when(rollenRepository.findAll()).thenReturn(rollen);

		// Das hier auch
		when(zuordnungenRepository.save(any()))
			.thenAnswer(i -> {
				whatShouldSaved.add((ZuordnungEntity) i.getArguments()[0]);
				return whatShouldSaved.get(whatShouldSaved.size() - 1);
			});
	}

	@Test
	void testSchreibrechteDemErstellerZuweisen() {
		// Arrange
		when(zuordnungenRepository.findByRessourceIdOrderById(anyString())).thenReturn(Collections.emptyList());

		// Act
		boolean actual = accessRights.addRessourcePermission(SCHREIBER_ID, RESOURCE_TYPE, RESSOURCE_ID,
			getRolle(ROLLE_SCHREIBER).getBezeichnung(), NOW);

		// Assert
		assertThat(actual).isTrue();
		assertThat(whatShouldSaved.get(0)).hasFieldOrPropertyWithValue("befristung", null);
		assertThat(deletions).isEmpty();
	}


	@Test
	void testSchreibrechteEinemVertreterZuweisen() {
		// Arrange
		ZuordnungEntity zuordnungEntity = zuordnungen.get(0);
		zuordnungEntity.setDeaktiviert(false);

		when(zuordnungenRepository.findByRessourceIdOrderById(anyString())).thenReturn(List.of(zuordnungEntity));

		// Act
		boolean actual = accessRights.addRessourcePermission(NEUE_BENUTZER_ID, RESOURCE_TYPE, RESSOURCE_ID,
			getRolle(ROLLE_SCHREIBER).getBezeichnung(), NOW);

		// Assert
		assertThat(actual).isTrue();
		assertThat(whatShouldSaved.get(1))
			.hasFieldOrPropertyWithValue("deaktiviert", false)
			.hasFieldOrPropertyWithValue("befristung", NOW);
		assertThat(deletions).isEmpty();
	}

	@Test
	void testSchreibrechteweitergabeAnNtenNutzer() {
		// Act
		boolean actual = accessRights.addRessourcePermission(NEUE_BENUTZER_ID, RESOURCE_TYPE, RESSOURCE_ID,
			getRolle(ROLLE_SCHREIBER).getBezeichnung(), NOW);

		// Assert
		assertThat(actual).isTrue();
		assertThat(whatShouldSaved.get(1)).hasFieldOrPropertyWithValue("befristung", NOW);
		assertThat(deletions).isEmpty();
	}

	@Test
	void testSchreibrechteweitergabeZurueckAnErsteller() {
		// Act
		boolean actual = accessRights.addRessourcePermission(
			ERSTELLER_BENUTZER_ID, RESOURCE_TYPE, RESSOURCE_ID, getRolle(ROLLE_SCHREIBER).getBezeichnung(), NOW);

		// Assert
		assertThat(actual).isTrue();
		whatShouldSaved.sort(Comparator.comparing(ZuordnungEntity::getId));
		assertThat(whatShouldSaved).hasSize(2).satisfiesExactlyInAnyOrder(
			zuordnungEntity -> assertThat(zuordnungEntity)
				.hasFieldOrPropertyWithValue("benutzerId", ERSTELLER_BENUTZER_ID)
				.hasFieldOrPropertyWithValue("rolle", getRolle(ROLLE_ERSTELLER))
				.hasFieldOrPropertyWithValue("befristung", null)
				.hasFieldOrPropertyWithValue("deaktiviert", false),
			zuordnungEntity -> assertThat(zuordnungEntity)
				.hasFieldOrPropertyWithValue("benutzerId", SCHREIBER_ID)
				.hasFieldOrPropertyWithValue("rolle", getRolle(ROLLE_LESER))
				.hasFieldOrPropertyWithValue("befristung", null)
				.hasFieldOrPropertyWithValue("deaktiviert", false)
		);
		assertThat(deletions).hasSize(1);
	}

	@Test
	void testSchreibrechteweitergabeZurueckAnSchreiber() {
		// Act
		boolean actual = accessRights.addRessourcePermission(
			SCHREIBER_ID, RESOURCE_TYPE, RESSOURCE_ID, getRolle(ROLLE_SCHREIBER).getBezeichnung(), NOW);

		// Assert
		assertThat(actual).isTrue();
		assertThat(whatShouldSaved).singleElement().hasFieldOrPropertyWithValue("befristung", NOW);
		assertThat(deletions).isEmpty();
	}

	@Test
	void testSchreibrechteweitergabeZurueckAnVertreter() {
		// Act
		boolean actual = accessRights.addRessourcePermission(VERTRETER_ID, RESOURCE_TYPE, RESSOURCE_ID,
			getRolle(ROLLE_SCHREIBER).getBezeichnung(), FIVE_BEFORE_NOW);

		// Assert
		assertThat(actual).isTrue();
		assertThat(whatShouldSaved.get(1)).hasFieldOrPropertyWithValue("befristung", FIVE_BEFORE_NOW);
		assertThat(deletions).isEmpty();
	}

	@Test
	void testBefristungDarfUrspruenglicheFristNichtUeberschreiten() {
		Instant later = NOW.plus(5, ChronoUnit.MINUTES);

		// Act
		boolean actual = accessRights.addRessourcePermission(NEUE_BENUTZER_ID, RESOURCE_TYPE, RESSOURCE_ID,
			getRolle(ROLLE_SCHREIBER).getBezeichnung(), later);

		// Assert
		assertThat(actual).isTrue();
//        assertThat(whatShouldSaved).hasFieldOrPropertyWithValue("befristung", NOW);
		assertThat(deletions).isEmpty();
	}

	@Test
	void wennBefristungenAbgelaufen_RechteMuessenZurueckGegebenWerden() {
		// Act
		accessRights.clearInvalidPermissionsByPreviousSetLimitation(RESSOURCE_ID);

		// Assert
		assertThat(deletions).isEmpty();
		assertThat(whatShouldSaved).isEmpty();
	}
}
