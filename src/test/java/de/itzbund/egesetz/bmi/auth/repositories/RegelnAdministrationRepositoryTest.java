// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.repositories;

import de.itzbund.egesetz.bmi.auth.api.rights.EGesetzRechte;
import de.itzbund.egesetz.bmi.auth.entities.AktionEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelAdministrationEntity;
import de.itzbund.egesetz.bmi.auth.entities.RegelEntity;
import de.itzbund.egesetz.bmi.auth.entities.RessourceTypEntity;
import de.itzbund.egesetz.bmi.auth.entities.RolleEntity;
import de.itzbund.egesetz.bmi.auth.entities.StatusEntity;
import de.itzbund.egesetz.bmi.auth.entities.ZuordnungEntity;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!! In H2 MEMORY MODE, the scripts schema.sql and data.sql !!!!!!!!!!!!
// !!!!!!!!!! are automatically applied !!!                          !!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//@SqlGroup({
//        @Sql(scripts = "/data.sql")
//})
@SpringBootTest
class RegelnAdministrationRepositoryTest {

	@Autowired
	private AktionenRepository aktionenRepository;
	@Autowired
	private RollenRepository rollenRepository;
	@Autowired
	private RegelnAdministrationRepository regelnAdministrationRepository;

	private List<AktionEntity> aktionen = null;
	private List<RolleEntity> rollen = null;

	@BeforeEach
	void init() {
		aktionen = aktionenRepository.findAll();
		rollen = rollenRepository.findAll();
	}

	private static boolean equals(String s1, String s2) {
		return StringUtils.equalsIgnoreCase(StringUtils.trim(s1), StringUtils.trim(s2));
	}

	private AktionEntity getAktion(String bezeichnung) {
		return aktionen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Aktion '" + bezeichnung + "' nicht gefunden!"));
	}
	private RolleEntity getRolle(String bezeichnung) {
		return rollen.stream()
			.filter(e -> equals(e.getBezeichnung(), bezeichnung))
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("Rolle '" + bezeichnung + "' nicht gefunden!"));
	}

	@Test
	void findByParameters() {
		// Arrange
		String regelName1 = "Regel: Ich darf schreiben";
		String regelName2 = "Regel: Ich darf lesen";

		RegelAdministrationEntity regelAdministrationEntity1 = RegelAdministrationEntity.builder()
			.aktion(getAktion("DOKUMENTE_ERSTELLEN")) // schreiben
			.rolle(getRolle("LESER")) // Mitarbeiter
			.angefragteRolleId(1L)
			.build();

		RegelAdministrationEntity regelAdministrationEntity2 = RegelAdministrationEntity.builder()
			.aktion(getAktion("DOKUMENTENMAPPEN_STATUS_AENDERN")) // schreiben
			.rolle(getRolle("LESER")) // Mitarbeiter
			.angefragteRolleId(2L)
			.build();

		regelnAdministrationRepository.save(regelAdministrationEntity1);
		regelnAdministrationRepository.save(regelAdministrationEntity2);

		// Act
		Optional<RegelAdministrationEntity> actualOptinal = regelnAdministrationRepository.findByRolleAndAktionAndAngefragteRolleId(getRolle("LESER"), getAktion("DOKUMENTE_ERSTELLEN"), 1L);

		assertThat(actualOptinal).isPresent();
		RegelAdministrationEntity actual = actualOptinal.get();

		assertThat(actual).isNotNull();
	}
}
