// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

package de.itzbund.egesetz.bmi.auth.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class RessourceTypEntityTest {

    @Autowired
    private TestEntityManager entityManager;

    private RessourceTypEntity ressourceTyp;


    @BeforeEach
    public void setUp() {
        ressourceTyp = new RessourceTypEntity();
        ressourceTyp.setBezeichnung("TestRessourceTyp");

        Set<RegelEntity> regeln = new HashSet<>();
        RegelEntity regel1 = new RegelEntity();
        regel1.setBezeichnung("TestRegel1");
        entityManager.persistAndFlush(regel1);

        RegelEntity regel2 = new RegelEntity();
        regel2.setBezeichnung("TestRegel2");
        entityManager.persistAndFlush(regel2);

        regeln.add(regel1);
        regeln.add(regel2);

        ressourceTyp.setRegel(regeln);
        entityManager.persistAndFlush(ressourceTyp);
    }


    @Test
    public void testRessourceTypEntitySaveAndRetrieve() {
        entityManager.persistAndFlush(ressourceTyp);

        RessourceTypEntity retrievedRessourceTyp = entityManager.find(RessourceTypEntity.class, ressourceTyp.getId());

        assertNotNull(retrievedRessourceTyp);
        assertEquals("TestRessourceTyp", retrievedRessourceTyp.getBezeichnung());
        assertEquals(2, retrievedRessourceTyp.getRegel().size());
    }


    @Test
    public void testRessourceTypEntityWithNullBezeichnung() {
        ressourceTyp.setBezeichnung(null);
        entityManager.persistAndFlush(ressourceTyp);

        RessourceTypEntity retrievedRessourceTyp = entityManager.find(RessourceTypEntity.class, ressourceTyp.getId());

        assertNotNull(retrievedRessourceTyp);
        assertNull(retrievedRessourceTyp.getBezeichnung());
    }


    @Test
    public void testRessourceTypEntityWithEmptyRegel() {
        ressourceTyp.setRegel(new HashSet<>());
        entityManager.persistAndFlush(ressourceTyp);

        RessourceTypEntity retrievedRessourceTyp = entityManager.find(RessourceTypEntity.class, ressourceTyp.getId());

        assertNotNull(retrievedRessourceTyp);
        assertEquals(0, retrievedRessourceTyp.getRegel().size());
    }
}
