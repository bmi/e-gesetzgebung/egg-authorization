-- Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
--
-- SPDX-License-Identifier: MPL-2.0

-- ======== ERSTELLER

-- Dokumentenmappe
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1', 'DOKUMENTENMAPPE 00000001-0000-0000-0000-000000000001 1', '00000001-0000-0000-0000-000000000001',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTENMAPPE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='ERSTELLER'),
	        1, -- deaktiviert
			  NULL,
			  NULL,
			  'Ersteller Dokumentenmappe');

-- Dokument
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user1', 'DOKUMENT 00000002-0000-0000-0000-000000000002 1', '00000002-0000-0000-0000-000000000002',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='ERSTELLER'),
	        1, -- deaktiviert
			  NULL,
			  NULL,
			  'Ersteller Dokument');


-- ======== LESERECHTE

-- Dokumentenmappe
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user2', 'DOKUMENTENMAPPE 00000001-0000-0000-0000-000000000001 2', '00000001-0000-0000-0000-000000000001',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTENMAPPE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='LESER'),
	          0,
			  NULL,
			  NULL,
			  'Leserechte');

-- Dokument
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
  VALUES ('user2', 'DOKUMENT 00000002-0000-0000-0000-000000000002 2', '00000002-0000-0000-0000-000000000002',
           (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTE'),
           (SELECT id FROM `rollen` WHERE bezeichnung='LESER'),
	          0,
			  NULL,
			  NULL,
			  'Leserechte');


-- ======== SCHREIBRECHTE

-- -------- DOKUMENTENMAPPE

-- Erstelle eine temporäre Tabelle, um die Unterabfrage durchzuführen
CREATE TEMPORARY TABLE `temp_zuordnungen` AS
SELECT id
FROM `zuordnungen`
WHERE ressource_id='00000001-0000-0000-0000-000000000001' AND rolle_id=(SELECT id FROM `rollen` WHERE bezeichnung='ERSTELLER');

-- Führe die INSERT-Anweisung mit der temporären Tabelle durch
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
VALUES (
    'user3',
    'DOKUMENTENMAPPE 00000001-0000-0000-0000-000000000001 3',
    '00000001-0000-0000-0000-000000000001',
    (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTENMAPPE'),
    (SELECT id FROM `rollen` WHERE bezeichnung='SCHREIBER'),
    0,
    NULL,
    (SELECT id FROM `temp_zuordnungen`),
    'Weitergegebene Schreibrechte'
);

-- Lösche die temporäre Tabelle, nachdem die INSERT-Anweisung ausgeführt wurde
DROP TABLE `temp_zuordnungen`;


-- -------- DOKUMENT

-- Erstelle eine temporäre Tabelle, um die Unterabfrage durchzuführen
CREATE TEMPORARY TABLE `temp_zuordnungen` AS
SELECT id
FROM `zuordnungen`
WHERE ressource_id='00000002-0000-0000-0000-000000000002' AND rolle_id=(SELECT id FROM `rollen` WHERE bezeichnung='ERSTELLER');

-- Führe die INSERT-Anweisung mit der temporären Tabelle durch
INSERT INTO `zuordnungen` (`benutzer_id`, `bezeichnung`, `ressource_id`, `ressource_typ_id`, `rolle_id`, `deaktiviert`, `befristung`, `fallback_zuordnung`, `anmerkungen`)
VALUES (
    'user3',
    'DOKUMENT 00000002-0000-0000-0000-000000000002 3',
    '00000002-0000-0000-0000-000000000002',
    (SELECT id FROM `ressource_typen` WHERE bezeichnung='DOKUMENTE'),
    (SELECT id FROM `rollen` WHERE bezeichnung='SCHREIBER'),
    0,
    NULL,
    (SELECT id FROM `temp_zuordnungen`),
    'Weitergegebene Schreibrechte'
);

-- Lösche die temporäre Tabelle, nachdem die INSERT-Anweisung ausgeführt wurde
DROP TABLE `temp_zuordnungen`;